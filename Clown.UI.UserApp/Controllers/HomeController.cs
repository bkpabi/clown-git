﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Clown.Business.Entities;
namespace Clown.UI.UserApp.Controllers
{
    public class HomeController : Controller
    {
        private RestClient _client;

        public HomeController()
        {
            this._client = new RestClient();
            _client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]);

        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public JsonResult GetSearchTypeHeadData(long CityId, string searchToken)
        {
            var requestEmp = new RestRequest("api/Typehead/GetSearchTypeHeadData/"+CityId.ToString()+"/"+searchToken, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseEmp = _client.Execute<List<TypeHeadDTO>>(requestEmp);
            if (responseEmp.Data != null)
            {
                return Json(responseEmp.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Invalid city id");
            }
        }

        [HttpGet]
        public JsonResult GetFeaturedBusiness(string BusinessType)
        {
            var requestBusiness = new RestRequest("api/Business/GetFeaturedBusiness/" + BusinessType, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseBusiness = _client.Execute<List<BusinessDTO>>(requestBusiness);
            if (responseBusiness.Data != null)
            {
                return Json(responseBusiness.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Invalid Business type");
            }
        }
    }
}