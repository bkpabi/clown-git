﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.UI.UserApp.Models;
using RestSharp;
using Clown.Business.Entities;
namespace Clown.UI.UserApp.Controllers
{
    public class ActionController : Controller
    {
        private RestClient _client;
        public ActionController()
        {
            this._client = new RestClient();
            _client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]);
        }
        // GET: Action
        public ActionResult Index()
        {
            return View();
        }
        //public ActionResult Search()
        //{
        //    return View();
        //}

        public ActionResult RequestQuote()
        {
            return View(new RequestQuoteViewModel());
        }

        [HttpPost]
        public ActionResult RequestQuote(RequestQuoteViewModel model, HttpPostedFileBase[] file)
        {
            return View(model);
        }
        [HttpPost]
        public ActionResult Find(FormCollection collection)
        {
            return RedirectToRoute("FindByName", new { CityName = "Bangalore", BusinessType = "Interior Designer", BusinessName = "Cherry Wood Interior" });
        }

        [HttpGet]
        public ActionResult Find(string CityName, string BusinessType, string SearchToken, string SearchKey)
        {
            if (SearchKey == "Business")
            {
                return RedirectToRoute("FindByName", new { CityName = CityName, BusinessType = BusinessType, BusinessName = SearchToken });
            }
            else
            {

            }
            return View();
        }
        [HttpGet]
        [Route("Find/{CityName}/{BusinessType}/{BusinessName}", Name = "FindByName")]
        public ActionResult FindByName(string CityName, string BusinessType, string BusinessName)
        {
            var request = new RestSharp.RestRequest("api/Business/GetBusinessByName/" + CityName + "/" + BusinessType + "/" + BusinessName, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<BusinessDTO>(request);
            if (response != null)
            {
                return View(response.Data);
            }

            return View(new BusinessDTO());
        }
        public ActionResult Search(long CityId, string SearchString, string SearchKey, long SearchId, string BusinessType)
        {
            if (SearchKey == "Business")
            {
                return RedirectToAction("Business", new { Id = SearchId });
            }
            else
            {
                return View();
            }
        }



        public ActionResult Business(string Id)
        {
            return View();
        }

        public ActionResult ShortlistedBusiness()
        {
            return View();
        }

        public ActionResult Reviews()
        {
            return View();
        }

        public ActionResult Projects()
        {
            return View();
        }

        public ActionResult Offers()
        {
            return View();
        }
    }
}