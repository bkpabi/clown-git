﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Clown.UI.UserApp.Models
{
    public class RequestQuoteViewModel
    {
        public RequestQuoteViewModel()
        {
            AvailableTVUnitQuantity = new List<SelectListItem>() { new SelectListItem() { Text = "1", Value = "1" }, new SelectListItem() { Text = "2", Value = "2" }, new SelectListItem() { Text = "3", Value = "3" }, new SelectListItem() { Text = "4", Value = "4" } };
            AvailableHouseType = new List<SelectListItem>() { new SelectListItem() { Text = "1 BHK", Value = "1" }, new SelectListItem() { Text = "2 BHK", Value = "2" }, new SelectListItem() { Text = "3 BHK", Value = "3" }, new SelectListItem() { Text = "4 BHK", Value = "4" } };
            AvailableCotSize = new List<SelectListItem>() { new SelectListItem() { Text = "King", Value = "King" }, new SelectListItem() { Text = "Queen", Value = "Queen" } };
            AvailableKitchenType = new List<SelectListItem>() { new SelectListItem() { Text = "Parallel", Value = "Parallel" }, new SelectListItem() { Text = "U Shaped", Value = "U Shaped" }, new SelectListItem() { Text = "Straight", Value = "Straight" }, new SelectListItem() { Text = "L Shaped", Value = "L Shaped" } };

        }
        [Display(Name="House Type")]
        public string HouseType { get; set; }

        public List<SelectListItem> AvailableHouseType { get; set; }

        [Display(Name="TV Unit")]
        public bool TVUnit { get; set; }

        public List<SelectListItem> AvailableTVUnitQuantity { get; set; }
        
        [Display(Name="Quantity")]
        public int TVUnitQnty { get; set; }

        [Display(Name="Crockery Unit")]
        public bool CrockeryUnit { get; set; }

        [Display(Name="Length")]
        public decimal CrockeryUnitLength { get; set; }

        [Display(Name="Height")]
        public decimal CrockeryUnitHeight { get; set; }

        [Display(Name="Puja Unit")]
        public bool PujaUnit { get; set; }

        [Display(Name = "Length")]
        public decimal PujaUnitLength { get; set; }

        [Display(Name = "Height")]
        public decimal PujaUnitHeight { get; set; }

        [Display(Name = "Shoes Rack")]
        public bool ShoesRack { get; set; }

        [Display(Name = "Length")]
        public decimal ShoesRackLength { get; set; }

        [Display(Name = "Height")]
        public decimal ShoesRackHeight { get; set; }

        [Display(Name = "Wardrobe")]
        public bool Wardrobe { get; set; }

        [Display(Name="Bedroom1")]
        public bool WardrobeBedroom1 { get; set; }

        [Display(Name = "Length")]
        public decimal WardrobeBedroom1Length { get; set; }

        [Display(Name = "Height")]
        public decimal WardrobeBedroom1Height { get; set; }

        [Display(Name="Loft")]
        public bool WardrobeBedroom1Loft { get; set; }

        [Display(Name = "Bedroom2")]
        public bool WardrobeBedroom2 { get; set; }

        [Display(Name = "Length")]
        public decimal WardrobeBedroom2Length { get; set; }

        [Display(Name = "Height")]
        public decimal WardrobeBedroom2Height { get; set; }

        [Display(Name = "Loft")]
        public bool WardrobeBedroom2Loft { get; set; }

        [Display(Name = "Bedroom3")]
        public bool WardrobeBedroom3 { get; set; }

        [Display(Name = "Length")]
        public decimal WardrobeBedroom3Length { get; set; }

        [Display(Name = "Height")]
        public decimal WardrobeBedroom3Height { get; set; }

        [Display(Name = "Loft")]
        public bool WardrobeBedroom3Loft { get; set; }

        [Display(Name = "Bedroom4")]
        public bool WardrobeBedroom4 { get; set; }

        [Display(Name = "Length")]
        public decimal WardrobeBedroom4Length { get; set; }

        [Display(Name = "Height")]
        public decimal WardrobeBedroom4Height { get; set; }

        [Display(Name = "Loft")]
        public bool WardrobeBedroom4Loft { get; set; }

        [Display(Name="Cot")]
        public bool Cot { get; set; }

        [Display(Name="Bedroom1")]
        public bool CotBedroom1 { get; set; }

        [Display(Name="Size")]
        public string CotBedroom1Size { get; set; }

        [Display(Name = "Bedroom2")]
        public bool CotBedroom2 { get; set; }

        [Display(Name = "Size")]
        public string CotBedroom2Size { get; set; }

        [Display(Name = "Bedroom3")]
        public bool CotBedroom3 { get; set; }

        [Display(Name = "Size")]
        public string CotBedroom3Size { get; set; }

        [Display(Name = "Bedroom4")]
        public bool CotBedroom4 { get; set; }

        [Display(Name = "Size")]
        public string CotBedroom4Size { get; set; }

        public List<SelectListItem> AvailableCotSize { get; set; }

        public bool Kitchen { get; set; }

        [Display(Name="Kitchen type")]
        public string KitchenType { get; set; }

        public List<SelectListItem> AvailableKitchenType { get; set; }

        [Display(Name="Height")]
        public decimal KitchenHeight { get; set; }

        [Display(Name = "Length")]
        public decimal KitchenLength { get; set; }

        [Display(Name="Details")]
        public string Details { get; set; }

    }
}