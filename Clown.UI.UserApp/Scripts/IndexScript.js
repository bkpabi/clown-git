﻿$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });

    $("#btnFind").click(function () {
        if ($("#txtBoxParam").val() == "") {
            $('#txtBoxParam').popover({
                trigger: 'manual',
                placement: 'bottom',
                content: function () {
                    var message = "Please enter valid location or business name";
                    return message;
                }
            });
            $("#txtBoxParam").popover('show');
            //$(".popover-content").html('TEST')
        }
        else {
            window.location = "/Action/Find?CityName=Bangalore&BusinessType=" + $(".btn-select-input").val() + "&SearchToken=" + $("#txtBoxParam").val() + "&SearchKey=" + $("#key").val();
        }
    });
    $("#btnCalulateEMI").click(function (e) {
        e.preventDefault();
        CalculateEMI();
    });

    InitialiseSearchTypeHead();
    PopulateBusinessType();
    PopulateFeaturedInteriorDesigners();
    PopulateFeaturedCarpenters();
    PopulateFeaturedOffers();
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});



$(document).on('click', function (e) {
    if (e.target.id == "btnFind") {
        
    }
    else
    {
        $("#txtBoxParam").popover('hide');
    }
    //alert(e.target.id);
});

function PopulateBusinessType() {

    $.ajax({
        //url: refURl + '/api/ServiceProvider/GetAllAreaByCity/' + $('option:selected', $(this)).text(),
        url: refURl + "/api/BusinessType/GetAllBusinessTypes",
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //crossDomain: true,
        success: function (data) {
            for (var key in data) {
                $('#businessType').append('<li>' + data[key].Name + '</li>');
            }
        },
        error: function (x, y, z) {
            alert(x + y + z);
        }

    });


    //$.ajax({
    //    url: refURl + "/api/BusinessType/GetAllBusinessTypes",
    //        type: 'GET',
    //        contentType: 'application/json; charset=utf-8',
    //        dataType: 'json',
    //        //crossDomain: true,
    //        success: function (data) {
    //            for (var key in data) {
    //                $('#businessType').append('<li>'+data[key].Name+'</li>');
    //            }
    //        },
    //        error: function (x, y, z) {
    //            alert(x + y + z);
    //        }

    //    });
}

function InitialiseSearchTypeHead() {
    var resources = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.Value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: '/Home/GetSearchTypeHeadData?CityId=1&searchToken=%QUERY',
        }
    });
    //resources.clearPrefetchCache();
    resources.initialize();


    $('#bloodhound .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        displayKey: 'Value',
        source: resources.ttAdapter(),
        templates: {
            suggestion: function (data) {
                return '<div><strong>' + data.Value + '</strong> <div class="badge pull-right"> ' + data.Key + '</div></div>';
            }
            //footer: Handlebars.compile("<b>Searched for '{{query}}'</b>")
        }
    }).bind('typeahead:open', function (ev, suggestion) {

    }).on("typeahead:selected", function (obj, res) {
        $("#key").val(res.Key);
        $("#itemId").val(res.ItemId);
    });
}

function PopulateFeaturedInteriorDesigners() {
    $.ajax({
        //url: refURl + '/api/ServiceProvider/GetAllAreaByCity/' + $('option:selected', $(this)).text(),
        url: refURl + "/api/Business/GetFeaturedBusiness/Interior Designers",
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //crossDomain: true,
        success: function (data) {
            for (var key in data) {
                if (key > 3) break;
                var image = "http://lorempixel.com/850/850/?random=124";
                var services = new Array(data[key].ServiceOffered.length);
                if (data[key].Images[0] != undefined) {
                    image = data[key].Images[0].ImageUrl;
                }
                for (var p in data[key].ServiceOffered) {
                    services[p] = data[key].ServiceOffered[p].ServiceTypeName;
                }
                //$('#businessType').append('<li>' + data[key].Name + '</li>');
                $("#featuredInteriors > div.container > div.row").append('<div class="col-md-3"><div class="fbItemContainer"><div class="fbItemImage"><img src="' + image + '" height="250" width="261" alt="' + data[key].Name + '" /></div><div class="fbItemContent"><div class="fbItemBName">' + data[key].Name + '</div><div class="fbItemLocation"><i class="glyphicon glyphicon-map-marker"></i>' + data[key].Area + '</div><div class="fbItemServices">' + services.join() + '</div><div class="fbItemViewDetails"><a class="btn btn-danger">See Details</a></div></div></div></div>');
                $("#featuredInteriors_loading").hide();
            }
        },
        error: function (x, y, z) {
            alert(x + y + z);
        }

    });

}

function PopulateFeaturedCarpenters() {
    $.ajax({
        url: refURl + "/api/Business/GetFeaturedBusiness/Carpenters",
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //crossDomain: true,
        success: function (data) {
            for (var key in data) {
                if (key > 3) break;
                var image = "http://lorempixel.com/850/850/?random=124";
                var services = new Array(data[key].ServiceOffered.length);
                if (data[key].Images[0] != undefined) {
                    image = data[key].Images[0].ImageUrl;
                }
                for (var p in data[key].ServiceOffered) {
                    services[p] = data[key].ServiceOffered[p].ServiceTypeName;
                }
                //$('#businessType').append('<li>' + data[key].Name + '</li>');
                $("#featuredCarpenters > div.container > div.row.body").append('<div class="col-md-4 carpenterListing"><div><div class="row"><div class="col-md-4 imgContainer"><img src="' + image + '" height="120" width="130" alt="' + data[key].Name + '" /></div><div class="col-md-8 carpenterDetails"><div class="fbItemContent"><div class="fbItemBName">' + data[key].Name + '</div><div class="fbItemLocation"><i class="glyphicon glyphicon-map-marker"></i>' + data[key].Area + 't</div><div class="fbItemServices">' + services.join() + '</div><div></div></div></div></div></div></div>');
                $("#featuredCarpenters_loading").hide();
            }
        },
        error: function (x, y, z) {
            alert(x + y + z);
        }

    });

}

function PopulateFeaturedOffers() {
    $.ajax({
        url: refURl + "/api/Offer/GetFeaturedOffers",
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //crossDomain: true,
        success: function (data) {
            for (var key in data) {
                $("#offers > div.container > div.row").append('<div class="col-md-3 offer"><div class="offer-container"><div class="offer-body"><div class="offer-buinessName"><a href="#">' + data[key].BusinessName + '</a></div><div class="offer-tittle"><a href="#">' + data[key].OfferTittle + ' </a></div><div class="offer-description">' + data[key].OfferDescription + '</div><div class="offer-stats"><div class="col-md-5 text-left" style=""><i class="glyphicon glyphicon-eye-open"></i> 289 Views</div><div class="col-md-7 text-right" style="color:#CC5251;"><i class="glyphicon glyphicon-time"></i> Expires: ' + $.datepicker.formatDate('dd M, yy', new Date(data[key].EndDate)) + '</div></div></div><div class="offer-footer text-center"><i class="glyphicon glyphicon-gift" style="color:#3498DB;"></i> <a href="/Action/Offers">View all offers from ' + data[key].BusinessName + '</a></div></div></div>');
                $("#offers_loading").hide();
            }
        },
        error: function (x, y, z) {
            alert(x + y + z);
        }

    });

}

function CalculateEMI() {
    //var princ = $("#amount").val();
    //var intr = $("#roi").val();
    //var term = $("#tenure").val();
    //$(".emiamount").html(princ * intr / (1 - (Math.pow(1 / (1 + intr), term))));

    var loanAmount = $("#amount").val();
    var numberOfMonths = $("#tenure").val();
    var rateOfInterest = $("#roi").val();
    var monthlyInterestRatio = (rateOfInterest / 100) / 12;

    var top = Math.pow((1 + monthlyInterestRatio), numberOfMonths);
    var bottom = top - 1;
    var sp = top / bottom;
    var emi = ((loanAmount * monthlyInterestRatio) * sp);
    $(".emiamount").html(Math.round(emi));
}