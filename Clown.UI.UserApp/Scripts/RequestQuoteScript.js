﻿$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}


//according menu

$(document).ready(function () {
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');

    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});

    //Open The First Accordion Section When Page Loads
    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');

    // The Accordion Effect
    $('.accordion-header').click(function () {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }

        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    return false;
});


//Furniture detail toggle
$(document).ready(function () {
    $("#tv_container").hide();
    $("#crockery_container").hide();
    $("#poojaunit_container").hide();
    $("#shoesrack_container").hide();
    $("#wardrobe_container_br1").hide();
    $("#wardrobe_container_br2").hide();
    $("#wardrobe_container_br3").hide();
    $("#wardrobe_container_br4").hide();
    $("#cot_container_br1").hide();
    $("#cot_container_br2").hide();
    $("#cot_container_br3").hide();
    $("#cot_container_br4").hide();
    $("#kitchen_container").hide();

    $("#TVUnit").change(function () {
        if ($(this).is(":checked")) {
            $("#tv_container").show("blind");
        }
        else {
            $("#tv_container").hide("blind");
        }
    });
    $("#CrockeryUnit").change(function () {
        if ($(this).is(":checked")) {
            $("#crockery_container").show("blind");
        }
        else {
            $("#crockery_container").hide("blind");
        }
    });
    $("#PujaUnit").change(function () {
        if ($(this).is(":checked")) {
            $("#poojaunit_container").show("blind");
        }
        else {
            $("#poojaunit_container").hide("blind");
        }
    });
    $("#ShoesRack").change(function () {
        if ($(this).is(":checked")) {
            $("#shoesrack_container").show("blind");
        }
        else {
            $("#shoesrack_container").hide("blind");
        }
    });
    $("#Wardrobe").change(function () {
        if ($(this).is(":checked")) {
            
            switch ($("#house_type").val()) {
                case "1":
                    $("#wardrobe_container_br1").show("blind");
                    break;
                case "2":
                    $("#wardrobe_container_br1").show("blind");
                    $("#wardrobe_container_br2").show("blind");
                    break;
                case "3":
                    $("#wardrobe_container_br1").show("blind");
                    $("#wardrobe_container_br2").show("blind");
                    $("#wardrobe_container_br3").show("blind");
                    break;
                case "4":
                    $("#wardrobe_container_br1").show("blind");
                    $("#wardrobe_container_br2").show("blind");
                    $("#wardrobe_container_br3").show("blind");
                    $("#wardrobe_container_br4").show("blind");
                    break;
                default:
                    break;
            }
        }
        else {
            $("#wardrobe_container_br1").hide("blind");
            $("#wardrobe_container_br2").hide("blind");
            $("#wardrobe_container_br3").hide("blind");
            $("#wardrobe_container_br4").hide("blind");
        }
    });
    $("#Cot").change(function () {
        if ($(this).is(":checked")) {
            switch ($("#house_type").val()) {
                case "1":
                    $("#cot_container_br1").show("blind");
                    break;
                case "2":
                    $("#cot_container_br1").show("blind");
                    $("#cot_container_br2").show("blind");
                    break;
                case "3":
                    $("#cot_container_br1").show("blind");
                    $("#cot_container_br2").show("blind");
                    $("#cot_container_br3").show("blind");
                    break;
                case "4":
                    $("#cot_container_br1").show("blind");
                    $("#cot_container_br2").show("blind");
                    $("#cot_container_br3").show("blind");
                    $("#cot_container_br4").show("blind");
                    break;
                default:
                    break;
            }
        }
        else {
            $("#cot_container_br1").hide("blind");
            $("#cot_container_br2").hide("blind");
            $("#cot_container_br3").hide("blind");
            $("#cot_container_br4").hide("blind");
        }
    });
    $("#Kitchen").change(function () {
        if ($(this).is(":checked")) {
            $("#kitchen_container").show("blind");
        }
        else {
            $("#kitchen_container").hide("blind");
        }
    });
});