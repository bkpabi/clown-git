﻿$(document).ready(function () {
    $('#tblRequest').DataTable({
        "bLengthChange": false,
        "iDisplayLength": 3
    });

    $('#tblResponse').DataTable({
        "bLengthChange": false,
        "iDisplayLength": 20
    });

    $("#tblRequest_wrapper div div").first().first().html('<h4 style="line-height:2px;">Quote Request List</h4>');
    $("#tblResponse_wrapper div div").first().first().html('<h4 style="line-height:2px;">Response for 1234</h4>');

});