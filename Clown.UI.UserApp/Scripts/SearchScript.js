﻿$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $(".srpLocation span").hide();
    //Handles menu drop down
    $('#refineOptions').click(function (e) {
        e.stopPropagation();
    });



    $('.img-thumbnail').on('click', function () {

        $(this).lightGallery({
            dynamic: true,
            dynamicEl: [{
                "src": 'http://lorempixel.com/850/850/?random=123',
                'thumb': 'http://lorempixel.com/850/850/?random=123',
                'subHtml': '<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>'
            }, {
                'src': 'http://lorempixel.com/850/850/?random=124',
                'thumb': 'http://lorempixel.com/850/850/?random=124',
                'subHtml': "<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>"
            }, {
                'src': 'http://lorempixel.com/850/850/?random=125',
                'thumb': 'http://lorempixel.com/850/850/?random=125',
                'subHtml': "<h4>Coniston Calmness</h4><p>Beautiful morning</p>"
            }]
        })

    });

    $('.img-shadow div span').click(function () {
        if ($(this).hasClass("shortlisted")) {
            $(this).removeClass("shortlisted");
        }
        else {
            $(this).addClass("shortlisted");
        }
    });

    $(".row-listing")
  .mouseenter(function () {
      $('[data-toggle="tooltip"]').tooltip();
      $(this).find(".srpLocation span").show();
  })
  .mouseleave(function () {
      $(this).find(".srpLocation span").hide();
  });
});