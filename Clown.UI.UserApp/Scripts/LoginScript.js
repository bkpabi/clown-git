﻿$(document).ready(function () {
    $("#btnForgotpwd").click(function () {
        $.ajax({
            url: "/Account/ForgotPasswordModal",
            type: 'POST',
            dataType: 'json',
            data: { email: $("#txtboxEmail").val() },
            success: function (data) {
                if (data == "1") {
                    $("#forgotpwsSuccessMsg").show();
                    $("#forgotpwsErrorMsg").hide();
                }
                else
                {
                    $("#forgotpwsSuccessMsg").hide();
                    $("#forgotpwsErrorMsg").show();
                }
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });
});