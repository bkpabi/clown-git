﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Clown.UI.UserApp.Startup))]
namespace Clown.UI.UserApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
