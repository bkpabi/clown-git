﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.UI.MVCApp.Models;
using Clown.Core.BusinessEntities;
using Clown.Core.DataModel;
using System.Web.Routing;
namespace Clown.UI.MVCApp.Controllers
{
    public class HomeController : Controller
    {
        private IServiceProviderRepository _repository;
        public HomeController()
        {
            _repository = new ServiceProviderRepository();
        }
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        //[Route("")]
        [Route("Search/{location}/{searchString}/{category}")]
        public ActionResult Search(string location, string searchString, string category=null)
        {
            List<Models.ServiceProviderDTO> lServiceProviderCollection = new List<Models.ServiceProviderDTO>();
            string locationCategory = string.Empty;
            if (location.Contains(','))
            {
                locationCategory = "Area";
            }
            else
            {
                locationCategory = "City";
            }
            ViewBag.location = location;
            ViewBag.locationCategory = locationCategory;
            ViewBag.serach = searchString;
            ViewBag.serachCategory = category;
            return View(lServiceProviderCollection);
        }

        //[Route("Details/{spId}/{test}")]
        public ActionResult Details(string spId)
        {
            ViewBag.ServiceProviderId = spId;
            return View();
        }

        public ActionResult BusinessDetails(string spId)
        {
            ViewBag.ServiceProviderId = spId;
            return View();
        }

	}
}