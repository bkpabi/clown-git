﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Core.BusinessEntities;
using Clown.Core.DataModel;
using Clown.UI.MVCApp.Models;
namespace Clown.UI.MVCApp.Controllers
{
    public class ServiceProviderController : ApiController
    {
        private IServiceProviderRepository _repository;
        public ServiceProviderController()
        {
            _repository = new ServiceProviderRepository();
        }
        [Route("api/ServiceProvider/GetAllAreasByCityAC")]
        [HttpGet]
        public IQueryable<AutoComplete> GetAllAreasAC()
        {
            var areaCollection = _repository.GetAllCities();
            List<Models.AutoComplete> acCollection = new List<Models.AutoComplete>();
            foreach (var item in areaCollection)
            {
                AutoComplete ac = new AutoComplete();
                ac.value = item.CityName;
                ac.data = "City";
                foreach (var area in item.AreaMasters)
                {
                    AutoComplete areaAC = new AutoComplete();
                    areaAC.value = area.AreaName + ", " + area.CityMaster.CityName;
                    areaAC.data = "Area";
                    acCollection.Add(areaAC);
                }
                acCollection.Add(ac);
            }
            
            return acCollection.OrderBy(e=> e.value).AsQueryable();
        }


        [Route("api/ServiceProvider/GetSearchString")]
        [HttpGet]
        public IQueryable<Models.AutoComplete> GetSearchString(string area)
        {
            List<Models.AutoComplete> acCollection = new List<Models.AutoComplete>();

            // Populate ServiceProvider for autocomplete list
            var dbServicProviderCollection = _repository.GetAllServiceProvider();
            foreach (var item in dbServicProviderCollection)
            {
                AutoComplete ac = new Models.AutoComplete();
                ac.value = item.Name + ", " + item.Address_Area;
                ac.data = "Service Provider";
                acCollection.Add(ac);
            }

            // Populate Service Provider Type
            var dbServicProviderTypeCollection = _repository.GetAllServiceProviderTypes();
            foreach (var spt in dbServicProviderTypeCollection)
            {
                Models.AutoComplete ad = new Models.AutoComplete();
                ad.value = spt.Name;
                ad.data = "Service Provider Type";
                acCollection.Add(ad);
            }

            // Populate product
            var dbProductCollection = _repository.GetAllProducts();
            foreach (var prod in dbProductCollection)
            {
                Models.AutoComplete ad = new Models.AutoComplete();
                ad.value = prod.Name;
                ad.data = "Product";
                acCollection.Add(ad);
            }

            // Populate Keywords
            var dbKeywordCollection = _repository.GetAllKeywords();
            foreach (var keyword in dbKeywordCollection)
            {
                if (acCollection.Where(e=> e.value ==keyword.KeywordName).Count() == 0 )
                {
                    Models.AutoComplete ad = new Models.AutoComplete();
                    ad.value = keyword.KeywordName;
                    ad.data = "Keyword";
                    acCollection.Add(ad);
                }
            }
            return acCollection.Distinct().AsQueryable();
        }

        [Route("api/ServiceProvider/FindServiceProvider")]
        [HttpGet]
        public IQueryable<Models.ServiceProviderDTO> FindServiceProvider(string location, string locationCategory, string serach, string serachCategory)
        {
            List<ServiceProvider> dbServicProvider = null;
            Dictionary<string, string> searchParm = new Dictionary<string, string>();
            searchParm.Add("location", location);
            searchParm.Add("locationCategory", locationCategory);
            searchParm.Add("search", serach);
            searchParm.Add("searchCategory", serachCategory);
            
            dbServicProvider = _repository.Search(searchParm).ToList();

            List<Models.ServiceProviderDTO> lServiceProviderCollection = new List<Models.ServiceProviderDTO>();
            foreach (var item in dbServicProvider)
            {
                Models.ServiceProviderDTO eachSP = new Models.ServiceProviderDTO();
                eachSP.Id = item.Id;
                eachSP.Name = item.Name;
                eachSP.Website = item.Website;
                eachSP.Address = item.Address_Street + ", " + item.AreaMaster.AreaName + ", " + item.CityMaster.CityName + ", " + item.Address_State;
                eachSP.TotalExperience = item.Experience;
                eachSP.Verified = item.Verified == true ? "Verified" : "";
                eachSP.ReviewCount = item.Reviews.Count;
                eachSP.CompletedProjects = item.Projects.Count;
                eachSP.Longitude = item.Longitude;
                eachSP.Lattitude = item.Lattitude;
                eachSP.OfferCount = item.Offers.Count;
                eachSP.ServiceProviderTypeName = item.ServiceProviderType.Name;
                if (item.Reviews.Sum(e => e.Ratting) > 0)
                {
                    eachSP.Ratting = item.Reviews.Sum(e => e.Ratting) / item.Reviews.Count;
                }
                eachSP.ServiceProvided = string.Empty;
                if (eachSP.ServiceProviderTypeName == "Interior Designers" || eachSP.ServiceProviderTypeName == "Carpenters")
                {
                    foreach (var sp in item.ServiceProvideds)
                    {
                        eachSP.ServiceProvided += ", " + sp.ServiceType.ServieTypeName;
                    }
                }
                else
                {
                    foreach (var sp1 in item.Catlogs)
                    {
                        foreach (var prod in sp1.CatlogProducts)
                        {
                            eachSP.ServiceProvided += ", " + prod.ProductMaster.Name;
                        }
                    }
                }
                

                if (eachSP.ServiceProvided.Length > 0 )
                {
                    eachSP.ServiceProvided = eachSP.ServiceProvided.Remove(0, 2);
                    if (eachSP.ServiceProvided.Length > 100)
                    {
                        eachSP.ServiceProvided = eachSP.ServiceProvided.Substring(0, 100);
                        eachSP.ServiceProvided = eachSP.ServiceProvided + "...";
                    }
                }
                lServiceProviderCollection.Add(eachSP);
            }
            return lServiceProviderCollection.AsQueryable();
        }

        [Route("api/ServiceProvider/GetAllServiceTypes")]
        [HttpGet]
        public IQueryable<Models.ServiceTypeDTO> GetAllServiceType()
        {
            List<ServiceTypeDTO> serviceTypeCollection = new List<ServiceTypeDTO>();
            var dbServieType = _repository.GetAllServiceTypes();
            foreach (var item in dbServieType)
            {
                ServiceTypeDTO theServiceType = new ServiceTypeDTO();
                theServiceType.Id = item.Id;
                theServiceType.ServieTypeName = item.ServieTypeName;
                serviceTypeCollection.Add(theServiceType);
            }

            return serviceTypeCollection.AsQueryable();
        }

        [Route("api/ServiceProvider/GetServiceProviderDetails")]
        public Models.ServiceProviderDTO GetServiceProviderDetails(string spId)
        {
            var result = _repository.GetServiceProviderDetails(int.Parse(spId));
            ServiceProviderDTO spdto = new Models.ServiceProviderDTO();
            spdto.Id = result.Id;
            spdto.Name = result.Name;
            //spdto.ContactNumber = result.ContactNumber;
            //spdto.Website = result.Website;
            spdto.ContactNumber =  result.ContactNumber != null ? result.ContactNumber : string.Empty;
            spdto.Website = result.Website != null ? result.Website : string.Empty;
            if (result.Reviews.Sum(e => e.Ratting) > 0)
            {
                spdto.Ratting = result.Reviews.Sum(e => e.Ratting) / result.Reviews.Count;
            }
            spdto.ReviewCount = result.Reviews.Count;
            spdto.Verified = result.Verified == true ? "Verified" : "";
            spdto.Address = result.Address_Street + ", " + result.AreaMaster.AreaName + ", " + result.CityMaster.CityName + ", " + result.Address_State;
            spdto.Lattitude = result.Lattitude;
            spdto.Longitude = result.Longitude;
            spdto.ServiceProviderTypeName = result.ServiceProviderType.Name;
            spdto.ServiceProvided = string.Empty;
            if (spdto.ServiceProviderTypeName == "Interior Designers" || spdto.ServiceProviderTypeName == "Carpenters")
            {
                foreach (var item in result.ServiceProvideds.ToList())
                {
                    spdto.ServiceOffered.Add(new ServiceTypeDTO() { Id = item.Id, ServieTypeName = item.ServiceType.ServieTypeName, url = item.ServiceType.ImageUrl });
                }
            }
            else
            {
                foreach (var item in result.Catlogs.ToList())
                {
                    foreach (var item1 in item.CatlogProducts.ToList())
                    {
                        spdto.ServiceOffered.Add(new ServiceTypeDTO() { Id = item1.ProductMaster.Id, ServieTypeName = item1.ProductMaster.Name, url = item1.ProductMaster.IconUrl });

                    }
                }
            }
            
            //= result.ServiceProvideds.ToList();
            //foreach (var sp in result.ServiceProvideds)
            //{
            //    spdto.ServiceProvided += ", " + sp.ServiceType.ServieTypeName;
            //}

            //if (spdto.ServiceProvided.Length > 0)
            //{
            //    spdto.ServiceProvided = spdto.ServiceProvided.Remove(0, 2);
            //}
            spdto.WorkingHrs = new List<string>();
            foreach (var whrs in result.WorkingHrs)
            {
                spdto.WorkingHrs.Add(whrs.WeekDay + " : " + whrs.WorkingHours);
            }

            //spdto.PriceRange = result.PriceRange;

            spdto.Images = new List<Image>();
            foreach (var item in result.Images)
            {
                spdto.Images.Add(new Image() {ImageUrl = item.ImageUrl, ThumbUrl = item.ThumbUrl });
            }

            spdto.Projects = new List<Project>();
            foreach (var item in result.Projects)
            {
                spdto.Projects.Add(new Project() {ThumbUrl= item.ThumbUrl, ProjectTittle = item.ProjectTittle, Area = item.Area });
            }

            spdto.Reviews = new List<ReviewsDTO>();
            foreach (var item in result.Reviews)
            {
                spdto.Reviews.Add(new ReviewsDTO() { Ratting = item.Ratting, Reviews = item.Reviews, CreatedOn = item.CreatedOn.ToString("MMMM dd, yyyy"), UserName = item.AspNetUser.UserName });
            }
            return spdto;
        }

    }
}
