﻿$(document).ready(function () {
    $("#searchbtn").click(function (e) {
        //TODO : Validate if user has entered correct details
        //var location = $("#location-select-txtbox").val();
        //var search = $("#seach-txtbox").val();
        //var searchCategory = $().val();
        //window.location.href = "Home/Search?location=" + $("#location-select-txtbox").val() + "&locationCategory=" + $("#locationCategory").val() + "&serach=" + $("#seach-txtbox").val() + "&serachCategory=" + $("#searchCategory").val();
        window.location.href = "Search/" + $("#location-select-txtbox").val() + "/" + $("#seach-txtbox").val() + "/" + $("#searchCategory").val();
    });
    autocomplete();
});
function autocomplete() {
    $.ajax({
        url: 'http://localhost:1075/api/ServiceProvider/GetAllAreasByCityAC',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var areas = [];
            for (var key in data) {
                areas.push(data[key]);
            }
            $('#location-select-txtbox').autocomplete({
                lookup: areas,
                minChars: 3,
                autoSelectFirst:true,
                showNoSuggestionNotice: true,
                noSuggestionNotice: "No match found",
                width: 350,
                lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                    return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
                },
                onSelect: function (suggestion) {
                    //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                    $("#locationCategory").val(suggestion.data);
                }
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });

    $.ajax({
        url: 'http://localhost:1075/api/ServiceProvider/GetSearchString?area=Bangalore',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var search = [];
            for (var key in data) {
                search.push(data[key]);
            }
            $('#seach-txtbox').autocomplete({
                lookup: search,
                minChars: 3,
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                noSuggestionNotice: "No match found",
                width: 350,
                lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                    return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
                },
                onSelect: function (suggestion) {
                    //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                    $("#searchCategory").val(suggestion.data);
                }
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });

            
}
