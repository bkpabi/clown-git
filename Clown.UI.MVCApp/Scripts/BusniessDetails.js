﻿$(document).ready(function () {
    PopulateData();
    
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'bottom'
    });
});

function drawMap(lat,lang) {
    var centerLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lang));
    var mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: centerLatlng
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var marker;
    var myLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lang));
    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
    });
    //google.maps.event.addListener(marker, 'click', (function (marker, i) {
    //    return function () {
    //        infowindow.setContent(coordinates[index][2]);
    //        infowindow.open(map, marker);
    //    }
    //})(marker, i));



}

function PopulateData() {
    $.ajax({
        url: "http://localhost:1075/api/ServiceProvider/GetServiceProviderDetails?spId="+ $("#ServiceProviderId").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var coordinates = [];
            //for (var key in data) {
            $(".header-info h1").html(data.Name);
            if (data.ContactNumber =="") {
                $(".contact").html(data.Website);
            }
            else {
                $(".contact").html(data.ContactNumber + ", " + data.Website);
            }
            $(".category").html(data.ServiceProviderTypeName);
            if (data.ServiceProviderTypeName == "Interior Designers" || data.ServiceProviderTypeName == "Carpenters") {
                $(".services-section-head h3").html("Service Offered");
                $(".project-text").html(data.Projects.length);
            }
            else {
                $(".services-section-head h3").html("Products");
                $("#completedproject").empty();
                $("#completedprojecttext").empty();
                $("#projects").hide();
            }
            if (data.Verified != "Verified") {
                $("#verified").hide();
            }
            $(".ratting-number").html(data.Ratting);
            $(".ratting-text").html(data.ReviewCount + " Reviews");
            
            //$("#verified-text").html(data.Verified);
            $(".location-text").html(data.Address);
            drawMap(data.Lattitude, data.Longitude);
            
            var serviceItems = "";
            for (var i = 0; i < data.ServiceOffered.length; i++) {
                if (i!=0 && (i%4) == 0) {
                    $("#servicelist-container").append('<div class="services-section-grids row">' + serviceItems + '</div>');
                    serviceItems = "";
                }
                serviceItems += '<div class="col-md-3 services-section-grid"><div class="services-section-grid-head"><div class="service-icon"><i class="event" style="background:url(' + data.ServiceOffered[i].url + ') no-repeat 0 0;"></i></div><div class="service-icon-heading"><h4>' + data.ServiceOffered[i].ServieTypeName + '</h4></div><div class="clearfix"></div></div></div>';
            }
            if ((data.ServiceOffered.length % 4) != 0 || data.ServiceOffered.length ==4) {
                $("#servicelist-container").append('<div class="services-section-grids row">' + serviceItems + '</div>');
            }

            if (data.WorkingHrs.length > 0) {
                $("#bisunesshrs").empty();
            }
            for (var key in data.WorkingHrs) {
                $("#bisunesshrs").append('<li>' + data.WorkingHrs[key] + '</li>');
            }

            if (data.PriceRange != null) {
                $(".price-text").html(data.PriceRange);
            }

            if (data.Images.length ==0) {
                $(".gallery-section-head").append('<p>Not Available</p>')
            }
            for (var key in data.Images) {
                $("#lightGallery").append('<li data-src="' + data.Images[key].ImageUrl + '"><img src="' + data.Images[key].ThumbUrl + '" /></li>');
            }

            $('#lightGallery').lightGallery({
                showThumbByDefault: true,
                addClass: 'showThumbByDefault'
            });

            if (data.Projects.length == 0) {
                $(".projects-section-head").append('<p>We donot have the details yet.</p>');
            }
            for (var i = 0; i < data.Projects.length; i++) {
                if (i === 3) { break; }
                $(".projects-section-grids").append('<div class="col-md-4"><div class="projects-section-grid-head"><div class="projects-section-grid-head-tittle"><h3>' + data.Projects[i].Tittle + '</h3><h5>' + data.Projects[i].Location + '</h5></div><div class="projects-section-grid-head-body">' + data.Projects[i].Description + '</div></div></div>');
            }

            if (data.Reviews.length == 0) {
                $(".testimonial-section-head").append('<p>No one has reviewed this Interior Designer yet. Be the one to review it</p>');
            }
            for (var key in data.Reviews) {
                if (key === 3) { break; }
                $(".testimonial-section-grids").append('<div class="col-md-4 services-section-grid"><div class="services-section-grid-head"><div class="testimonial-icon-heading"><h4>' + data.Reviews[key].UserName + '</h4><div style="font-size:12px;">Ratted <div class="indivisual-ratting-number">' + data.Reviews[key].Ratting + '</div> | Reviewed on ' + data.Reviews[key].CreatedOn + '</div></div><div class="clearfix"></div></div><p><pre><span>"</span>' + data.Reviews[key].Reviews + '<span>"</span></pre></p></div>');
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
