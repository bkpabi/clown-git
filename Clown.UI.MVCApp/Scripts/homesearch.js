﻿var map;
$(document).ready(function () {
    autocomplete();
    $("#more").click(function (e) {
        //$("#moreSelect").css("display", "block !important")
        $("#moreSelect").removeClass("filterMoreDisabled");
        $("#moreSelect").addClass("filterMore");
        e.stopPropagation();
    });
    $("#moreSelect").click(function (e) {
        e.stopPropagation();
    });
    $(document.body).click(function () {
        if ($('#moreSelect').attr('class') == "filterMore") {
            $("#moreSelect").removeClass("filterMore");
            $("#moreSelect").addClass("filterMoreDisabled");
        }
    });
    PopulateData();
    ShowFilterOptions();
    $(".search_btn").click(function (e) {
        //TODO : Validate if user has entered correct details
        
        PopulateData();
        //window.location.href = "Home/Search?location=" + $("#location-select-txtbox").val() + "&locationCategory=" + $("#locationCategory").val() + "&serach=" + $("#seach-txtbox").val() + "&serachCategory=" + $("#searchCategory").val();
    });
    $(".search_btn").ajaxStart(function () {
        $("#ajax").show();
    });
    //$(".search_btn").ajaxEnd(function () {
    //    $("#ajax").Hide();
    //});
    
});

function autocomplete() {
    $.ajax({
        url: 'http://localhost:1075/api/ServiceProvider/GetAllAreasByCityAC',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var areas = [];
            for (var key in data) {
                areas.push(data[key]);
            }
            $('#location-select-txtbox').autocomplete({
                lookup: areas,
                minChars: 3,
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                noSuggestionNotice: "No match found",
                width: 350,
                lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                    return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
                },
                onSelect: function (suggestion) {
                    //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                    $("#locationCategory").val(suggestion.data);
                    $("#location").val(suggestion.value);
                }
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });

    $.ajax({
        url: 'http://localhost:1075/api/ServiceProvider/GetSearchString?area=Bangalore',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var search = [];
            for (var key in data) {
                search.push(data[key]);
            }
            $('#seach-txtbox').autocomplete({
                lookup: search,
                minChars: 3,
                autoSelectFirst: true,
                showNoSuggestionNotice: true,
                noSuggestionNotice: "No match found",
                width: 350,
                lookupFilter: function (suggestion, originalQuery, queryLowerCase) {
                    return suggestion.value.toLowerCase().indexOf(queryLowerCase) === 0;
                },
                onSelect: function (suggestion) {
                    //alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                    $("#searchCategory").val(suggestion.data);
                    $("#search").val(suggestion.value);
                }
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });


}

function PopulateData() {
    var location = $("#location").val();
    var locationCategory = $("#locationCategory").val();
    var searchCategory = $("#searchCategory").val();
    var search = $("#search").val();
    //var location = $("#location-select-txtbox").val();
    //var search = $("#seach-txtbox").val();

    $(".search-nav").html("Home/" + search + "/" + location);
    $(".search-header").html(search + " in " + location);

    $.ajax({
        url: "http://localhost:1075/api/ServiceProvider/FindServiceProvider?location=" + location + "&locationCategory=" + locationCategory + "&serach=" + search + "&serachCategory=" + searchCategory,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#data-list').empty();
            var coordinates = [];
            for (var key in data) {
                var url = "http://localhost:1075/home/BusinessDetails?spid=" + data[key].Id;
                var verified;
                if (data[key].Verified == "Verified") {
                    verified = '<div class="verification"><img src="/Content/Images/1429632319_heart-32.png" height="20" width="20" /><span>' + data[key].Verified + '</span></div>';
                }
                else {
                    verified = '';
                }

                if (data[key].ServiceProviderTypeName == "Interior Designers" || data[key].ServiceProviderTypeName == "Carpenters") {
                    $('#data-list').append('<li><p><a href="' + url + '">' + data[key].Name + '&nbsp;</a></p><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div>' + verified + '</div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceProvided + '</div><div class="address"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left;"><div style="color:#808080; font-size:12px; min-width:100px; display:inline-block;">Projects</div><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">' + data[key].CompletedProjects + '</span></div><div style="float:left; margin-left:15px;"><span style="color:#808080; font-size:12px; width:100px; display:inline-block;">Experience</span><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">' + data[key].TotalExperience + '</span></div><div style="float:left; margin-left:15px;"><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;">Offers</span><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">' + data[key].OfferCount + '</span></div></div></li>');
                }
                else {
                    $('#data-list').append('<li><p><a href="' + url + '">' + data[key].Name + '&nbsp;</a></p><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div>' + verified + '</div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceProvided + '</div><div class="address"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left;"><div style="float:left;"><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;">Offers</span><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">' + data[key].OfferCount + '</span></div></div></li>');
                }
                
                coordinates.push(new Array(data[key].Longitude, data[key].Lattitude, data[key].Address));
            }
            //addLocation(12.917213, 77.622769);
            $(".search-resultCount").html(data.length + " results found");
            $("#ajax").hide();
            //$('#pagination-demo').twbsPagination({
            //    totalPages: 35,
            //    visiblePages: 7,
            //    onPageClick: function (event, page) {
            //        // TODO : add pagination code
            //        $('#data-list').html('<li><p>Bonito Designs</p><div class="ratting"><div class="ratting-number">4.5</div><div class="ratting-text" style="">101 Reviews</div></div><div class="address">HSR Layout > 2362, 24th Main Road, Sector 1, HSR Layout, Bengaluru, Karnataka</div><div class="deals"><span>Deals in > </span> Kitchen, Interior design</div><div class="statistics"><div style="float:left;"><div style="color:#808080; font-size:11px; width:100px; display:inline;">Completed Projects</div><span style="display:block; color: #514e4e; font-size: 16px; font-weight:600;">15</span></div><div style="float:left; margin-left:15px;"><span style="color:#808080; font-size:11px; width:100px; display:inline;">Total exp</span><span style="display:block; color: #514e4e; font-size: 16px; font-weight:600;">5</span></div></div></li>');
            //    }
            //});
            //initialize(coordinates);
            $(function () {
                /* initiate plugin */
                $("div.holder").jPages({
                    containerID: "data-list",
                    perPage: 2,
                    fallback: 500,
                    scrollBrowse: true,
                    keyBrowse: true
                    //animation: "rotatein"
                });
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function initialize(coordinates) {
    var centerLatlng = new google.maps.LatLng(12.971599, 77.594563);
    var mapOptions = {
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: centerLatlng
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var marker;
    for (index = 0; index < coordinates.length; ++index) {
        if (coordinates[index][0] != null) {
            var myLatlng = new google.maps.LatLng(parseFloat(coordinates[index][1]), parseFloat(coordinates[index][0]));
            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
            //google.maps.event.addListener(marker, 'click', (function (marker, i) {
            //    return function () {
            //        infowindow.setContent(coordinates[index][2]);
            //        infowindow.open(map, marker);
            //    }
            //})(marker, i));
        }
    }

    
}

function ShowFilterOptions() {
    if ($("#searchCategory").val() == "Service Provider Type" && $("#search").val() == "Interior Designers") {
        GetAllServiceTypes();
        $("#filter").append('<li><a class="dropdown-toggle" id="cost" data-toggle="dropdown" style="cursor:pointer;">Avg Cost&nbsp;&nbsp;<span class="caret"></span></a><ul class="dropdown-menu bullet" role="menu" aria-labelledby="cost"><li><a href="#">Less than 1Lakh</a></li><li><a href="#">Rs 1lakh to 2Lakh</a></li><li><a href="#">Rs 2lakh to 3Lakh</a></li><li><a href="#">Rs 3lakh to 4Lakh</a></li><li><a href="#">More than 4lakh</a></li></ul></li>');
        $("#filter").append('<li><a class="dropdown-toggle" id="serviceType" data-toggle="dropdown" style="cursor:pointer;">Service Offered&nbsp;&nbsp;<span class="caret"></span></a><ul id="ser" class="dropdown-menu bullet" role="menu" aria-labelledby="serviceType"></ul></li>');
        $("#filter").append('<li><a class="dropdown-toggle" id="more" data-toggle="dropdown" style="cursor:pointer;">More...&nbsp;&nbsp;<span class="caret"></span></a><ul class="dropdown-menu bullet" role="menu" aria-labelledby="more"><li><a href="#">Machine Made</a></li><li><a href="#">Hand Made</a></li></ul></li>');
    }
    else if ($("#searchCategory").val() == "Service Provider Type" && $("#search").val() == "Carpenters") {
        $("#filter").append('<li>Service Provided<img src="/Content/Images/arrowdown.png" height="20" width="20" /></li>');
        $("#filter").append('<li>More<img src="/Content/Images/arrowdown.png" height="20" width="20" /></li>');
    }
    else if ($("#searchCategory").val() == "Service Type") {
        $("#filter").append('<li>Establishment Type<img src="/Content/Images/arrowdown.png" height="20" width="20" /></li>');
        $("#filter").append('<li>More<img src="/Content/Images/arrowdown.png" height="20" width="20" /></li>');
    }
}

function GetAllServiceTypes() {
    $.ajax({
        url: 'http://localhost:1075/api/ServiceProvider/GetAllServiceTypes',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            for (var key in data) {
                $("#ser").append('<li><a href="#">' + data[key].ServieTypeName + '</a></li>');
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}