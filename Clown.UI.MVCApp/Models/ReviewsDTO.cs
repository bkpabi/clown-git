﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clown.UI.MVCApp.Models
{
    public class ReviewsDTO
    {
        public decimal Ratting { get; set; }
        public string Reviews { get; set; }
        public string CreatedOn { get; set; }
        public string UserName { get; set; }
    }
}