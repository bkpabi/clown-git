﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Clown.Core.BusinessEntities;
namespace Clown.UI.MVCApp.Models
{
    public class ServiceProviderDTO
    {
        public ServiceProviderDTO()
        {
            ServiceOffered = new List<ServiceTypeDTO>();
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public Nullable<decimal> TotalExperience { get; set; }
        public int CompletedProjects { get; set; }
        public decimal Ratting { get; set; }
        public int ReviewCount { get; set; }

        public int OfferCount { get; set; }
        public string ServiceProvided { get; set; }

        public string Verified { get; set; }

        public string Longitude { get; set; }

        public string Lattitude { get; set; }

        public int ServiceProviderTypeId { get; set; }
        public string ServiceProviderTypeName { get; set; }
        public string Address_Area { get; set; }
        public string Address_Street { get; set; }
        public string Address_City { get; set; }
        public string Address_State { get; set; }

        public List<string> WorkingHrs { get; set; }
        public List<Image> Images { get; set; }

        public List<Project> Projects { get; set; }

        public List<ReviewsDTO> Reviews { get; set; }

        public List<Models.ServiceTypeDTO> ServiceOffered { get; set; }

        public string ContactNumber { get; set; }
        public string PriceRange { get; set; }
        public string Tags { get; set; }
    }
}