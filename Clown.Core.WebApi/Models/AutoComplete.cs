﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace Clown.Business.Service.Models
{
    public class AutoComplete
    {
        public string value { get; set; }
        public string data { get; set; }
    }
}