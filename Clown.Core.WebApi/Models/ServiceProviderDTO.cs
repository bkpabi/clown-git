﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clown.Business.Service.Models
{
    public class ServiceProviderDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public Nullable<decimal> TotalExperience { get; set; }
        public int CompletedProjects { get; set; }
        public decimal Ratting { get; set; }
        public int ReviewCount { get; set; }
        
        public int ServiceProviderTypeId { get; set; }
        public string ServiceProviderTypeName { get; set; }
        public string Address_Area { get; set; }
        public string Address_Street { get; set; }
        public string Address_City { get; set; }
        public string Address_State { get; set; }
        
        
        public string Tags { get; set; }
        
    }
}