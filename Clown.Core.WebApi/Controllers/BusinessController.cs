﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Persistance.DataModel;
using Clown.Business.Entities;
using System.Data;
namespace Clown.Business.Service.Controllers
{
    public class BusinessController : ApiController
    {
        private IUnitOfWork unitOfWork;

        //constructor
        public BusinessController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [Route("api/Business/GetAllBusiness")]
        public IEnumerable<BusinessDTO> GetAllBusiness()
        {
            try
            {
                var businessList = this.unitOfWork.BusinessRepository.Get().ToList();
                var mBusinessList = AutoMapper.Mapper.Map<IList<ServiceProvider>, IList<BusinessDTO>>(businessList);
                return mBusinessList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                throw ex;
            }
        }
    }
}
