﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Clown.Persistance.DataModel;
using System.Data.Entity.Validation;
namespace Clown.Test.Repository
{
    [TestClass]
    public class UnitTest1
    {
        private IUnitOfWork unitOfWork;

        //constructor
        public UnitTest1()
        {
            this.unitOfWork = new UnitOfWork(new ClownEntities());
        }
        [TestMethod]
        public void TestMethod1()
        {
            var businessEntiity = this.unitOfWork.BusinessRepository.GetByID(34);
            //ServiceProvider dbBusinessEntity = new ServiceProvider();
            //dbBusinessEntity.Id = 34;
            businessEntiity.PriceRange = decimal.Parse("300000");
            this.unitOfWork.BusinessRepository.Update(businessEntiity);
            this.unitOfWork.Save();
            Assert.AreEqual(1, 1);
            //new line
        }
    }
}
