﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace Clown.Persistance.DataModel
{
    public class GenericRepository<T> : IGenericRepository<T> where T:class
    {
        internal ClownEntities clownContext;
        internal DbSet<T> dbSet;

        public GenericRepository(ClownEntities context)
        {
            this.clownContext = context;
            this.dbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "")
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual T GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual void Insert(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            T entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(T entityToDelete)
        {
            if (clownContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(T entityToUpdate)
        {
            try
            {
                //var dbEntry = clownContext.Entry(entityToUpdate);
                //if (dbEntry == null) return;

                //if (clownContext.Entry(entityToUpdate).State == EntityState.Detached)
                //    dbSet.Attach(entityToUpdate);
                //else
                //{
                //    dbEntry.CurrentValues.SetValues(entityToUpdate);
                //    clownContext.Entry(entityToUpdate).State = EntityState.Modified;
                //}
                dbSet.Attach(entityToUpdate);
                clownContext.Entry(entityToUpdate).State = EntityState.Modified;
            }
            catch (DataException dbEx)
            {

                throw dbEx;
            }
            catch (Exception Ex)
            {

                throw Ex;
            }
            
        }
    }
}
