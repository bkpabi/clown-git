//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Clown.Persistance.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class BundledProduct
    {
        public BundledProduct()
        {
            this.Subscriptions = new HashSet<Subscription>();
        }
    
        public long Id { get; set; }
        public long ProductId { get; set; }
        public int Duration { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public decimal Price { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}
