﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ReviewEntities;
using Clown.Business.Entities;
namespace Clown.Persistance.DataModel.ReviewRepo
{
    public interface IReviewRepository : IDisposable
    {
        IEnumerable<Review> GetAllReviewsByServiceProvider(long ServiceProviderId);
        bool AddAReview(Review review);
        void Save();
    }
}
