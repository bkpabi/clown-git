﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ReviewEntities;
using Clown.Business.Entities;
namespace Clown.Persistance.DataModel.ReviewRepo
{
    public class ReviewRepository : IReviewRepository,IDisposable
    {
        ClownEntities clownContext;

        public ReviewRepository(ClownEntities context)
        {
            this.clownContext = context;
        }
        public IEnumerable<Review> GetAllReviewsByServiceProvider(long ServiceProviderId)
        {
            using (var clownContext = new ClownEntities())
            {
                var dbReviewCollection = clownContext.Reviews.Include("AspNetUser").Where(e => e.ServiceProviderId == ServiceProviderId).ToList();
                return dbReviewCollection;
            }
        }
        public bool AddAReview(Review review)
        {
            using (var clownContext = new ClownEntities())
            {
                clownContext.Reviews.Add(review);
                clownContext.SaveChanges();
                return true;
            }
        }


        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
