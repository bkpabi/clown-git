//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Clown.Persistance.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceType
    {
        public ServiceType()
        {
            this.ServiceProvideds = new HashSet<ServiceProvided>();
        }
    
        public long Id { get; set; }
        public string ServieTypeName { get; set; }
        public string ImageUrl { get; set; }
    
        public virtual ICollection<ServiceProvided> ServiceProvideds { get; set; }
    }
}
