﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities;
using Clown.Business.Entities.WishListEntities;
namespace Clown.Persistance.DataModel.WishListRepo
{
    public interface IWishListRepository : IDisposable
    {
        
        bool AddToWishList(WishList wishList);

        List<WishListViewModel> GetWishListOfAUser(string userId);

        bool DeleteServiceProviderFromWishList(long WishId);

        void Save();
    }
}
