﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.WishListEntities;
namespace Clown.Persistance.DataModel.WishListRepo
{
    public class WishListRepository : IWishListRepository, IDisposable
    {
        ClownEntities clownContext;

        public WishListRepository(ClownEntities context)
        {
            this.clownContext = context;
        }

        public bool AddToWishList(WishList wishList)
        {
            using (var clownContext = new ClownEntities())
            {
                clownContext.WishLists.Add(wishList);
                clownContext.SaveChanges();
                return true;
            }
        }

        public List<WishListViewModel> GetWishListOfAUser(string userId)
        {
            using (var clownContext = new ClownEntities())
            {
                List<WishListViewModel> wishList = new List<WishListViewModel>();
                var wishListCollection = clownContext.WishLists.Include("ServiceProvider").Include("ServiceProvider.ServiceProviderType").Include("ServiceProvider.ServiceProvideds.ServiceType").Include("ServiceProvider.Projects").Include("ServiceProvider.Offers").Include("ServiceProvider.Reviews").Include("ServiceProvider.CityMaster").Include("ServiceProvider.AreaMaster").Include("ServiceProvider.CatlogProducts").Include("ServiceProvider.CatlogProducts.ProductMaster").Where(e => e.UserId == userId).ToList();
                foreach (var item in wishListCollection)
                {
                    WishListViewModel wishListDTO = new WishListViewModel();
                    wishListDTO.WishId = item.Id;
                    wishListDTO.BusinessId = item.ServiceProviderId;
                    wishListDTO.BusinessName = item.ServiceProvider.Name;
                    wishListDTO.BusinessType = item.ServiceProvider.ServiceProviderType.Name;
                    wishListDTO.InBusinessSince = item.ServiceProvider.InBusinessSince.Value;
                    wishListDTO.IsVerified = item.ServiceProvider.Verified.Value;
                    wishListDTO.OfferCount = item.ServiceProvider.Offers.Count;
                    wishListDTO.ProjectCount = item.ServiceProvider.Projects.Count;
                    wishListDTO.Address = item.ServiceProvider.Address_Street + ", " + item.ServiceProvider.AreaMaster.AreaName + ", " + item.ServiceProvider.CityMaster.CityName + ", " + item.ServiceProvider.Address_State;
                    if (item.ServiceProvider.Reviews.Sum(e => e.Ratting) > 0)
                    {
                        wishListDTO.Ratting = item.ServiceProvider.Reviews.Sum(e => e.Ratting) / item.ServiceProvider.Reviews.Count;
                    }
                    wishListDTO.ReviewCount = item.ServiceProvider.Reviews.Count;

                    wishListDTO.ServiceOffered = string.Empty;
                    if (item.ServiceProvider.ServiceProviderType.Name == "Interior Designers" || item.ServiceProvider.ServiceProviderType.Name == "Carpenters")
                    {
                        foreach (var sp in item.ServiceProvider.ServiceProvideds)
                        {
                            wishListDTO.ServiceOffered += ", " + sp.ServiceType.ServieTypeName;
                        }
                    }
                    else
                    {
                        foreach (var sp1 in item.ServiceProvider.CatlogProducts)
                        {
                            wishListDTO.ServiceOffered += ", " + sp1.ProductMaster.Name;
                        }
                    }


                    if (wishListDTO.ServiceOffered.Length > 0)
                    {
                        wishListDTO.ServiceOffered = wishListDTO.ServiceOffered.Remove(0, 2);
                        if (wishListDTO.ServiceOffered.Length > 100)
                        {
                            wishListDTO.ServiceOffered = wishListDTO.ServiceOffered.Substring(0, 100);
                            wishListDTO.ServiceOffered = wishListDTO.ServiceOffered + "...";
                        }
                    }

                    wishList.Add(wishListDTO);
                }
                return wishList;
            }
        }

        public bool DeleteServiceProviderFromWishList(long WishId)
        {
            using (var clownContext = new ClownEntities())
            {
                var wishListDbEntity = clownContext.WishLists.FirstOrDefault(e => e.Id == WishId);
                if (wishListDbEntity != null)
                {
                    clownContext.WishLists.Remove(wishListDbEntity);
                    clownContext.SaveChanges();
                    return true;
                }

                // If we are here some thing went wrong
                return false;
            }
        }

        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
