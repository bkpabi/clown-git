﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Clown.Persistance.DataModel.OfferRepo
{
    public interface IOfferRepository : IDisposable
    {
        IEnumerable<Offer> GetHotOffers();

        Offer GetOfferById(long OfferId);

        IEnumerable<Offer> GetOffersByBusiness(long BusinessId);

        IEnumerable<Offer> GetOffersByCategory(int BusinessCategoryId);

        void AddNewOffer(Offer AddOfferEntity);

        void UpdateOffer(Offer UpdateOfferEntity);

        void DeleteOffer(long OfferId);

        void Save();
    }
}
