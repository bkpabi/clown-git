﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Clown.Persistance.DataModel.OfferRepo
{

    public class OfferRepository : IOfferRepository, IDisposable
    {
        ClownEntities clownContext;

        public OfferRepository(ClownEntities context)
        {
            this.clownContext = context;
        }

        /// <summary>
        /// Fetch all the offers that are currently running with high views
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Offer> GetHotOffers()
        {
            return clownContext.Offers.Include("ServiceProvider").Where(e => e.IsActive == true && e.StartDate <= DateTime.Now && e.EndDate >= DateTime.Now).ToList();
        }

        public Offer GetOfferById(long OfferId)
        {
            var offerEntity = clownContext.Offers.Include("ServiceProvider").FirstOrDefault(e => e.Id == OfferId);
            return offerEntity;
        }

        public IEnumerable<Offer> GetOffersByBusiness(long BusinessId)
        {
            return clownContext.Offers.Include("ServiceProvider").Where(e => e.ServiceProviderId == BusinessId && e.IsActive == true && e.StartDate <= DateTime.Now && e.EndDate >= DateTime.Now).ToList();
        }

        public IEnumerable<Offer> GetOffersByCategory(int BusinessCategoryId)
        {
            throw new NotImplementedException();
        }

        public void AddNewOffer(Offer AddOfferEntity)
        {
            clownContext.Offers.Add(AddOfferEntity);
        }

        public void UpdateOffer(Offer UpdateOfferEntity)
        {
            var dbOffer = clownContext.Offers.FirstOrDefault(e => e.Id == UpdateOfferEntity.Id);
            if (dbOffer != null)
            {
                dbOffer.IsActive = UpdateOfferEntity.IsActive;
                dbOffer.OfferDescription = UpdateOfferEntity.OfferDescription;
                dbOffer.EndDate = UpdateOfferEntity.EndDate;
                dbOffer.OfferTittle = UpdateOfferEntity.OfferTittle;
                dbOffer.StartDate = UpdateOfferEntity.StartDate;
            }
            else
            {
                throw new Exception("Invalid offer entity", new Exception("We cannot find a offer with the given id"));
            }
        }

        public void DeleteOffer(long OfferId)
        {
            var dbOffer = clownContext.Offers.FirstOrDefault(e => e.Id == OfferId);
            if (dbOffer != null)
            {
                clownContext.Offers.Remove(dbOffer);
            }
            else
            {
                throw new Exception("Invalid offer entity", new Exception("We cannot find a offer with the given id"));
            }
            //}
        }


        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
