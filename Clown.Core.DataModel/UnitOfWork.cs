﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Persistance.DataModel
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private ClownEntities clownContext;

        public UnitOfWork(ClownEntities context)
        {
            this.clownContext = context;
        }

        // Declare all repositories here and create accessors for them below
        private GenericRepository<ServiceProvider> businessRepository;
        private GenericRepository<Offer> offerRepository;
        private GenericRepository<Project> projectRepository;
        private GenericRepository<ServiceProviderType> businessTypeRepository;
        private GenericRepository<CityMaster> cityRepository;
        private GenericRepository<AreaMaster> areaRepository;
        private GenericRepository<ServiceType> serviceRepository;
        private GenericRepository<CatlogProduct> catlogRepository;
        private GenericRepository<ProductMaster> productMasterRepository;
        private GenericRepository<ServiceProvided> serviceProvidedRepository;
        private GenericRepository<WorkingHr> workingHrsRepository;
        private GenericRepository<ProjectImage> projectImageRepository;
        private GenericRepository<Image> imageRepository;
        private GenericRepository<Review> reviewRepository;
        private GenericRepository<WishList> wishlistRepository;
        private GenericRepository<Product> productRepository;
        private GenericRepository<LineItem> lineItemRepository;
        private GenericRepository<ProductDetail> productDetailRepository;
        private GenericRepository<BundledProduct> bundledProductRepository;
        private GenericRepository<Subscription> subscriptionRepository;
        public GenericRepository<ServiceProvider> BusinessRepository
        {
            get
            {
                if (businessRepository == null)
                {
                    businessRepository = new GenericRepository<ServiceProvider>(clownContext);
                }

                return this.businessRepository;
            }
        }

        public GenericRepository<Offer> OfferRepository
        {
            get
            {
                if (offerRepository == null)
                {
                    offerRepository = new GenericRepository<Offer>(clownContext);
                }

                return this.offerRepository;
            }
        }

        public GenericRepository<Project> ProjectRepository
        {
            get
            {
                if (projectRepository == null)
                {
                    projectRepository = new GenericRepository<Project>(clownContext);
                }

                return this.projectRepository;
            }
        }

        public GenericRepository<ServiceProviderType> BusinessTypeRepository
        {
            get
            {
                if (businessTypeRepository == null)
                {
                    businessTypeRepository = new GenericRepository<ServiceProviderType>(clownContext);
                }

                return this.businessTypeRepository;
            }
        }

        public GenericRepository<CityMaster> CityRepository
        {
            get
            {
                if (cityRepository == null)
                {
                    cityRepository = new GenericRepository<CityMaster>(clownContext);
                }

                return this.cityRepository;
            }
        }

        public GenericRepository<AreaMaster> AreaRepository
        {
            get
            {
                if (areaRepository == null)
                {
                    areaRepository = new GenericRepository<AreaMaster>(clownContext);
                }

                return this.areaRepository;
            }
        }

        public GenericRepository<ServiceType> ServiceRepository
        {
            get
            {
                if (serviceRepository == null)
                {
                    serviceRepository = new GenericRepository<ServiceType>(clownContext);
                }

                return this.serviceRepository;
            }
        }

        public GenericRepository<CatlogProduct> CatlogRepository
        {
            get
            {
                if (catlogRepository == null)
                {
                    catlogRepository = new GenericRepository<CatlogProduct>(clownContext);
                }

                return this.catlogRepository;
            }
        }

        public GenericRepository<ProductMaster> ProductMasterRepository
        {
            get
            {
                if (productMasterRepository == null)
                {
                    productMasterRepository = new GenericRepository<ProductMaster>(clownContext);
                }

                return this.productMasterRepository;
            }
        }

        public GenericRepository<ServiceProvided> ServiceProvidedRepository
        {
            get
            {
                if (serviceProvidedRepository == null)
                {
                    serviceProvidedRepository = new GenericRepository<ServiceProvided>(clownContext);
                }

                return this.serviceProvidedRepository;
            }
        }

        public GenericRepository<WorkingHr> WorkingHrsRepository
        {
            get
            {
                if (workingHrsRepository == null)
                {
                    workingHrsRepository = new GenericRepository<WorkingHr>(clownContext);
                }

                return this.workingHrsRepository;
            }
        }

        public GenericRepository<ProjectImage> ProjectImageRepository
        {
            get
            {
                if (projectImageRepository == null)
                {
                    projectImageRepository = new GenericRepository<ProjectImage>(clownContext);
                }

                return this.projectImageRepository;
            }
        }

        public GenericRepository<Image> ImageRepository
        {
            get
            {
                if (imageRepository == null)
                {
                    imageRepository = new GenericRepository<Image>(clownContext);
                }

                return this.imageRepository;
            }
        }

        public GenericRepository<Review> ReviewRepository
        {
            get
            {
                if (reviewRepository == null)
                {
                    reviewRepository = new GenericRepository<Review>(clownContext);
                }

                return this.reviewRepository;
            }
        }

        public GenericRepository<WishList> WishListRepository
        {
            get
            {
                if (wishlistRepository == null)
                {
                    wishlistRepository = new GenericRepository<WishList>(clownContext);
                }

                return this.wishlistRepository;
            }
        }

        public GenericRepository<Product> ProductRepository
        {
            get
            {
                if (productRepository == null)
                {
                    productRepository = new GenericRepository<Product>(clownContext);
                }

                return this.productRepository;
            }
        }

        public GenericRepository<LineItem> LineItemRepository
        {
            get
            {
                if (lineItemRepository == null)
                {
                    lineItemRepository = new GenericRepository<LineItem>(clownContext);
                }

                return this.lineItemRepository;
            }
        }

        public GenericRepository<ProductDetail> ProductDetailRepository
        {
            get
            {
                if (productDetailRepository == null)
                {
                    productDetailRepository = new GenericRepository<ProductDetail>(clownContext);
                }

                return this.productDetailRepository;
            }
        }

        public GenericRepository<BundledProduct> BundledProductRepository
        {
            get
            {
                if (bundledProductRepository == null)
                {
                    bundledProductRepository = new GenericRepository<BundledProduct>(clownContext);
                }

                return this.bundledProductRepository;
            }
        }

        public GenericRepository<Subscription> SubsciptionRepository
        {
            get
            {
                if (subscriptionRepository == null)
                {
                    subscriptionRepository = new GenericRepository<Subscription>(clownContext);
                }

                return this.subscriptionRepository;
            }
        }

        public void Save()
        {
            try
            {
                clownContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        //Trace.TraceInformation("Property: {0} Error: {1}",
                                                //validationError.PropertyName,
                                                //validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
