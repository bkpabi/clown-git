﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ProductsEntities;
namespace Clown.Persistance.DataModel.ProductRepo
{
    public interface IProductRepository : IDisposable
    {
        IEnumerable<ProductViewModel> GetAllProducts();
        void Save();
    }
}
