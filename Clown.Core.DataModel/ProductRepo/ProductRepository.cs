﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ProductsEntities;
namespace Clown.Persistance.DataModel.ProductRepo
{
    public class ProductRepository : IProductRepository, IDisposable
    {
        ClownEntities clownContext;

        public ProductRepository(ClownEntities context)
        {
            this.clownContext = context;
        }
        public IEnumerable<ProductViewModel> GetAllProducts()
        {
            using (var clownContext = new ClownEntities())
            {
                List<ProductViewModel> ProductCollection = new List<ProductViewModel>();
                var result = clownContext.ProductMasters.ToList();
                foreach (var eachProductEntity in result)
                {
                    ProductViewModel entity = new ProductViewModel();
                    entity.ProductId = eachProductEntity.Id;
                    entity.ProductName = eachProductEntity.Name;

                    ProductCollection.Add(entity);
                }

                return ProductCollection;
            }
        }

        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
