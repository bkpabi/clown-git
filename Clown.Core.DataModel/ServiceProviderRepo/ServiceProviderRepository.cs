﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ServiceProviderEntities;
using System.Data.Entity.Validation;
using System.Diagnostics;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
namespace Clown.Persistance.DataModel.ServiceProviderRepo
{
    public class ServiceProviderRepository : IServiceProviderRepository, IDisposable
    {
        ClownEntities clownContext;

        public ServiceProviderRepository(ClownEntities context)
        {
            this.clownContext = context;
        }

        public IEnumerable<ServiceProvider> GetAllServiceProvider()
        {
            using (var clownContext = new ClownEntities())
            {
                var result = clownContext.ServiceProviders.Include("AreaMaster").Include("CityMaster").Include("Projects").Include("Projects.AreaMaster").Include("Reviews").Include("ServiceProvideds").Include("ServiceProvideds.ServiceType").Include("ServiceProviderType").Include("WorkingHrs").Include("Images").Include("Reviews.AspNetUser").Include("CatlogProducts").Include("CatlogProducts.ProductMaster").ToList();
                foreach (var item in result)
                {
                    foreach (var srvItem in item.ServiceProvideds)
                    {
                        srvItem.ServiceType = clownContext.ServiceTypes.FirstOrDefault(e => e.Id == srvItem.ServiceTypeId);
                    }

                    foreach (var catItem in item.CatlogProducts)
                    {
                        catItem.ProductMaster = clownContext.ProductMasters.FirstOrDefault(e => e.Id == catItem.ProductId);
                    }
                }

                return result;
            }
        }

        public ServiceProvider GetServiceProviderDetailsById(long ServiceProviderId)
        {
            using (var clownContext = new ClownEntities())
            {
                var result = clownContext.ServiceProviders.Include("AreaMaster").Include("CityMaster").Include("Offers").Include("Projects").Include("Projects.AreaMaster").Include("Reviews").Include("ServiceProvideds").Include("ServiceProvideds.ServiceType").Include("ServiceProviderType").Include("WorkingHrs").Include("Images").Include("Reviews.AspNetUser").Include("CatlogProducts").Include("CatlogProducts.ProductMaster").FirstOrDefault(e => e.Id == ServiceProviderId);
                foreach (var item in result.ServiceProvideds)
                {
                    item.ServiceType = clownContext.ServiceTypes.FirstOrDefault(e => e.Id == item.ServiceTypeId);
                }
                foreach (var item in result.CatlogProducts)
                {
                    item.ProductMaster = clownContext.ProductMasters.FirstOrDefault(e => e.Id == item.ProductId);
                }

                return result;
            }
        }

        public ServiceProvider GetServiceproviderDetailsByName(string ServiceProviderName)
        {
            using (var clownContext = new ClownEntities())
            {
                var result = clownContext.ServiceProviders.Include("AreaMaster").Include("CityMaster").Include("Projects").Include("Projects.AreaMaster").Include("Reviews").Include("ServiceProvideds").Include("ServiceProvideds.ServiceType").Include("ServiceProviderType").Include("WorkingHrs").Include("Images").Include("Reviews.AspNetUser").Include("CatlogProducts").Include("CatlogProducts.ProductMaster").FirstOrDefault(e => e.Name == ServiceProviderName);
                foreach (var item in result.ServiceProvideds)
                {
                    item.ServiceType = clownContext.ServiceTypes.FirstOrDefault(e => e.Id == item.ServiceTypeId);
                }
                foreach (var item in result.CatlogProducts)
                {
                    item.ProductMaster = clownContext.ProductMasters.FirstOrDefault(e => e.Id == item.ProductId);
                }

                return result;
            }
        }

        public IEnumerable<Image> GetAllImagesByServiceProviderId(long ServiceProviderId)
        {
            throw new NotImplementedException();
        }

        public long AddNewServiceProvider(ServiceProvider ServiceProviderEntity)
        {
            using (var clownContext = new ClownEntities())
            {
                using (var transaction = clownContext.Database.BeginTransaction())
                {
                    try
                    {
                        clownContext.ServiceProviders.Add(ServiceProviderEntity);
                        clownContext.SaveChanges();
                        transaction.Commit();
                        return ServiceProviderEntity.Id;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }

        }

        public bool UpdateServiceProvider(ServiceProvider ServiceProviderEntity)
        {
            using (var clownContext = new ClownEntities())
            {
                var dbServiceProvider = clownContext.ServiceProviders.Include("ServiceProvideds").FirstOrDefault(e => e.Id == ServiceProviderEntity.Id);
                if (dbServiceProvider != null)
                {
                    try
                    {
                        dbServiceProvider.Address_Area = ServiceProviderEntity.Address_Area;
                        dbServiceProvider.Address_City = ServiceProviderEntity.Address_City;
                        dbServiceProvider.Address_State = ServiceProviderEntity.Address_State;
                        dbServiceProvider.Address_Street = ServiceProviderEntity.Address_Street;
                        dbServiceProvider.Address_ZipCode = ServiceProviderEntity.Address_ZipCode;
                        dbServiceProvider.ContactNumber = ServiceProviderEntity.ContactNumber;
                        dbServiceProvider.InBusinessSince = ServiceProviderEntity.InBusinessSince;
                        dbServiceProvider.Lattitude = ServiceProviderEntity.Lattitude;
                        dbServiceProvider.Longitude = ServiceProviderEntity.Longitude;
                        dbServiceProvider.ModifiedBy = ServiceProviderEntity.ModifiedBy;
                        dbServiceProvider.ModifiedOn = ServiceProviderEntity.ModifiedOn;
                        dbServiceProvider.Name = ServiceProviderEntity.Name;
                        dbServiceProvider.PriceRange = ServiceProviderEntity.PriceRange;
                        dbServiceProvider.ServiceProviderTypeId = ServiceProviderEntity.ServiceProviderTypeId;
                        dbServiceProvider.Verified = ServiceProviderEntity.Verified;
                        dbServiceProvider.Website = ServiceProviderEntity.Website;
                        var dbServiceProvidedList = dbServiceProvider.ServiceProvideds.ToList();
                        foreach (var item in dbServiceProvidedList)
                        {
                            var x = ServiceProviderEntity.ServiceProvideds.FirstOrDefault(e => e.ServiceTypeId == item.ServiceTypeId);
                            if (x == null)
                            {
                                clownContext.ServiceProvideds.Remove(item);
                                //dbServiceProvider.ServiceProvideds.Remove(item);
                            }
                        }
                        var newServiceProvidedList = ServiceProviderEntity.ServiceProvideds.ToList();
                        foreach (var item in newServiceProvidedList)
                        {
                            var x = dbServiceProvider.ServiceProvideds.FirstOrDefault(e => e.ServiceTypeId == item.ServiceTypeId);
                            if (x == null)
                            {
                                clownContext.ServiceProvideds.Add(item);
                                //dbServiceProvider.ServiceProvideds.Add(item);
                            }
                        }

                        var workingHrs = ServiceProviderEntity.WorkingHrs.ToList();
                        foreach (var item in workingHrs)
                        {
                            var whr = clownContext.WorkingHrs.FirstOrDefault(e => e.WeekDay == item.WeekDay && e.ServiceProviderId == item.ServiceProviderId);
                            if (whr != null)
                            {
                                whr.StartTime = item.StartTime;
                                whr.EndTime = item.EndTime;
                                clownContext.SaveChanges();
                            }
                        }

                        clownContext.SaveChanges();

                        return true;
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Property: {0} Error: {1}",
                                                        validationError.PropertyName,
                                                        validationError.ErrorMessage);
                            }
                        }
                        throw dbEx;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool DeleteServiceProvider(long ServiceProviderId)
        {
            using (var clownContext = new ClownEntities())
            {
                var dbServiceProvider = clownContext.ServiceProviders.Include("WishLists").Include("Offers").Include("Projects").Include("Projects.ProjectImages").Include("Reviews").Include("ServiceProvideds").Include("WorkingHrs").Include("Images").Include("CatlogProducts").FirstOrDefault(e => e.Id == ServiceProviderId);
                if (dbServiceProvider != null)
                {
                    using (var transaction = clownContext.Database.BeginTransaction())
                    {
                        try
                        {
                            // Delete Working hours
                            dbServiceProvider.WorkingHrs.ToList().ForEach(p => clownContext.WorkingHrs.Remove(p));
                            // Delete Images
                            dbServiceProvider.Images.ToList().ForEach(p => clownContext.Images.Remove(p));
                            // Delete offers
                            dbServiceProvider.Offers.ToList().ForEach(p => clownContext.Offers.Remove(p));
                            // Delete Project Images
                            foreach (var item in dbServiceProvider.Projects)
                            {
                                foreach (var eachProjectImage in item.ProjectImages)
                                {
                                    string image = eachProjectImage.ImageLink.Split('/').Last();
                                    // Delete the image from CDN
                                    // Retrieve storage account from connection string.
                                    //System.Configuration.Assem
                                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse("StorageConnectionString");

                                    // Create the blob client.
                                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                                    // Retrieve reference to a previously created container.
                                    CloudBlobContainer container = blobClient.GetContainerReference("images");

                                    // Retrieve reference to a blob named "myblob.txt".
                                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

                                    // Delete the blob.
                                    blockBlob.Delete();
                                }
                                item.ProjectImages.ToList().ForEach(p => clownContext.ProjectImages.Remove(p));
                            }
                            //Remove projects
                            dbServiceProvider.Projects.ToList().ForEach(p => clownContext.Projects.Remove(p));
                            // Delete service offered
                            dbServiceProvider.ServiceProvideds.ToList().ForEach(p => clownContext.ServiceProvideds.Remove(p));
                            // Delete product catalog
                            dbServiceProvider.CatlogProducts.ToList().ForEach(p => clownContext.CatlogProducts.Remove(p));
                            // Delete Reviews
                            dbServiceProvider.Reviews.ToList().ForEach(p => clownContext.Reviews.Remove(p));
                            // Delete Wishlists
                            dbServiceProvider.WishLists.ToList().ForEach(p => clownContext.WishLists.Remove(p));

                            // Finally delete service provider
                            clownContext.ServiceProviders.Remove(dbServiceProvider);
                            clownContext.SaveChanges();

                            transaction.Commit();
                            return true;
                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            transaction.Rollback();
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    Trace.TraceInformation("Property: {0} Error: {1}",
                                                            validationError.PropertyName,
                                                            validationError.ErrorMessage);
                                }
                            }
                            throw dbEx;
                        }

                    }
                }
                else
                {
                    throw new ArgumentException("Invalid serviceproviderid");
                }
            }
        }

        #region - Search methods
        private IEnumerable<ServiceProviderListViewModel> FillServiceProviderViewModel(List<ServiceProviderResult> result)
        {
            List<ServiceProviderListViewModel> serviceProviderCollection = new List<ServiceProviderListViewModel>();
            using (var clownContext = new ClownEntities())
            {

                foreach (var eachServiceProvider in result)
                {
                    ServiceProviderListViewModel serviceProviderEntity = new ServiceProviderListViewModel();
                    serviceProviderEntity.Address = eachServiceProvider.Address_Street + ", " + eachServiceProvider.AreaMaster.AreaName + ", " + eachServiceProvider.CityMaster.CityName + ", " + eachServiceProvider.Address_State;
                    serviceProviderEntity.BusinessId = eachServiceProvider.Id;
                    serviceProviderEntity.BusinessName = eachServiceProvider.Name;
                    serviceProviderEntity.BusinessType = eachServiceProvider.SPType.Name;
                    serviceProviderEntity.InBusinessSince = eachServiceProvider.InBusinessSince;
                    serviceProviderEntity.IsVerified = eachServiceProvider.Verified;
                    serviceProviderEntity.OfferCount = eachServiceProvider.Offers.Count;
                    serviceProviderEntity.ProjectCount = eachServiceProvider.Projects.Count;
                    serviceProviderEntity.Longitude = eachServiceProvider.Longitude;
                    serviceProviderEntity.Lattitude = eachServiceProvider.Lattitude;
                    serviceProviderEntity.PriceRange = eachServiceProvider.PriceRange;
                    if (eachServiceProvider.Reviews.Sum(e => e.Ratting) > 0)
                    {
                        serviceProviderEntity.Ratting = eachServiceProvider.Reviews.Sum(e => e.Ratting) / eachServiceProvider.Reviews.Count;
                    }
                    serviceProviderEntity.ReviewCount = eachServiceProvider.Reviews.Count;

                    serviceProviderEntity.ServiceOffered = string.Empty;
                    if (eachServiceProvider.SPType.Name == "Interior Designers" || eachServiceProvider.SPType.Name == "Carpenters")
                    {
                        foreach (var sp in eachServiceProvider.Service)
                        {
                            serviceProviderEntity.ServiceOffered += ", " + clownContext.ServiceTypes.FirstOrDefault(e => e.Id == sp.ServiceTypeId).ServieTypeName;//. sp.ServiceType.ServieTypeName;
                        }
                    }
                    else
                    {
                        foreach (var item in eachServiceProvider.catlog)
                        {
                            serviceProviderEntity.ServiceOffered += ", " + clownContext.ProductMasters.FirstOrDefault(e => e.Id == item.ProductId).Name;
                        }
                    }


                    if (serviceProviderEntity.ServiceOffered.Length > 0)
                    {
                        serviceProviderEntity.ServiceOffered = serviceProviderEntity.ServiceOffered.Remove(0, 2);
                        if (serviceProviderEntity.ServiceOffered.Length > 100)
                        {
                            serviceProviderEntity.ServiceOffered = serviceProviderEntity.ServiceOffered.Substring(0, 100);
                            serviceProviderEntity.ServiceOffered = serviceProviderEntity.ServiceOffered + "...";
                        }
                    }

                    serviceProviderCollection.Add(serviceProviderEntity);
                }

                return serviceProviderCollection;
            }
        }
        /// <summary>
        /// Fetches the service providers list for a given city and service provider type
        /// </summary>
        /// <param name="BusinessType">Interior designers, Carpenters, Furniture Shop, Decore House</param>
        /// <param name="City">Bangalore, New Delhi</param>
        /// <returns></returns>
        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByCityByBusinessType(string BusinessType, string City)
        {
            using (var clownContext = new ClownEntities())
            {

                var result_ServiceProviderCollection = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("ServiceProvideds.ServiceType").Include("ServiceProvideds.ServiceProvider").Include("Offer").Include("Catlogs")
                                                        join q in clownContext.CityMasters on p.Address_City equals q.Id
                                                        join r in clownContext.ServiceProviderTypes on p.ServiceProviderTypeId equals r.Id
                                                        where q.CityName == City && r.Name == BusinessType
                                                        select new ServiceProviderResult() { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();


                return FillServiceProviderViewModel(result_ServiceProviderCollection);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByCityByService(string ServiceName, string City)
        {
            using (var clownContext = new ClownEntities())
            {
                var result = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("Offer")
                              join q in clownContext.ServiceProvideds on p.Id equals q.ServiceProviderId
                              join r in clownContext.ServiceTypes on q.ServiceTypeId equals r.Id
                              join b in clownContext.CityMasters on p.Address_City equals b.Id
                              where r.ServieTypeName == ServiceName && b.CityName == City
                              select new ServiceProviderResult { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();

                return FillServiceProviderViewModel(result);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByCityByProduct(string ProductName, string City)
        {
            using (var clownContext = new ClownEntities())
            {
                string[] serviceProviderType = new string[] { "Furniture Store", "Decor House" };
                var stc = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("Offer")
                           join r in clownContext.CatlogProducts on p.Id equals r.ServiceProviderId
                           join a in clownContext.ProductMasters on r.ProductId equals a.Id
                           join t in clownContext.CityMasters on p.Address_City equals t.Id
                           where serviceProviderType.Contains(p.ServiceProviderType.Name) && a.Name == ProductName && t.CityName == City
                           select new ServiceProviderResult { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();
                return FillServiceProviderViewModel(stc);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByAreaByBusinessType(string BusinessType, string Area)
        {
            using (var clownContext = new ClownEntities())
            {

                var spta = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("Offer")
                            join q in clownContext.CityMasters on p.Address_City equals q.Id
                            join s in clownContext.AreaMasters on p.Address_Area equals s.Id
                            join r in clownContext.ServiceProviderTypes on p.ServiceProviderTypeId equals r.Id
                            where r.Name == BusinessType && s.AreaName == Area
                            select new ServiceProviderResult { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();

                return FillServiceProviderViewModel(spta);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByAreaByService(string ServiceName, string Area)
        {
            using (var clownContext = new ClownEntities())
            {
                string areaName = Area.Split(',')[0];
                string cityName = Area.Split(',')[1].Remove(0, 1);
                var result = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("Offer")
                              join q in clownContext.ServiceProvideds on p.Id equals q.ServiceProviderId
                              join r in clownContext.ServiceTypes on q.ServiceTypeId equals r.Id
                              join a in clownContext.AreaMasters on p.Address_Area equals a.Id
                              join b in clownContext.CityMasters on p.Address_City equals b.Id
                              where r.ServieTypeName == ServiceName && a.AreaName == areaName && b.CityName == cityName
                              select new ServiceProviderResult { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();

                return FillServiceProviderViewModel(result);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByAreaByProduct(string ProductName, string Area)
        {
            using (var clownContext = new ClownEntities())
            {
                string areaName = Area.Split(',')[0];
                string cityName = Area.Split(',')[1].Remove(0, 1);
                string[] aserviceProviderType = new string[] { "Furniture Store", "Decor House" };
                var sta = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("Offer")
                           join r in clownContext.CatlogProducts on p.Id equals r.ServiceProviderId
                           join a in clownContext.ProductMasters on r.ProductId equals a.Id
                           join b in clownContext.AreaMasters on p.Address_Area equals b.Id
                           join t in clownContext.CityMasters on p.Address_City equals t.Id
                           //join s in context.ServiceTypes on q.ServiceTypeId equals s.Id
                           where aserviceProviderType.Contains(p.ServiceProviderType.Name) && a.Name == ProductName && b.AreaName == areaName && t.CityName == cityName
                           select new ServiceProviderResult { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();

                return FillServiceProviderViewModel(sta);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByNameByCity(string ServiceProviderName, string Location)
        {
            using (var clownContext = new ClownEntities())
            {

                var result_ServiceProviderCollection = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("ServiceProvideds.ServiceType").Include("ServiceProvideds.ServiceProvider").Include("Offer").Include("Catlogs")
                                                        join q in clownContext.CityMasters on p.Address_City equals q.Id
                                                        join r in clownContext.ServiceProviderTypes on p.ServiceProviderTypeId equals r.Id
                                                        where q.CityName == Location && p.Name == ServiceProviderName
                                                        select new ServiceProviderResult() { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();


                return FillServiceProviderViewModel(result_ServiceProviderCollection);
            }
        }

        public IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByNameByArea(string ServiceProviderName, string Location)
        {
            using (var clownContext = new ClownEntities())
            {

                var spta = (from p in clownContext.ServiceProviders.Include("Reviews").Include("Projects").Include("AreaMaster").Include("CityMaster").Include("ServiceProvideds").Include("Offer")
                            join q in clownContext.CityMasters on p.Address_City equals q.Id
                            join s in clownContext.AreaMasters on p.Address_Area equals s.Id
                            join r in clownContext.ServiceProviderTypes on p.ServiceProviderTypeId equals r.Id
                            where p.Name == ServiceProviderName && s.AreaName == Location
                            select new ServiceProviderResult { Id = p.Id, PriceRange = p.PriceRange, AreaMaster = p.AreaMaster, Name = p.Name, Website = p.Website, CityMaster = p.CityMaster, InBusinessSince = p.InBusinessSince, Address_ZipCode = p.Address_ZipCode, Address_Street = p.Address_Street, Address_State = p.Address_State, Lattitude = p.Lattitude, Longitude = p.Longitude, Projects = p.Projects, Reviews = p.Reviews, Service = p.ServiceProvideds, Verified = p.Verified.Value, Offers = p.Offers, SPType = p.ServiceProviderType, catlog = p.CatlogProducts }).ToList();

                return FillServiceProviderViewModel(spta);
            }
        }
        #endregion

        #region - City & Area
        public IEnumerable<CityMaster> GetAllCities()
        {
            using (var context = new ClownEntities())
            {
                return context.CityMasters.Include("AreaMasters").ToList();
            }
        }

        public IEnumerable<AreaMaster> GetAllAreas()
        {
            using (var clownContext = new ClownEntities())
            {
                var dbAreas = clownContext.AreaMasters.ToList();
                return dbAreas;
            }
        }

        public IEnumerable<AreaMaster> GetAearByCity(string CityName)
        {
            List<AreaMaster> areaCollection = new List<AreaMaster>();
            using (var context = new ClownEntities())
            {
                var city = context.CityMasters.FirstOrDefault(e => e.CityName == CityName);
                if (city != null)
                {
                    areaCollection = context.AreaMasters.Include("CityMaster").Where(e => e.CityId == city.Id).ToList();
                }

            }
            return areaCollection;
        }

        public IEnumerable<AreaMaster> GetAreaByCityId(long CityId)
        {
            List<AreaMaster> areaCollection = null;
            using (var context = new ClownEntities())
            {
                areaCollection = context.AreaMasters.Include("CityMaster").Where(e => e.CityId == CityId).ToList();
            }
            return areaCollection;
        }
        #endregion
        public IEnumerable<ProductMaster> GetAllProducts()
        {
            using (var clownContext = new ClownEntities())
            {
                return clownContext.ProductMasters.ToList();
            }
        }

        public IEnumerable<ServiceType> GetAllServiceTypes()
        {
            using (var context = new ClownEntities())
            {
                return context.ServiceTypes.ToList();
            }
        }

        public IEnumerable<ServiceProviderType> GetAllServiceProviderTypes()
        {
            using (var context = new ClownEntities())
            {
                return context.ServiceProviderTypes.ToList();
            }
        }


        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class ServiceProviderResult
    {
        public ServiceProviderResult()
        {
            AreaMaster = new AreaMaster();
            CityMaster = new CityMaster();
            Projects = new List<Project>();
            Reviews = new List<Review>();
            Service = new List<ServiceProvided>();
            Offers = new List<Offer>();
            catlog = new List<CatlogProduct>();
            SPType = new ServiceProviderType();
        }
        public long Id { get; set; }

        public AreaMaster AreaMaster { get; set; }

        public string Name { get; set; }

        public string Website { get; set; }

        public CityMaster CityMaster { get; set; }

        public int? InBusinessSince { get; set; }

        public decimal? PriceRange { get; set; }

        public string Address_ZipCode { get; set; }

        public string Address_Street { get; set; }

        public string Address_State { get; set; }

        public string Lattitude { get; set; }

        public string Longitude { get; set; }

        public ICollection<Project> Projects { get; set; }

        public ICollection<Review> Reviews { get; set; }

        public ICollection<ServiceProvided> Service { get; set; }

        public ICollection<Offer> Offers { get; set; }

        public bool Verified { get; set; }

        public ICollection<CatlogProduct> catlog { get; set; }

        public ServiceProviderType SPType { get; set; }


    }
}
