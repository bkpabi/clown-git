﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ServiceProviderEntities;
namespace Clown.Persistance.DataModel.ServiceProviderRepo
{
    public interface IServiceProviderRepository : IDisposable
    {
        IEnumerable<ServiceProvider> GetAllServiceProvider();

        ServiceProvider GetServiceProviderDetailsById(long ServiceProviderId);

        ServiceProvider GetServiceproviderDetailsByName(string ServiceProviderName);

        long AddNewServiceProvider(ServiceProvider ServiceProviderEntity);

        bool UpdateServiceProvider(ServiceProvider ServiceProviderEntity);

        bool DeleteServiceProvider(long ServiceProviderId);
        

        #region - Search
        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByCityByBusinessType(string BusinessType, string City);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByCityByService(string ServiceName, string City);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByCityByProduct(string ProductName, string City);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByAreaByBusinessType(string BusinessType, string Area);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByAreaByService(string ServiceName, string Area);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByAreaByProduct(string ProductName, string Area);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByNameByCity(string ServiceProviderName, string Location);

        IEnumerable<ServiceProviderListViewModel> GetServiceProvidersByNameByArea(string ServiceProviderName, string Location);
        #endregion
        
        #region- City & Area
        IEnumerable<CityMaster> GetAllCities();

        IEnumerable<DataModel.AreaMaster> GetAllAreas();

        IEnumerable<AreaMaster> GetAearByCity(string CityName);

        IEnumerable<AreaMaster> GetAreaByCityId(long CityId);
        #endregion
        IEnumerable<ProductMaster> GetAllProducts();

        IEnumerable<ServiceType> GetAllServiceTypes();

        IEnumerable<ServiceProviderType> GetAllServiceProviderTypes();

        IEnumerable<Image> GetAllImagesByServiceProviderId(long ServiceProviderId);

        void Save();
    }
}
