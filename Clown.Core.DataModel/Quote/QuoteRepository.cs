﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.Quote;

namespace Clown.Persistance.DataModel.Quote
{
    public class QuoteRepository: IQuoteRepository, IDisposable
    {
        ClownEntities clownContext;

        public QuoteRepository(ClownEntities context)
        {
            this.clownContext = context;
        }
        public bool RequestQuote(InteriorRequirment interiorRequrimentEntity)
        {
            using (var clownContext = new ClownEntities())
            {
                clownContext.InteriorRequirments.Add(interiorRequrimentEntity);
                clownContext.SaveChanges();
                return true;
            }
        }

        public IEnumerable<TrackQuoteRequestViewModel> GetRequestsByUser(string UserId)
        {
            using (var clownContext = new ClownEntities())
            {
                List<TrackQuoteRequestViewModel> requestCollection = new List<TrackQuoteRequestViewModel>();
                var requestDbCollection = clownContext.InteriorRequirments.Where(e => e.UserId == UserId && e.IsActive == true).ToList();
                foreach (var item in requestDbCollection)
                {
                    TrackQuoteRequestViewModel requestEntity = new TrackQuoteRequestViewModel();
                    requestEntity.QuotesSubmited = clownContext.InteriorDesignQuotes.Where(e => e.RequestId == item.Id).Count();
                    requestEntity.RequestDate = item.RequestDate;
                    requestEntity.RequestId = item.Id;
                    requestEntity.TotalViews = 0; // TODO: Change it once request view func is implemented.
                    requestCollection.Add(requestEntity);
                }

                return requestCollection;
            }
        }

        public IEnumerable<SubmitedQuoteViewModel> GetQuotesByRequest(long RequestId)
        {
            using (var clownContext = new ClownEntities())
            {
                List<SubmitedQuoteViewModel> quoteCollection = new List<SubmitedQuoteViewModel>();
                var quoteDbCollection = clownContext.InteriorDesignQuotes.Where(e => e.RequestId == RequestId).ToList();
                foreach (var quoteItem in quoteDbCollection)
                {
                    SubmitedQuoteViewModel quoteEntity = new SubmitedQuoteViewModel();
                    quoteEntity.Quote = quoteItem.QuoteLink;
                    quoteEntity.QuoteAmount = quoteItem.QuoteAmount;
                    quoteEntity.QuoteDate = quoteItem.QuoteDate;
                    quoteEntity.QuoteId = quoteItem.Id;
                    quoteItem.RequestId = quoteItem.RequestId;
                    quoteEntity.ValidTill = quoteItem.ValidTill;
                    quoteCollection.Add(quoteEntity);
                }

                return quoteCollection;
            }
        }

        public bool DeleteRequest(long RequestId)
        {
            using (var clownContext = new ClownEntities())
            {
                var requestEntity = clownContext.InteriorRequirments.Where(e => e.Id == RequestId).FirstOrDefault();
                if (requestEntity == null)
                {
                    throw new ArgumentException("There is no request for the given request id", "RequestId");
                }

                requestEntity.IsActive = false;
                clownContext.SaveChanges();
                return true;
            }
        }


        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
