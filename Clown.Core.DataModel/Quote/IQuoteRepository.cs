﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.Quote;

namespace Clown.Persistance.DataModel.Quote
{
    public interface IQuoteRepository : IDisposable
    {
        bool RequestQuote(InteriorRequirment interiorRequrimentEntity);

        IEnumerable<TrackQuoteRequestViewModel> GetRequestsByUser(string UserId);

        IEnumerable<SubmitedQuoteViewModel> GetQuotesByRequest(long RequestId);

        bool DeleteRequest(long RequestId);

        void Save();
    }
}
