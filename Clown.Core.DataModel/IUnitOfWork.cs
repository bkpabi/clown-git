﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Persistance.DataModel
{
    public interface IUnitOfWork : IDisposable
    {
        GenericRepository<ServiceProvider> BusinessRepository { get; }
        GenericRepository<Offer> OfferRepository { get; }
        GenericRepository<Project> ProjectRepository { get;}
        GenericRepository<ServiceProviderType> BusinessTypeRepository { get; }
        GenericRepository<CityMaster> CityRepository { get; }
        GenericRepository<AreaMaster> AreaRepository{get;}
        GenericRepository<ServiceType> ServiceRepository { get; }
        GenericRepository<CatlogProduct> CatlogRepository { get; }
        GenericRepository<ProductMaster> ProductMasterRepository { get; }
        GenericRepository<ServiceProvided> ServiceProvidedRepository { get; }
        GenericRepository<WorkingHr> WorkingHrsRepository { get; }
        GenericRepository<ProjectImage> ProjectImageRepository { get; }
        GenericRepository<Image> ImageRepository { get; }
        GenericRepository<Review> ReviewRepository { get; }
        GenericRepository<WishList> WishListRepository { get; }
        GenericRepository<Product> ProductRepository { get; }
        GenericRepository<LineItem> LineItemRepository { get; }
        GenericRepository<ProductDetail> ProductDetailRepository { get; }
        GenericRepository<BundledProduct> BundledProductRepository { get; }
        GenericRepository<Subscription> SubsciptionRepository { get; }

        void Save();
    }
}
