﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clown.Business.Entities.ProjectsEntities;
namespace Clown.Persistance.DataModel.ProjectRepo
{
    public interface IProjectRepository : IDisposable
    {
        #region-- Get
        IEnumerable<Project> GetProjectsByServiceProviderId(long ServiceProviderId);

        IEnumerable<Project> GetProjectsByServiceProviderName(string ServiceProviderName);

        Project GetProjectDetailsByProjectId(long ProjectId);

        Project GetProjectDetailsByProjectName(string ProjectName);

        IEnumerable<ProjectImage> GetProjectImagesByProject(long ProjectId);

        ProjectImage GetProjectImageByid(long id);
        #endregion

        #region -- Add
        long AddNewProject(Project ProjectEntity);
        long AddProjectImage(ProjectImage ProjectImageEntity);
        #endregion

        #region -- Modify
        bool UpdateProject(Project projectEntity);
        #endregion

        #region-- delete
        bool DeleteProject(long ProjectId);
        bool DeleteProjectImage(long ProjectImageId);
        #endregion
        void Save();
    }
}
