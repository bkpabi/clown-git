﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Clown.Persistance.DataModel.ProjectRepo
{
    public class ProjectRepository : IProjectRepository, IDisposable
    {
        ClownEntities clownContext;
        public ProjectRepository(ClownEntities context)
        {
            this.clownContext = context;
        }

        public IEnumerable<Project> GetProjectsByServiceProviderId(long ServiceProviderId)
        {
            using (var clownContext = new ClownEntities())
            {

                var dbProjectsCollection = clownContext.Projects.Include("ProjectImages").Include("AreaMaster").Include("AreaMaster.CityMaster").Where(e => e.ServiceProviderId == ServiceProviderId).ToList();
                return dbProjectsCollection;

            }
        }

        public IEnumerable<Project> GetProjectsByServiceProviderName(string ServiceProviderName)
        {
            throw new NotImplementedException();
        }

        public Project GetProjectDetailsByProjectId(long ProjectId)
        {
            using (var clownContext = new ClownEntities())
            {
                var projectEntity = clownContext.Projects.Include("ServiceProvider").Include("ProjectImages").FirstOrDefault(e => e.Id == ProjectId);
                return projectEntity;
            }
        }

        public Project GetProjectDetailsByProjectName(string ProjectName)
        {
            using (var clownContext = new ClownEntities())
            {
                var projectEntity = clownContext.Projects.Include("ProjectImages").FirstOrDefault(e => e.ProjectTittle == ProjectName);
                return projectEntity;
            }
        }

        public IEnumerable<ProjectImage> GetProjectImagesByProject(long ProjectId)
        {
            using (var clownContext = new ClownEntities())
            {
                var dbProjectImagesCollection = clownContext.ProjectImages.Where(e => e.ProjectId == ProjectId).ToList();
                return dbProjectImagesCollection;
            }
        }

        public ProjectImage GetProjectImageByid(long id)
        {
            using (var clownContext = new ClownEntities())
            {
                var projectImageEntity = clownContext.ProjectImages.FirstOrDefault(e => e.Id == id);
                return projectImageEntity;
            }
        }

        public long AddNewProject(Project ProjectEntity)
        {
            using (var clownContext = new ClownEntities())
            {
                // do some validation if required
                clownContext.Projects.Add(ProjectEntity);
                clownContext.SaveChanges();
                return ProjectEntity.Id;
            }
        }

        public bool DeleteProject(long ProjectId)
        {
            using (var clownContext = new ClownEntities())
            {
                using (var transaction = clownContext.Database.BeginTransaction())
                {
                    try
                    {
                        var dbProject = clownContext.Projects.Include("ProjectImages").FirstOrDefault(e => e.Id == ProjectId);
                        if (dbProject != null)
                        {
                            foreach (var item in dbProject.ProjectImages)
                            {
                                clownContext.ProjectImages.Remove(item);
                            }
                            clownContext.SaveChanges();
                            clownContext.Projects.Remove(dbProject);
                            clownContext.SaveChanges();
                            transaction.Commit();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }

            }
        }

        public long AddProjectImage(ProjectImage ProjectImageEntity)
        {
            using (var clownContext = new ClownEntities())
            {
                clownContext.ProjectImages.Add(ProjectImageEntity);
                clownContext.SaveChanges();
                return ProjectImageEntity.Id;
            }
        }

        public bool DeleteProjectImage(long ProjectImageId)
        {
            using (var clownContext = new ClownEntities())
            {
                var dbProjectImage = clownContext.ProjectImages.FirstOrDefault(e => e.Id == ProjectImageId);
                if (dbProjectImage != null)
                {
                    clownContext.ProjectImages.Remove(dbProjectImage);
                    clownContext.SaveChanges();
                    return true;
                }
                else
                {
                    throw new ArgumentException("Invalid project image id .");
                }
            }
        }

        public bool UpdateProject(Project projectEntity)
        {
            using (var clownContext = new ClownEntities())
            {
                var dbProjectEntity = clownContext.Projects.FirstOrDefault(e => e.Id == projectEntity.Id);
                if (dbProjectEntity != null)
                {
                    dbProjectEntity.Area = projectEntity.Area;
                    dbProjectEntity.Cost = projectEntity.Cost;
                    dbProjectEntity.HouseType = projectEntity.HouseType;
                    dbProjectEntity.ModifiedBy = projectEntity.ModifiedBy;
                    dbProjectEntity.ModifiedOn = projectEntity.ModifiedOn;
                    dbProjectEntity.NoOfDaysTaken = projectEntity.NoOfDaysTaken;
                    dbProjectEntity.ProjectTittle = projectEntity.ProjectTittle;
                    dbProjectEntity.Status = projectEntity.Status;
                    clownContext.SaveChanges();

                    return true;
                }
                else
                {
                    throw new ArgumentException("Invalid project details.");
                }

            }
        }


        public void Save()
        {
            clownContext.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    clownContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
