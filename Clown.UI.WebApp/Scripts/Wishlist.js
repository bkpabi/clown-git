﻿var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    initilizeLoginModal();
    PopulateWishList();

});
function initilizeLoginModal() {
    $("#loginformContainer").show();
    $("#loginright").show();
    $("#SignupformContainer").hide();
    $("#signupright").hide();
    $("#ForgotformContainer").hide();
}

function RemoveFromWishList(businessId) {
    $.ajax({
        url: Refurl + "/api/WishList/DeleteWish/" + businessId,
        type: 'DELETE',
        dataType: 'json',
        success: function (data) {
            if (data == "Wish deleted successfully") {
                PopulateWishList();
                GetWishList();
            }
            else {
                //Show error message
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function PopulateWishList() {
    $.ajax({
        url: Refurl + "/api/WishList/GetWishListForAUser",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#data-list').empty();
            if (data.length == 0) {
                $("#ajax").hide();
                $("#message").html("There are no shortlisted business.");
                $("#message").show();
                $('#data-list').hide();
                $("div.holder").hide();
            }
            else {
                $("#message").html("");
                $("#message").hide();
                for (var key in data) {
                    var url = Refurl + "/home/BusinessDetails?spid=" + data[key].BusinessId;
                    var verified;
                    if (data[key].IsVerified == true) {
                        verified = '<div class="verification"><span style="margin-right:3px;">Verified</span><span class="glyphicon glyphicon-heart" style="color:#2c6c2c; font-size:16px;"></span></div>';
                    }
                    else {
                        verified = '';
                    }
                    //<img src="/Content/Images/1429632319_heart-32.png" height="20" width="20" />
                    if (data[key].BusinessType == "Interior Designers" || data[key].BusinessType == "Carpenters") {
                        $('#data-list').append('<li><div style="height:25px;"><div class="pull-left"><span class="businessName"><a href="' + url + '">' + data[key].BusinessName + '&nbsp;</a></span><span class="tags">' + data[key].BusinessType + '</span></div><div class="pull-right"><div class="ratting">' + verified + '<div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div><div class="ratting-number">' + data[key].Ratting + '</div><div class="delete" data-toggle="tooltip" data-original-title="Remove from wishlist"><input type="hidden" value="' + data[key].WishId + '"></input><span class="glyphicon glyphicon-trash"></span></div></div></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceOffered + '</div><div class="address"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].ProjectCount + '</span><div style="color:#808080; font-size:12px; display:inline-block;" class="text-center">Projects</div></div><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].InBusinessSince + '</span><span style="color:#808080; font-size:12px; width:100px; display:inline-block;" class="text-center">In Business Since</span></div><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].OfferCount + '</span><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;" class="text-center">Offers</span></div></div></li>');
                    }
                    else {
                        $('#data-list').append('<li><div style="height:25px;"><div class="pull-left"><span class="businessName"><a href="' + url + '">' + data[key].BusinessName + '&nbsp;</a></span><span class="tags">' + data[key].BusinessType + '</span></div><div class="pull-right"><div class="ratting">' + verified + '<div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div><div class="ratting-number">' + data[key].Ratting + '</div><div class="delete" data-toggle="tooltip" data-original-title="Remove from wishlist"><input type="hidden" value="' + data[key].WishId + '"></input><span class="glyphicon glyphicon-trash"></span></div></div></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceOffered + '</div><div class="address"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">'+ data[key].InBusinessSince+'</span><span style="color:#808080; font-size:12px; width:100px; display:inline-block;" class="text-center">In Business Since</span></div><div style="float:left;"><div style="float:left;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].OfferCount + '</span><span style="color:#808080; font-size:12px; display:inline-block;" class="text-center">Offers</span></div></div></li>');
                    }

                }
                $(".delete").on("click", function (event) {
                    RemoveFromWishList($(this).children("input").val());
                });
                $("#ajax").hide();
                $(function () {
                    /* initiate plugin */
                    $("div.holder").jPages({
                        containerID: "data-list",
                        perPage: 4,
                        fallback: 500,
                        scrollBrowse: true,
                        keyBrowse: true
                        //animation: "rotatein"
                    });
                });
            }

        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
