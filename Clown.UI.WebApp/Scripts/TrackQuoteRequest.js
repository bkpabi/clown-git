﻿var QuoteRequest;
var Quote;
var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    //initilizeLoginModal();
    QuoteRequest = $('#TblQuotesRequested').DataTable({
        "bLengthChange": false,
        "columns": [
          null,
          null,
          null,
          null,
          { "width": "50px", sClass: "text-center" }
        ]
    });
    Quote = $('#TblQuotes').DataTable({
        "bLengthChange": false,
        "columns": [
          null,
          null,
          null,
          null,
          { "width": "50px", sClass: "text-center" }
        ]
    });
    GetRequests();
    $('#TblQuotesRequested').on('click', 'td:nth-child(2)', function () {
        $("#HeadingQuoutes").html("Quotes for Request id: " + $(this).text());
        GetQuotes($(this).text());
    });

    $('#TblQuotesRequested').on('click', 'td:nth-child(5)', function () {
        //alert($(this).parent().find("td").eq(1).text());
        $("#hdnDeleteRequestId").val($(this).parent().find("td").eq(1).text());
        $('#delete').modal('show');
    });

    $('#btnDeleteRequest').click(function () {
        $.ajax({
            url: "http://localhost:1406/api/Quote/DeleteRequest/" + $("#hdnDeleteRequestId").val(),
            cache: false,
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                if (data == "Request deleted successfully") {
                    GetRequests();
                    $('#delete').modal('hide');
                }
                else if (data == "Sorry, We are unable to delete the request.") {
                    $("#DRerror-message").html("Sorry, We are unable to delete the request..");
                    $("#DRerror-message").css("color", "Red");
                }
                //else {
                //    GetWishList();
                //    $("#wishlisticon").notify("Item added to wishlsit", "success", { position: "bottom center", arrowShow: true, gap: 3 });
                //}
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });
});

function initilizeLoginModal() {
    //$("#loginformContainer").show();
    //$("#loginright").show();
    //$("#SignupformContainer").hide();
    //$("#signupright").hide();
    //$("#ForgotformContainer").hide();
}

function GetRequests() {
    $.ajax({
        url: Refurl + "/api/Quote/GetRequestsByUserId",
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            QuoteRequest.clear().draw();
            for (var key in data) {
                QuoteRequest.row.add([$.datepicker.formatDate('dd M yy', new Date(data[key].RequestDate)), '<span class="link">' + data[key].RequestId + '</span>', data[key].TotalViews, data[key].QuotesSubmited, '<span style="color:#1c9fda;" class="glyphicon glyphicon-trash"></span>']).draw();
            };
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function GetQuotes(RequestId) {
    $.ajax({
        url: Refurl + "/api/Quote/GetQuotesByRequest?RequestId=" + RequestId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            Quote.clear().draw();
            for (var key in data) {
                Quote.row.add([$.datepicker.formatDate('dd M yy', new Date(data[key].QuoteDate)), data[key].QuoteId, data[key].QuoteAmount, $.datepicker.formatDate('dd M yy', new Date(data[key].ValidTill)), '<a href="Download?file=' + data[key].Quote + '" target="_blank"><span class="glyphicon glyphicon-download-alt"></span></a>']).draw();
            };
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

//function GetWishList() {
//    $.ajax({
//        url: "http://localhost:1406/api/WishList/GetWishListForAUser",
//        type: 'GET',
//        dataType: 'json',
//        success: function (data) {
//            $(".baddge").html(data.length);
//            $("ul.dropdown-cart").empty();
//            for (var key in data) {
//                $("ul.dropdown-cart").append('<li><span class="item"><span class="item-left"><span class="item-info"><span style="font-size:13px;">' + data[key].ServiceProviderName + '</span><span style="font-size:11px; color:#ccc;">' + data[key].ServiceProviderType + '</span></span></span><span class="item-right"><button class="btn btn-xs btn-danger pull-right">x</button></span></span></li>');
//            };

//            $("ul.dropdown-cart").append('<li class="divider"></li>');
//            $("ul.dropdown-cart").append('<li><a class="text-center" href="" style="color:#808080;">VIEW WISHLIST</a></li>');
//        },
//        error: function (x, y, z) {
//            alert(x + '\n' + y + '\n' + z);
//        }
//    });
//}

//TODO: Delete Request
