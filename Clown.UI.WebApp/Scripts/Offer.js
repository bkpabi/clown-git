﻿var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net/";
$(document).ready(function () {
    GetOffers();
});

function GetOffers() {
    $.ajax({
        url: Refurl + 'api/Offer/GetOffersByServiceProvider/' + $("#spid").val(),
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (offerList) {
            for (var key in offerList) {
                var businessName = null;
                if (offerList[key].BusinessName.length > 20) {
                    offerList[key].BusinessName = offerList[key].BusinessName.substring(0, 20) + "...";
                }
                $("#OfferContainer").append('<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><div class="offer offer-default"><div class="offer-content"><div style="margin-top:5px;font-size:20px; font-weight:500; margin-bottom:5px;color:#1fadbb;">' + offerList[key].BusinessName + '</div><p id="offerTittle"style="color:#f8b019;" ><span class="glyphicon glyphicon-gift"></span> ' + offerList[key].OfferTittle + '</p><p style="min-height:30px;"> ' + offerList[key].OfferDescription + '</p><div class="row" ><div class="col-md-12 post-header-line" style="font-size:12px; margin-top:10px; color:#808080;"><div class="text-left" style="display:inline-block;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Valid till <br /><strong>' + $.datepicker.formatDate('dd M, yy', new Date(offerList[key].ValidTill)) + '</strong></div><div class="text-left" style="display:inline-block; margin-left:15px;"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Views<br /><strong>' + offerList[key].Views + '</strong></div></div></div></div></div></div>');
            }
            $("span.search-resultCount").html(offerList.length + " Offers");
            GetServiceProviderDetails();
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}


function GetServiceProviderDetails() {
    $.ajax({
        url: Refurl + "/api/ServiceProvider/GetServiceProviderDetails?spId=" + $("#spid").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $(".search-nav").html('<a href="' + Refurl + '">Home</a> / <a href="' + Refurl + '/Home/BusinessDetails?spid=' + $("#spid").val() + '">' + data.Name + '</a>');
            $(".search-header").text(data.Name + " - Offers");
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}