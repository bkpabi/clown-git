﻿var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    $('#rootwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var decimalRegEx = /^((\d+)((\.\d{1,2})?))$/;

            if (index == 1) {
                var RQHouseCarpetArea = $("#RQHouseCarpetArea").val();
                if (RQHouseCarpetArea == "") {
                    $("#RQerror-message").html("Please provide carpet area");
                    $("#RQerror-message").css("color", "Red");
                    $("#RQHouseCarpetArea").focus();
                    return false;
                }
                else if (!decimalRegEx.test(RQHouseCarpetArea)) {
                    $("#RQerror-message").html("Please provide decimal value for carpet area");
                    $("#RQerror-message").css("color", "Red");
                    $("#RQHouseCarpetArea").focus();
                    return false;
                }
                else {
                    $("#RQerror-message").html("");
                }
            }
            else if (index == 2) {
                var RQKitchenWidth = $("#RQKitchenWidth").val();
                var RQKitchenHeight = $("#RQKitchenHeight").val();
                if ($("#RQKitchenType").val() == "1") {
                    if (RQKitchenWidth == "") {
                        $("#RQerror-message").html("Please provide kithen width");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQKitchenWidth").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQKitchenWidth)) {
                        $("#RQerror-message").html("Please provide decimal value for kitchen width");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQKitchenWidth").focus();
                        return false;
                    }
                    else if ($("#RQKitchenHeight").val() == "") {
                        $("#RQerror-message").html("Please provide kithen height");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQKitchenHeight").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQKitchenHeight)) {
                        $("#RQerror-message").html("Please provide decimal value for kithen height");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQKitchenHeight").focus();
                        return false;
                    }
                }

                $("#RQerror-message").html("");
            }
            else if (index == 3) {
                var RQLVTVWidth = $("#RQLVTVWidth").val();
                var RQLVTVHeight = $("#RQLVTVHeight").val();
                var RQLVCUWidth = $("#RQLVCUWidth").val();
                var RQLVCUHeight = $("#RQLVCUHeight").val();
                var RQLVSRWidth = $("#RQLVSRWidth").val();
                var RQLVSRHeight = $("#RQLVSRHeight").val();
                var RQLVFSArea = $("#RQLVFSArea").val();
                var RQLVFSDesign = $("#RQLVFSDesign").val();
                var RQLVPUSize = $("#RQLVPUSize").val();

                if ($("#RQLVTVUnitChkbx").is(':checked') == true) {
                    if (RQLVTVWidth == "") {
                        $("#RQerror-message").html("Please provide width of living room TV Unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVTVWidth").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVTVWidth)) {
                        $("#RQerror-message").html("Please provide decimal value for width of living room TV Unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVTVWidth").focus();
                        return false;
                    }
                    else if (RQLVTVHeight == "") {
                        $("#RQerror-message").html("Please provide height of living room TV Unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVTVHeight").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVTVHeight)) {
                        $("#RQerror-message").html("Please provide decimal value for height of living room TV Unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVTVHeight").focus();
                        return false;
                    }
                }

                if ($("#RQLVCUChkbx").is(':checked') == true) {
                    if (RQLVCUWidth == "") {
                        $("#RQerror-message").html("Please provide width of living room crockery unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVCUWidth").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVCUWidth)) {
                        $("#RQerror-message").html("Please provide decimal value for width of living room crockery unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVCUWidth").focus();
                        return false;
                    }
                    else if (RQLVCUHeight == "") {
                        $("#RQerror-message").html("Please provide height of living room crockery unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVCUHeight").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVCUHeight)) {
                        $("#RQerror-message").html("Please provide decimal value for height of living room crockery unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVCUHeight").focus();
                        return false;
                    }
                }

                if ($("#RQLVSRChkbx").is(':checked') == true) {
                    if (RQLVSRWidth == "") {
                        $("#RQerror-message").html("Please provide width of living room shoe rack");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVSRWidth").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVSRWidth)) {
                        $("#RQerror-message").html("Please provide decimal value for width of living room shoe rack");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVSRWidth").focus();
                        return false;
                    }
                    else if (RQLVSRHeight == "") {
                        $("#RQerror-message").html("Please provide height of living room shoe rack");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVSRHeight").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVSRHeight)) {
                        $("#RQerror-message").html("Please provide decimal value for height of living room shoe rack");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVSRHeight").focus();
                        return false;
                    }
                }

                if ($("#RQLVFSChkbx").is(':checked') == true) {
                    if (RQLVFSArea == "") {
                        $("#RQerror-message").html("Please provide area of living room false celling");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVFSArea").focus();
                        return false;
                    }
                    else if (!decimalRegEx.test(RQLVFSArea)) {
                        $("#RQerror-message").html("Please provide decimal value for area of living room false celling");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVFSArea").focus();
                        return false;
                    }
                    else if (RQLVFSDesign == "-1") {
                        $("#RQerror-message").html("Please select a design of living room False Celling");
                        $("#RQLVFSDesign").focus();
                        $("#RQerror-message").css("color", "Red");
                        return false;
                    }

                }

                if ($("#RQLVPUChkbx").is(':checked') == true) {
                    if (RQLVPUSize == "-1") {
                        $("#RQerror-message").html("Please select a size of living room puja unit");
                        $("#RQerror-message").css("color", "Red");
                        $("#RQLVPUSize").focus();
                        return false;
                    }
                }

                $("#RQerror-message").html("");
            }
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index + 1;

            //var $percent = ($current / $total) * 100;
            //$('#rootwizard').find('.bar').css({ width: $percent + '%' });

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });

    $('#rootwizard .finish').click(function () {
        if (ValidateQuoteRequest()) {

            //build the object
            var InteriorRequirment = {
                RoomType: $("#RQHouseType option:selected").text(), CarpetArea: $("#RQHouseCarpetArea").val(),
                KitchenShape: $("#RQKitchenType option:selected").text(), KitchenWidth: $("#RQKitchenWidth").val(), KitchenHeight: $("#RQKitchenHeight").val(), KitchenRequriments: $("#RQKitchenReq").val(),
                LivingRoomTVUnit: $("#RQLVTVUnitChkbx").is(':checked'), LivingRoomTVUnitWidth: $("#RQLVTVWidth").val(), LivingRoomTVUnitHeight: $("#RQLVTVHeight").val(),
                LivingRoomShoeRack: $("#RQLVSRChkbx").is(':checked'), LivingRoomShoeRackWidth: $("#RQLVSRWidth").val(), LivingRoomShoeRackHeight: $("#RQLVSRHeight").val(),
                LivingRoomFalseCelling: $("#RQLVFSChkbx").is(':checked'), LivingRoomFalseCellingArea: $("#RQLVFSArea").val(), LivingRoomFalseCellingDesing: $("#RQLVFSDesign option:selected").text(),
                LivingRoomPujaUnit: $("#RQLVPUChkbx").is(':checked'), LivingRoomPujaUnitSize: $("#RQLVPUSize option:selected").text(),
                LivingRoomCrockeryUnit: $("#RQLVCUChkbx").is(':checked'), LivingRoomCrockeryUnitWidth: $("#RQLVCUWidth").val(), LivingRoomCrockeryUnitHeight: $("#RQLVCUHeight").val(), LivingRoomRequriments: $("#RQLVPUReq").val(),
                BedRoomWardrobes: $("#RQBRWRChkbx").is(':checked'), BedRoomWardrobesWidth: $("#RQBRWRWidth").val(), BedRoomWardrobesHeight: $("#RQBRWRHeight").val(), BedRoomWardrobesQuantity: $("#RQBRWRQnty option:selected").text(),
                BedRoomLoft: $("#RQBRLoftChkbx").is(':checked'), BedRoomLoftWidth: $("#RQBRLoftWidth").val(), BedRoomLoftHeight: $("#RQBRLoftHeight").val(), BedRoomLoftQuantity: $("#RQBRLoftQnty option:selected").text(),
                BedRoomFalseCelling: $("#RQBRFSChkbx").is(':checked'), BedRoomFalseCellingArea: $("#RQBRFSArea").val(), BedRoomFalseCellingDesign: $("#RQBRFSDesign option:selected").text(), BedRoomFalseCellingQuantity: $("#RQBRFSQnty option:selected").text(),
                BedRoomStudyTable: $("#RQBRSTChkbx").is(':checked'), BedRoomStudyTableQuantity: $("#RQBRSTQnty option:selected").text(),
                BedRoomDressingTable: $("#RQBRDTChkbx").is(':checked'), BedRoomDressingTableQuantity: $("#RQBRDTQnty option:selected").text(),
                BedRoomTVUnit: $("#RQBRTVChkbx").is(':checked'), BedRoomTVUnitQuantity: $("#RQBRTVQnty option:selected").text(),
                BedRoomRequriments: $("#RQBRReq").val()
            };
            var DTO = JSON.stringify(InteriorRequirment);

            $.ajax({
                url: Refurl+ "/api/Quote/RequestQuote",
                cache: false,
                type: 'POST',
                data: DTO,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    alert(data);
                    if (data == "Details are saved successfully") {
                        $("#RQerror-message").html("Your request is saved.");
                        $("#RQerror-message").css("color", "Green");
                    }
                    else if (data == "We are not able save details") {
                        $("#RQerror-message").html("Sorry, We are not able to save your request details.");
                        $("#RQerror-message").css("color", "Red");
                    }
                    //else {
                    //    GetWishList();
                    //    $("#wishlisticon").notify("Item added to wishlsit", "success", { position: "bottom center", arrowShow: true, gap: 3 });
                    //}
                },
                error: function (x, y, z) {
                    alert(x + '\n' + y + '\n' + z);
                }
            });
        }
    });

    $('#GetQuoteModal').on('hidden.bs.modal', function () {
        ResetRequestQuoteModal();
    });

    $("#rattings").jRate({
        shape: 'STAR', shapeGap: '1px', rating: 2, backgroundColor: '#ccc', onChange: function (rating) {
            $('#ratting-number').text(rating);
        }
    });

    RegisterAllCheckboxEvent();
    initilizeLoginModal();
    PopulateData();
    GetWishList();
    ResetRequestQuoteModal();
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'bottom'
    });


    $("#wishlist").on("click", function () {
        var ServiceProviderId = $("#ServiceProviderId").val();
        $.ajax({
            url: Refurl+ "/api/WishList/AddToWishList/" + $("#ServiceProviderId").val(),
            type: 'POST',
            data: JSON.stringify(ServiceProviderId),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                if (data == "No loggedin user found") {
                    $('#basicModal').modal('show');
                    initilizeLoginModal();
                }
                else if (data == "Item already added to your wish list") {
                    $("#wishlisticon").notify("Item already added to wishlsit", "info", { position: "bottom, center", arrowShow: true, gap: 3 });
                    //alert(data);
                }
                else {
                    GetWishList();
                    $("#wishlisticon").notify("Item added to wishlsit", "success", { position: "bottom center", arrowShow: true, gap: 3 });
                }
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

    $("#review").on("click", function () {
        var ServiceProviderId = $("#ServiceProviderId").val();
        $.ajax({
            url: Refurl + "/api/User/IsAuthenticated",
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data == false) {
                    $('#basicModal').modal('show');
                    initilizeLoginModal();
                }
                else if (data == true) {
                    $("#RattingModal").modal('show');

                    //alert(data);
                }
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

    $("#btnReview").on("click", function () {
        //alert($("#review-form-textarea-id").html());
        if ($("#ratting-number").html() == "") {
            $("#PublishReviewMeassage").html("Please provide your ratting.");
            $("#PublishReviewMeassage").css("color", "Red");
            $('#PublishReviewMeassage').delay(5000).fadeOut('slow');
        }
        else if ($("#review-form-textarea-id").html() == "") {
            $("#PublishReviewMeassage").html("Please provide your review.");
            $("#PublishReviewMeassage").css("color", "Red");
            $('#PublishReviewMeassage').delay(5000).fadeOut('slow');
        }
        else {
            $.ajax({
                url: Refurl + "/api/Review/WriteAReview?rating=" + $("#ratting-number").html() + "&review=" + $("#review-form-textarea-id").html() + "&ServiceProvider=" + $("#ServiceProviderId").val(),
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (data == "Successful") {
                        $('#RattingModal').modal('hide');
                        PopulateData();
                    }
                },
                error: function (x, y, z) {
                    alert(x + '\n' + y + '\n' + z);
                }
            });
        }
    });

    $("#RequestQuote").on("click", function () {
        var ServiceProviderId = $("#ServiceProviderId").val();
        $.ajax({
            url: Refurl+ "/api/User/IsAuthenticated",
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data == false) {
                    $('#basicModal').modal('show');
                    initilizeLoginModal();
                }
                else if (data == true) {
                    $("#GetQuoteModal").modal('show');

                    //alert(data);
                }
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

});

function drawMap(lat, lang) {
    var centerLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lang));
    var mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: centerLatlng
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var marker;
    var myLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lang));
    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
    });
    //google.maps.event.addListener(marker, 'click', (function (marker, i) {
    //    return function () {
    //        infowindow.setContent(coordinates[index][2]);
    //        infowindow.open(map, marker);
    //    }
    //})(marker, i));



}

function PopulateData() {
    $.ajax({
        url: Refurl + "/api/ServiceProvider/GetServiceProviderDetails?spId=" + $("#ServiceProviderId").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var coordinates = [];
            $(".header-info h1").html(data.Name);
            if (data.ContactNumber == "") {
                $(".contact").html(data.Website);
            }
            else {
                $(".contact").html(data.ContactNumber + ", " + data.Website);
            }
            $(".category").html(data.ServiceProviderTypeName);
            if (data.ServiceProviderTypeName == "Interior Designers" || data.ServiceProviderTypeName == "Carpenters") {
                $(".services-section-head h3").html("Service Offered");
                $(".project-text").html(data.Projects.length);
                
            }
            else {
                $(".services-section-head h3").html("Products Sold");
                $("#completedproject").empty();
                $("#completedprojecttext").empty();
                $("#projects").hide();

                $("#pricerangehead").empty();
                $("#pricerangetext").empty();
                $(".pricerangecontainer").hide();
            }
            if (data.Verified != "Verified") {
                $("#verified").hide();
            }
            $(".ratting-number").html(data.Ratting);
            $(".ratting-text").html(data.ReviewCount + " Reviews");

            $(".location-text").html(data.Address);
            drawMap(data.Lattitude, data.Longitude);

            // Check home page code and replace it
            var serviceItems = "";
            for (var i = 0; i < data.ServiceOffered.length; i++) {
                if (i != 0 && (i % 4) == 0) {
                    $("#servicelist-container").append('<div class="services-section-grids row">' + serviceItems + '</div>');
                    serviceItems = "";
                }
                serviceItems += '<div class="col-md-3 services-section-grid"><div class="services-section-grid-head"><div class="service-icon"><i class="event" style="background:url(' + data.ServiceOffered[i].url + ') no-repeat 0 0;"></i></div><div class="service-icon-heading"><h4>' + data.ServiceOffered[i].ServieTypeName + '</h4></div><div class="clearfix"></div></div></div>';
            }
            if ((data.ServiceOffered.length % 4) != 0 || data.ServiceOffered.length == 4) {
                $("#servicelist-container").append('<div class="services-section-grids row">' + serviceItems + '</div>');
            }

            if (data.WorkingHrs.length > 0) {
                $("#bisunesshrs").empty();
            }
            for (var key in data.WorkingHrs) {
                $("#bisunesshrs").append('<li>' + data.WorkingHrs[key] + '</li>');
            }

            if (data.PriceRange != null) {
                $(".price-text").html(data.PriceRange);
            }

            if (data.Images.length == 0) {
                $(".gallery-section-head").append('<p>Not Available</p>')
            }
            for (var key in data.Images) {
                $("#lightGallery").append('<li data-src="' + data.Images[key].ImageUrl + '"><img src="' + data.Images[key].ThumbUrl + '" /></li>');
            }

            $('#lightGallery').lightGallery({
                showThumbByDefault: true,
                addClass: 'showThumbByDefault'
            });

            if (data.Projects.length == 0) {
                $(".projects-section-head").append('<p>We donot have the details yet.</p>');
            }
            for (var i = 0; i < data.Projects.length; i++) {
                if (i === 3) { break; }
                $(".projects-section-grids").append('<div class="col-md-4"><div class="projects-section-grid-head"><div class="projects-section-grid-head-tittle"><h3>' + data.Projects[i].ProjectName + '</h3><h5>' + data.Projects[i].ProjectLocation + '</h5></div><div class="projects-section-grid-head-body">' + data.Projects[i].ProjectType + '</div></div></div>');
            }

            if (data.Reviews.length == 0) {
                $(".testimonial-section-head").append('<p>No one has reviewed this business yet.</p>');
            }
            for (var key in data.Reviews) {
                if (key === 3) { break; }
                $(".testimonial-section-grids").append('<div class="col-md-4 services-section-grid"><div class="services-section-grid-head"><div class="testimonial-icon-heading"><h4>' + data.Reviews[key].UserName + '</h4><div style="font-size:12px;">Ratted <div class="indivisual-ratting-number">' + data.Reviews[key].Ratting + '</div> | Reviewed on ' + data.Reviews[key].CreatedOn + '</div></div><div class="clearfix"></div></div><p><pre><span>"</span>' + data.Reviews[key].Reviews + '<span>"</span></pre></p></div>');
            }
            $(".Offer-text").html(data.Offers.length);
            for (var key in data.Offers) {
                var businessName = null;
                if (data.Offers[key].BusinessName.length > 20) {
                    data.Offers[key].BusinessName = data.Offers[key].BusinessName.substring(0, 20) + "...";
                }
                $("#OfferContainer").append('<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><div class="offer offer-default"><div class="offer-content"><div style="margin-top:5px;font-size:20px; font-weight:500; margin-bottom:5px;color:#1fadbb;">' + data.Offers[key].BusinessName + '</div><p id="offerTittle"style="color:#f8b019;" ><span class="glyphicon glyphicon-gift"></span> ' + data.Offers[key].OfferTittle + '</p><p style="min-height:30px;"> ' + data.Offers[key].OfferDescription + '</p><div class="row" ><div class="col-md-12 post-header-line" style="font-size:12px; margin-top:10px; color:#808080;"><div class="text-left" style="display:inline-block;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Valid till <br /><strong>' + $.datepicker.formatDate('dd M, yy', new Date(data.Offers[key].ValidTill)) + '</strong></div><div class="text-left" style="display:inline-block; margin-left:15px;"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Views<br /><strong>' + data.Offers[key].Views + '</strong></div></div></div></div></div></div>');
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function initilizeLoginModal() {
    $("#loginformContainer").show();
    $("#loginright").show();
    $("#SignupformContainer").hide();
    $("#signupright").hide();
    $("#ForgotformContainer").hide();
}



function ResetRequestQuoteModal() {
    $('#rootwizard').find("a[href*='RoomDetails']").trigger('click');
    $("#RQHouseType").val(1);
    $("#RQHouseCarpetArea").val("");
    $("#RQKitchenType").val(1);
    $("#RQKitchenWidth").val("");
    $("#RQKitchenHeight").val("");
    $("#RQKitchenReq").val("");
    $("#RQLVTVUnitChkbx").prop('checked', false);
    $("#RQLVTVWidth").val("");
    $("#RQLVTVWidth").attr('readonly', true);
    $("#RQLVTVHeight").val("");
    $("#RQLVTVHeight").attr('readonly', true);

    $("#RQLVCUChkbx").prop('checked', false);
    $("#RQLVCUWidth").val("");
    $("#RQLVCUWidth").attr('readonly', true);
    $("#RQLVCUHeight").val("");
    $("#RQLVCUHeight").attr('readonly', true);

    $("#RQLVSRChkbx").prop('checked', false);
    $("#RQLVSRWidth").val("");
    $("#RQLVSRWidth").attr('readonly', true);
    $("#RQLVSRHeight").val("");
    $("#RQLVSRHeight").attr('readonly', true);

    $("#RQLVFSChkbx").prop('checked', false);
    $("#RQLVFSArea").val("");
    $("#RQLVFSArea").attr('readonly', true);
    $("#RQLVFSDesign").val(-1);
    $("#RQLVFSDesign").attr('readonly', true);

    $("#RQLVPUChkbx").prop('checked', false);
    $("#RQLVPUSize").val(-1);
    $("#RQLVPUSize").attr('readonly', true);
    $("#RQLVPUReq").val("");

    $("#RQBRWRChkbx").prop('checked', false);
    $("#RQBRWRWidth").val("");
    $("#RQBRWRWidth").attr('readonly', true);
    $("#RQBRWRHeight").val("");
    $("#RQBRWRHeight").attr('readonly', true);
    $("#RQBRWRQnty").val(-1);
    $("#RQBRWRQnty").attr('readonly', true);

    $("#RQBRLoftChkbx").prop('checked', false);
    $("#RQBRLoftWidth").val("");
    $("#RQBRLoftWidth").attr('readonly', true);
    $("#RQBRLoftHeight").val("");
    $("#RQBRLoftHeight").attr('readonly', true);
    $("#RQBRLoftQnty").val(-1);
    $("#RQBRLoftQnty").attr('readonly', true);

    $("#RQBRFSChkbx").prop('checked', false);
    $("#RQBRFSArea").val("");
    $("#RQBRFSArea").attr('readonly', true);
    $("#RQBRFSDesign").val(-1);
    $("#RQBRFSDesign").attr('readonly', true);
    $("#RQBRFSQnty").val(-1);
    $("#RQBRFSQnty").attr('readonly', true);

    $("#RQBRSTChkbx").prop('checked', false);
    $("#RQBRSTQnty").val(-1);
    $("#RQBRSTQnty").attr('readonly', true);

    $("#RQBRDTChkbx").prop('checked', false);
    $("#RQBRDTQnty").val(-1);
    $("#RQBRDTQnty").attr('readonly', true);

    $("#RQBRTVChkbx").prop('checked', false);
    $("#RQBRTVQnty").val(-1);
    $("#RQBRTVQnty").attr('readonly', true);

    $("#RQBRReq").val("");
}

function ValidateQuoteRequest() {
    var decimalRegEx = /^((\d+)((\.\d{1,2})?))$/;
    var RQBRWRWidth = $("#RQBRWRWidth").val();
    var RQBRWRHeight = $("#RQBRWRHeight").val();
    var RQBRLoftWidth = $("#RQBRLoftWidth").val();
    var RQBRLoftHeight = $("#RQBRLoftHeight").val();
    var RQBRFSArea = $("#RQBRFSArea").val();

    if ($("#RQBRWRChkbx").is(':checked') == true) {
        if ($("#RQBRWRWidth").val() == "") {
            $("#RQerror-message").html("Please provide width of bed room wardrobe");
            $("#RQBRWRWidth").focus();
            return false;
        }
        else if (!decimalRegEx.test(RQBRWRWidth)) {
            $("#RQerror-message").html("Please provide decimal value for width of bed room wardrobe");
            $("#RQBRWRWidth").focus();
            return false;
        }
        else if ($("#RQBRWRHeight").val() == "") {
            $("#RQerror-message").html("Please provide height of bed room wardrobe");
            $("#RQBRWRHeight").focus();
            return false;
        }
        else if (!decimalRegEx.test(RQBRWRHeight)) {
            $("#RQerror-message").html("Please provide decimal value for height of bed room wardrobe");
            $("#RQBRWRHeight").focus();
            return false;
        }
        else if ($("#RQBRWRQnty").val() == "-1") {
            $("#RQerror-message").html("Please select a quantity of bed room wardrobe");
            $("#RQBRWRQnty").focus();
            return false;
        }
    }

    if ($("#RQBRLoftChkbx").is(':checked') == true) {
        if ($("#RQBRLoftWidth").val() == "") {
            $("#RQerror-message").html("Please provide width of bed room loft");
            $("#RQBRLoftWidth").focus();
            return false;
        }
        else if (!decimalRegEx.test(RQBRLoftWidth)) {
            $("#RQerror-message").html("Please provide decimal value for width of bed room loft");
            $("#RQBRLoftWidth").focus();
            return false;
        }
        else if ($("#RQBRLoftHeight").val() == "") {
            $("#RQerror-message").html("Please provide height of bed room Loft");
            $("#RQBRLoftHeight").focus();
            return false;
        }
        else if (!decimalRegEx.test(RQBRLoftHeight)) {
            $("#RQerror-message").html("Please provide decimal value for height of bed room loft");
            $("#RQBRLoftHeight").focus();
            return false;
        }
        else if ($("#RQBRLoftQnty").val() == "-1") {
            $("#RQerror-message").html("Please select a quantity of bed room Loft");
            $("#RQBRLoftQnty").focus();
            return false;
        }
    }

    if ($("#RQBRFSChkbx").is(':checked') == true) {
        if ($("#RQBRFSArea").val() == "") {
            $("#RQerror-message").html("Please provide area of bed room flase celling");
            $("#RQBRFSArea").focus();
            return false;
        }
        else if (!decimalRegEx.test(RQBRFSArea)) {
            $("#RQerror-message").html("Please provide decimal value for area of bed room false celling");
            $("#RQBRFSArea").focus();
            return false;
        }
        else if ($("#RQBRFSDesign").val() == "-1") {
            $("#RQerror-message").html("Please select a design of bed room flase celling");
            $("#RQBRFSDesign").focus();
            return false;
        }
        else if ($("#RQBRFSQnty").val() == "-1") {
            $("#RQerror-message").html("Please select a quantity of bed room flase celling");
            $("#RQBRFSQnty").focus();
            return false;
        }
    }

    if ($("#RQBRSTChkbx").is(':checked') == true) {
        if ($("#RQBRSTQnty").val() == "-1") {
            $("#RQerror-message").html("Please select quantity of bed room study table");
            $("#RQBRSTQnty").focus();
            return false;
        }
    }

    if ($("#RQBRDTChkbx").is(':checked') == true) {
        if ($("#RQBRDTQnty").val() == "-1") {
            $("#RQerror-message").html("Please select quantity of bed room Dressing table");
            $("#RQBRDTQnty").focus();
            return false;
        }
    }

    if ($("#RQBRTVChkbx").is(':checked') == true) {
        if ($("#RQBRTVQnty").val() == "-1") {
            $("#RQerror-message").html("Please select quantity of bed room TV Unit");
            $("#RQBRTVQnty").focus();
            return false;
        }
    }

    $("#RQerror-message").html("");
    return true;

}

function RegisterAllCheckboxEvent() {

    $('#RQLVTVUnitChkbx').change(function () {
        if ($('#RQLVTVUnitChkbx').is(':checked')) {
            $('#RQLVTVWidth').attr('readonly', false);
            $('#RQLVTVHeight').attr('readonly', false);
        }
        else {
            $('#RQLVTVWidth').attr('readonly', true);
            $('#RQLVTVHeight').attr('readonly', true);
            $('#RQLVTVHeight').val("");
            $('#RQLVTVWidth').val("");
        }
    });

    $('#RQLVCUChkbx').change(function () {
        if ($('#RQLVCUChkbx').is(':checked')) {
            $('#RQLVCUWidth').attr('readonly', false);
            $('#RQLVCUHeight').attr('readonly', false);
        }
        else {
            $('#RQLVCUWidth').attr('readonly', true);
            $('#RQLVCUHeight').attr('readonly', true);
            $('#RQLVCUHeight').val("");
            $('#RQLVCUWidth').val("");
        }
    });

    $('#RQLVSRChkbx').change(function () {
        if ($('#RQLVSRChkbx').is(':checked')) {
            $('#RQLVSRWidth').attr('readonly', false);
            $('#RQLVSRHeight').attr('readonly', false);
        }
        else {
            $('#RQLVSRWidth').attr('readonly', true);
            $('#RQLVSRHeight').attr('readonly', true);
            $('#RQLVSRHeight').val("");
            $('#RQLVSRWidth').val("");
        }
    });

    $('#RQLVFSChkbx').change(function () {
        if ($('#RQLVFSChkbx').is(':checked')) {
            $('#RQLVFSArea').attr('readonly', false);
            $('#RQLVFSDesign').attr('readonly', false);
        }
        else {
            $('#RQLVFSArea').attr('readonly', true);
            $('#RQLVFSDesign').attr('readonly', true);
            $('#RQLVFSDesign').val(-1);
            $('#RQLVFSArea').val("");
        }
    });

    $('#RQLVPUChkbx').change(function () {
        if ($('#RQLVPUChkbx').is(':checked')) {
            $('#RQLVPUSize').attr('readonly', false);
        }
        else {
            $('#RQLVPUSize').attr('readonly', true);
            $('#RQLVPUSize').val(-1);
        }
    });

    $('#RQBRWRChkbx').change(function () {
        if ($('#RQBRWRChkbx').is(':checked')) {
            $('#RQBRWRWidth').attr('readonly', false);
            $('#RQBRWRHeight').attr('readonly', false);
            $('#RQBRWRQnty').attr('readonly', false);
        }
        else {
            $('#RQBRWRWidth').attr('readonly', true);
            $('#RQBRWRQnty').attr('readonly', true);
            $('#RQBRWRHeight').attr('readonly', true);
            $('#RQBRWRHeight').val("");
            $('#RQBRWRWidth').val("");
            $('#RQBRWRQnty').val(-1);
        }
    });

    $('#RQBRLoftChkbx').change(function () {
        if ($('#RQBRLoftChkbx').is(':checked')) {
            $('#RQBRLoftWidth').attr('readonly', false);
            $('#RQBRLoftHeight').attr('readonly', false);
            $('#RQBRLoftQnty').attr('readonly', false);
        }
        else {
            $('#RQBRLoftWidth').attr('readonly', true);
            $('#RQBRLoftQnty').attr('readonly', true);
            $('#RQBRLoftHeight').attr('readonly', true);
            $('#RQBRLoftHeight').val("");
            $('#RQBRLoftWidth').val("");
            $('#RQBRLoftQnty').val(-1);
        }
    });

    $('#RQBRFSChkbx').change(function () {
        if ($('#RQBRFSChkbx').is(':checked')) {
            $('#RQBRFSArea').attr('readonly', false);
            $('#RQBRFSDesign').attr('readonly', false);
            $('#RQBRFSQnty').attr('readonly', false);
        }
        else {
            $('#RQBRFSArea').attr('readonly', true);
            $('#RQBRFSDesign').attr('readonly', true);
            $('#RQBRFSQnty').attr('readonly', true);
            $('#RQBRFSArea').val("");
            $('#RQBRFSDesign').val(-1);
            $('#RQBRFSQnty').val(-1);
        }
    });

    $('#RQBRSTChkbx').change(function () {
        if ($('#RQBRSTChkbx').is(':checked')) {
            $('#RQBRSTQnty').attr('readonly', false);
        }
        else {
            $('#RQBRSTQnty').attr('readonly', true);
            $('#RQBRSTQnty').val(-1);
        }
    });

    $('#RQBRDTChkbx').change(function () {
        if ($('#RQBRDTChkbx').is(':checked')) {
            $('#RQBRDTQnty').attr('readonly', false);
        }
        else {
            $('#RQBRDTQnty').attr('readonly', true);
            $('#RQBRDTQnty').val(-1);
        }
    });

    $('#RQBRTVChkbx').change(function () {
        if ($('#RQBRTVChkbx').is(':checked')) {
            $('#RQBRTVQnty').attr('readonly', false);
        }
        else {
            $('#RQBRTVQnty').attr('readonly', true);
            $('#RQBRTVQnty').val(-1);
        }
    });

}
