﻿var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net/";
$(document).ready(function () {
    $("#searchbtn").click(function (e) {
        //validation
        var url = "";
        if ($("#seach-txtbox").val() == "") {
            alert("Please let us know what do you want to find.");
        }
        else {
            var category = null;
            if ($("#searchCategory").val() == "Service Provider") {
                GetServiceProviderDetails();
                //window.location.href = "home/BusinessDetails?spid=" + spid;
            }
            else {
                if ($("#locationCategory").val() == "City" && $("#searchCategory").val() == "Service Provider Type") {
                    category = 1;
                }
                else if ($("#locationCategory").val() == "City" && $("#searchCategory").val() == "Service") {
                    category = 2;
                }
                else if ($("#locationCategory").val() == "City" && $("#searchCategory").val() == "Product") {
                    category = 3;
                }
                else if ($("#locationCategory").val() == "Area" && $("#searchCategory").val() == "Service Provider Type") {
                    category = 4;
                }
                else if ($("#locationCategory").val() == "Area" && $("#searchCategory").val() == "Service") {
                    category = 5;
                }
                else if ($("#locationCategory").val() == "Area" && $("#searchCategory").val() == "Product") {
                    category = 6;
                }
                window.location.href = "Search/" + $("#location-select-txtbox").val() + "/" + $("#seach-txtbox").val() + "?Category=" + category;
            }


        }
    });
    GetOffers();
});

function autocomplete(city) {
    $.ajax({
        url: Refurl + '/api/ServiceProvider/GetAllAreasByCityTypehead/' + city,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#location-select-txtbox').typeahead({
                order: "asc",
                minLength: 0,
                offset: true,
                hint: true,
                display: ["value", "data"],
                template: '<div style="height:25px;"><div style="display:inline; float:left;">{{value}}</div> <div class="text-right" style="float:right; display:inline;"><span class="badge">{{data}}</span></div></div>',
                source: {
                    data: data
                },
                backdrop: {
                    "background-color": "#000",
                    "opacity": "0.8",
                    "filter": "alpha(opacity=5)"
                },
                callback: {
                    onInit: function (node) {
                        console.log('Typeahead Initiated on ' + node.selector);
                    },
                    onClickAfter: function (node, a, item, event) {
                        $("#locationCategory").val(item.data);
                    }
                }
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });

    $.ajax({
        url: Refurl + '/api/ServiceProvider/GetSearchString?area=Bangalore',
        type: 'GET',
        dataType: 'json',
        success: function (searchstring) {
            $('#seach-txtbox').typeahead({
                order: "asc",
                minLength: 0,
                hint: true,
                offset: true,
                searchOnFocus: false,
                offset: true,
                display: ["value", "data"],
                template: '<div style="height:25px;"><div style="display:inline; float:left;">{{value}}</div> <div class="text-right" style="float:right; display:inline;"><span class="badge">{{data}}</span></div></div>',
                backdrop: {
                    "background-color": "#000",
                    "opacity": "0.8",
                    "filter": "alpha(opacity=5)"
                },
                source: {
                    data: searchstring
                },
                callback: {
                    onInit: function (node) {
                        console.log('Typeahead Initiated on ' + node.selector);
                    },
                    onClickAfter: function (node, a, item, event) {
                        $("#searchCategory").val(item.data);
                    }
                }
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });


}

function GetOffers() {
    $.ajax({
        url: Refurl + '/api/Offer/GetHotOffers',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            for (var key in data) {
                var businessName = null;
                if (data[key].BusinessName.length > 20) {
                    data[key].BusinessName = data[key].BusinessName.substring(0, 20) + "...";
                }
                $("#OfferContainer").append('<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"><div class="offer offer-default"><div class="offer-content"><div style="margin-top:5px;font-size:20px; font-weight:500; margin-bottom:5px;color:#1fadbb;">' + data[key].BusinessName + '</div><p id="offerTittle"style="color:#f8b019;" ><span class="glyphicon glyphicon-gift"></span> ' + data[key].OfferTittle + '</p><p style="min-height:30px;"> ' + data[key].OfferDescription + '</p><div class="row" ><div class="col-md-12 post-header-line" style="font-size:12px; margin-top:10px; color:#808080;"><div class="text-left" style="display:inline-block;"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Valid till <br /><strong>' + $.datepicker.formatDate('dd M, yy', new Date(data[key].ValidTill)) + '</strong></div><div class="text-left" style="display:inline-block; margin-left:15px;"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;Views<br /><strong>' + data[key].Views + '</strong></div></div></div></div></div></div>');
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function GetServiceProviderDetails() {
    var location;
    var locationCategory;
    if ($("#location-select-txtbox").val() != "") {
        location = $("#location-select-txtbox").val();
        locationCategory = $("#locationCategory").val();
    }
    else
    {
        location = $("div .usercity").text();
        locationCategory = "City";
    }
    $.ajax({
        url: Refurl + '/api/ServiceProvider/GetServiceProviderDetailsByName/' + $("#seach-txtbox").val() + '/' + location + '/' + locationCategory,
        type: 'GET',
        dataType: 'json',
        asyn:false,
        success: function (data) {
            window.location.href = "home/BusinessDetails?spid=" + data.BusinessId
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}


