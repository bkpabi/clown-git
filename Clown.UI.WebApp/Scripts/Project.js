﻿var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    GetServiceProviderDetails();
    GetAllProjects();
    $("#data-list").on("click","img",function (e) {
        var imageGallary = [];
        $.ajax({
            url: Refurl + '/api/Project/GetProjectImagesByProjectId/' + $(this).prev("input[type='hidden']").val(),
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                var item = {};
                for (var key in data) {
                    item["src"] = data[key].Link;
                    item["thumb"] = data[key].Link;
                    item["sub-html"] = "<div class='custom-html'><h4>Custom HTML</h4></div>";
                    imageGallary.push(item);
                }

                $(this).lightGallery({
                    dynamic: true,
                    html: true,
                    exThumbImage: 100,
                    showThumbByDefault: false,
                    dynamicEl: imageGallary
                    //dynamicEl: [
                    //    { "src": "../Content/Images/img1.jpg", "thumb": "../Content/Images/img1.jpg", "sub-html": "<div class='custom-html'><h4>Custom HTML</h4></div>" },
                    //    { "src": "../Content/Images/img2.jpg", "thumb": "../Content/Images/img1.jpg", "sub-html": "#dynamicHtml" },
                    //    { "src": "../Content/Images/img3.jpg", "thumb": "../Content/Images/img1.jpg", "sub-html": "#dynamicHtml" }
                    //]
                });
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

    $(".fliter-item-heading").click(function () {
        $(this).next("div").hide();
        $(this).next("div").next("div").toggle();
        $(this).children().toggleClass("glyphicon-menu-up");
    });

    $(".filter-items ul").on("click", "li", function () {
        //alert($(this).parent("ul").parent("div").html());
        $(this).parent("ul").parent("div").prev("div").html($(this).text() + '<span class="glyphicon glyphicon-remove pull-right filter-remove"></span>');
        $(this).parent("ul").parent("div").prev("div").toggle();
        $(this).parent("ul").parent("div").toggle();
        $(this).parent("ul").parent("div").prev("div").prev("div").children().toggleClass("glyphicon-menu-up");

    });

    $(".filter-string").on("click", "span", function () {
        //alert($(this).html());
        $(this).parent().html("");
        $(this).parent().hide();
        PopulateFilteredData();
    });

    $(".filter-eachItems").on("click", "li", function () {
        PopulateFilteredData();
    });
});

function GetAllProjects() {
    $.ajax({
        url: Refurl + '/api/Project/GetProjectsByServiceProvider/' + $("#spid").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $("span.search-resultCount").html(data.length + " Projects");
            var area = [];

            for (var key in data) {
                var index = $.inArray(data[key].Area, area);
                if (index == -1) {
                    area.push(data[key].Area);
                    $("#filter-area").append('<li>' + data[key].Area + '</li>');
                }
                var imagelink = "../Content/Images/img1.jpg";
                if (data[key].Gallery.length != 0) {
                    imagelink = data[key].Gallery[0].Link;
                }
                $("#data-list").append('<li class="listingcomponent"><div id="projectSnippetWidget" class="project-snpt-wrp"><div class="project-snpt-img"><input type="hidden" value="' + data[key].ProjectId + '"/><img src="' + imagelink + '" data-lightbox="roadtrip" class="project-thumb" /></div><div id="" class="project-snpt-right-cont"><div class="pull-left"><span class="project-snpt-tittle">' + data[key].ProjectName + '</span><span class="project-snpt-status"> ' + data[key].Status + ' </span><div class="clearfix"></div><p class="project-snpt-loc">' + data[key].ProjectLocation + '</p><p class="project-snpt-cost">Project cost - <span class="emphasis"> ' + data[key].ProjectCost + '</span></p><p class="project-snpt-cost">House Type - <span class="emphasis"> ' + data[key].ProjectType + '</span></p><p class="project-snpt-cost">Time taken  - <span class="emphasis"> ' + data[key].NoOfDaysTaken + ' Months</span></p></div></div></div></li>')
            }

            $(function () {
                $("div.holder").jPages({
                    containerID: "data-list",
                    perPage: 2,
                    fallback: 500,
                    scrollBrowse: true,
                    keyBrowse: true
                });
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function PopulateFilteredData() {
    var location = $("#location").val();
    var searchBy = $("#searchBy").val();
    var category = $("#category").val();
    var filterQuery = "?$filter=";
    if ($("#filter-cost-set").text() != "") {
        switch ($("#filter-cost-set").text()) {
            case "Less than 3Lakhs":
                filterQuery += "ProjectCost le " + 300000;
                break;
            case "3Lakhs to 5lakhs":
                filterQuery += "ProjectCost ge " + 300000 + "and ProjectCost le " + 500000;
                break;
            case "5Lakhs to 10Lakhs":
                filterQuery += "ProjectCost ge " + 500000 + "and ProjectCost le " + 1000000;
                break;
            case "More than 10Lakhs":
                filterQuery += "ProjectCost ge " + 1000000;
                break;
            default:

        }
    }

    if ($("#filter-area-set").text() != "") {
        if ($("#filter-cost-set").text() != "") {
            filterQuery += " and Area eq '" + $("#filter-area-set").text() + "'";
        }
        else {
            filterQuery += "Area eq '" + $("#filter-area-set").text() + "'";
        }

    }

    if ($("#filter-housetype-set").text() != "") {
        if ($("#filter-cost-set").text() != "" || $("#filter-cost-set").text() != "") {
            filterQuery += "and ProjectType eq '" + $("#filter-housetype-set").text() + "'";
        }
        else
        {
            filterQuery += "ProjectType eq '" + $("#filter-housetype-set").text() + "'";
        }
        
    }

    if ($("#filter-projectstatus-set").text() != "") {
        if ($("#filter-cost-set").text() != "" || $("#filter-cost-set").text() != "" || $("#filter-housetype-set").text() != "") {
            filterQuery += "and Status eq '" + $("#filter-projectstatus-set").text() + "'";
        }
        else {
            filterQuery += "Status eq '" + $("#filter-projectstatus-set").text() + "'";
        }

    }


    if (filterQuery == "?$filter=") {
        filterQuery = "";
    }

    $.ajax({
        url: Refurl + '/api/Project/GetProjectsByServiceProvider/' + $("#spid").val() + filterQuery,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#data-list').empty();
            $(".search-resultCount").html(data.length + " results found");
            $("#ajax").hide();
            for (var key in data) {
                var imagelink = "../Content/Images/img1.jpg";
                if (data[key].Gallery.length != 0) {
                    imagelink = data[key].Gallery[0].Link;
                }
                $("#data-list").append('<li class="listingcomponent"><div id="projectSnippetWidget" class="project-snpt-wrp"><div class="project-snpt-img"><input type="hidden" value="' + data[key].ProjectId + '"/><img src="' + imagelink + '" data-lightbox="roadtrip" class="project-thumb" /></div><div id="" class="project-snpt-right-cont"><div class="pull-left"><span class="project-snpt-tittle">' + data[key].ProjectName + '</span><span class="project-snpt-status"> ' + data[key].Status + ' </span><div class="clearfix"></div><p class="project-snpt-loc">' + data[key].ProjectLocation + '</p><p class="project-snpt-cost">Project cost - <span class="emphasis"> ' + data[key].ProjectCost + '</span></p><p class="project-snpt-cost">House Type - <span class="emphasis"> ' + data[key].ProjectType + '</span></p><p class="project-snpt-cost">Time taken  - <span class="emphasis"> ' + data[key].NoOfDaysTaken + ' Months</span></p></div></div></div></li>')
            }
            $("span.search-resultCount").html(data.length + " Projects");
            $(function () {
                $("div.holder").jPages({
                    containerID: "data-list",
                    perPage: 2,
                    fallback: 500,
                    scrollBrowse: true,
                    keyBrowse: true
                });
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}


function GetServiceProviderDetails() {
    $.ajax({
        url: Refurl + "/api/ServiceProvider/GetServiceProviderDetails?spId=" + $("#spid").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $(".search-nav").html('<a href="' + Refurl + '">Home</a> / <a href="' + Refurl + '/Home/BusinessDetails?spid=' + $("#spid").val() + '">' + data.Name + '</a>');
            $(".search-header").text(data.Name + " - Projects");
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}