﻿$(document).ready(function () {
    $("#rattings").jRate({
        shape: 'STAR', shapeGap: '1px', rating: 2, backgroundColor: '#ccc', onChange: function (rating) {
            $('#ratting-number').text(rating);
        }
    });

    $("#YourReviews").click(function() {
        alert($("#urReview").val());
    });
    
    PopulateData();
});

function drawMap(lat,lang) {
    var centerLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lang));
    var mapOptions = {
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: centerLatlng
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var marker;
    var myLatlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lang));
    marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
            //google.maps.event.addListener(marker, 'click', (function (marker, i) {
            //    return function () {
            //        infowindow.setContent(coordinates[index][2]);
            //        infowindow.open(map, marker);
            //    }
            //})(marker, i));
        


}

function PopulateData() {
    $.ajax({
        url: "http://localhost:1075/api/ServiceProvider/GetServiceProviderDetails?spId="+ $("#ServiceProviderId").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            var coordinates = [];
            //for (var key in data) {
            $(".spName").html(data.Name);
            $(".search-resultCount").html(data.ContactNumber + ", " + data.Website);
            $(".ratting-number").html(data.Ratting);
            $(".ratting-text").html(data.ReviewCount + " Reviews");
            $("#verified-text").html(data.Verified);
            $("#location-text").html(data.Address);
            drawMap(data.Lattitude, data.Longitude);
            $("#category-text").html(data.ServiceProviderTypeName);
            $("#deal-text").html(data.ServiceProvided);
            
            for (var key in data.WorkingHrs) {
                $("#businesshrs-text").append('<li>' + data.WorkingHrs[key] + '</li>');
            }
            $("#pricerange-text").html(data.PriceRange);

            for (var key in data.Images) {
                $("#lightGallery").append('<li data-src="'+data.Images[key].ImageUrl+'"><img src="'+data.Images[key].ThumbUrl+'" /></li>');
            }
            $('#lightGallery').lightGallery({
                showThumbByDefault: true,
                addClass: 'showThumbByDefault'
            });

            for (var i = 0; i < data.Projects.length; i++) {
                $(".projectsLists").append('<li><img src="' + data.Projects[i].ThumbUrl + '" /><div>' + data.Projects[i].Tittle + '</div></li>');
            }
            
            for (var key in data.Reviews) {
                $("#review-text").append('<li><div><div class="review-header"><div class="userName">' + data.Reviews[key].UserName + '</div><div id="rattingdetails">Ratting <div class="indivisual-ratting-number">' + data.Reviews[key].Ratting + '</div> <div class="reviewedOn">Reviwed on ' + data.Reviews[key].CreatedOn + '</div></div></div><div class="review-body"><pre>' + data.Reviews[key].Reviews + '</pre></div></div></li>');
            }
            //$(".review-body").append('<pre>' + $("#urReview").val()+'</pre>')
                //$('#data-list').append('<li><p><a href="' + url + '">' + data[key].Name + '&nbsp;</a></p><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div><div class="verification"><img src="/Content/Images/verified.png" height="20" width="20" /><span>' + data[key].Verified + '</span></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceProvided + '</div><div class="address"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left;"><div style="color:#808080; font-size:12px; min-width:100px; display:inline-block;">Completed Projects</div><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">15</span></div><div style="float:left; margin-left:15px;"><span style="color:#808080; font-size:12px; width:100px; display:inline-block;">Total experience</span><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">' + data[key].TotalExperience + '</span></div><div style="float:left; margin-left:15px;"><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;">Running Offers</span><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;">2</span></div></div></li>');
                //coordinates.push(new Array(data[key].Longitude, data[key].Lattitude, data[key].Address));
            //}
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}