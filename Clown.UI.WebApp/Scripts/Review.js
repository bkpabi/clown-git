﻿var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    GetServiceProviderDetails();
    GetReviews();
});

function GetReviews() {
    $.ajax({
        url: Refurl + '/api/Review/GetAllReviewsByServiceProviderId/' + $("#spid").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $(".search-resultCount").text(data.length + " Reviews");
            for (var key in data) {
                $("#data-list").append('<li class="listingcomponent"><div id="reviewSnippetWidget" class="review-snpt-wrp"><div class="review-snpt-header"><div class="carousel-info"><img alt="" src="http://keenthemes.com/assets/bootsnipp/img1-small.jpg" class="pull-left"><div class="pull-left"><h4 style="margin-bottom:5px;margin-top:5px;">' + data[key].UserName + '</h4><div style="font-size:12px;">Ratted <div class="indivisual-ratting-number">' + data[key].Ratting + '</div> | Reviewed on ' + data[key].CreatedOn + '</div></div></div></div><div id="" class="review-snpt-body"><div><pre><span><span class="emphasis" style="font-size:22px;">"</span> ' + data[key].Reviews + '<span class="emphasis" style="font-size:22px;">"</span></span></pre></div></div><div class="review-snpt-footer"></div></div></li>');
            }
            $(function () {
                $("div.holder").jPages({
                    containerID: "data-list",
                    perPage: 2,
                    fallback: 500,
                    scrollBrowse: true,
                    keyBrowse: true
                });
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function GetServiceProviderDetails() {
    $.ajax({
        url: Refurl + "/api/ServiceProvider/GetServiceProviderDetails?spId=" + $("#spid").val(),
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $(".search-nav").html('<a href="' + Refurl + '">Home</a> / <a href="' + Refurl + '/Home/BusinessDetails?spid=' + $("#spid").val() + '">' + data.Name + '</a>');
            $(".search-header").text(data.Name + " - Reviews");
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}