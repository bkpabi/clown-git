﻿var map;
var Refurl = "http://localhost:1406";
//var Refurl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    PopulateData();
    ShowFilterOptions();
    $(".search_btn").click(function (e) {
        //TODO : Validate if user has entered correct details
        
        PopulateData();
        //window.location.href = "Home/Search?location=" + $("#location-select-txtbox").val() + "&locationCategory=" + $("#locationCategory").val() + "&serach=" + $("#seach-txtbox").val() + "&serachCategory=" + $("#searchCategory").val();
    });
    $(".search_btn").ajaxStart(function () {
        $("#ajax").show();
    });
    //$(".search_btn").ajaxEnd(function () {
    //    $("#ajax").Hide();
    //});

    $(".fliter-item-heading").click(function () {
        $(this).next("div").hide();
        $(this).next("div").next("div").toggle();
        $(this).children().toggleClass("glyphicon-menu-up");
    });

    $(".filter-items ul").on("click", "li", function () {
        //alert($(this).parent("ul").parent("div").html());
        $(this).parent("ul").parent("div").prev("div").html($(this).text() + '<span class="glyphicon glyphicon-remove pull-right filter-remove"></span>');
        $(this).parent("ul").parent("div").prev("div").toggle();
        $(this).parent("ul").parent("div").toggle();
        $(this).parent("ul").parent("div").prev("div").prev("div").children().toggleClass("glyphicon-menu-up");

    });

    $(".filter-string").on("click","span", function () {
        //alert($(this).html());
        $(this).parent().html("");
        $(this).parent().hide();
        PopulateFilteredData();
    });
    
    $(".filter-eachItems").on("click", "li", function () {
        PopulateFilteredData();
    });
});



function PopulateData() {
    var location = $("#location").val();
    var searchBy = $("#searchBy").val();
    var category = $("#category").val();
    var prefilter = $("#prefilter").val();
    var apiURL = Refurl + "/api/ServiceProvider/GetServiceProviders/" + location + "/" + searchBy + "/" + category;
    if (prefilter != "") {
        // We need to load pre filterd view and hide the filter option and increase the width of data container
        $("div .filter-vertical").hide();
        $("div .data").css("width", "62%");
        switch (prefilter) {
            case "top-rated":
                apiURL += "?$top=10&$orderby=Ratting desc";
                break;
            case "most-exp":
                apiURL += "?$top=10&$orderby=Experience desc";
                break;
            case "afforable":
                apiURL += "?$top=10&$orderby=PriceRange asc";
                break;
            case "with-offer":
                apiURL += "?$filter=OfferCount gt 0";
                break;
            default:

        }
    }

    $(".search-nav").html("Home/" + location + "/" + searchBy);
    $(".search-header").html(searchBy + " in " + location);

    $.ajax({
        url: apiURL,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#data-list').empty();
            $(".search-resultCount").html(data.length + " results found");
            $("#ajax").hide();
            var coordinates = [];

            for (var key in data) {
                var url = Refurl + "/home/BusinessDetails?spid=" + data[key].BusinessId;
                var verified;
                if (data[key].IsVerified == true) {
                    verified = '<div class="verification"><span class="glyphicon glyphicon-heart" style="color:#2c6c2c; font-size:18px;"></span><span class="ratting-text">Verified</span></div>';
                }
                else {
                    verified = '';
                }
                if (data[key].BusinessType == "Interior Designers" || data[key].BusinessType == "Carpenters") {
                    $("#data-list").append('<li><div style="height:50px;"><div class="pull-left" style="width:100%;"><span class="businessName"><a href="'+url+'">' + data[key].BusinessName + '&nbsp;</a></span><span class="tags">' + data[key].BusinessType + '</span></div><div class="pull-left" style="width:100%;"><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div>' + verified + '</div></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceOffered + '</div><div class="deals"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].ProjectCount + '</span><div style="color:#808080; font-size:12px; display:inline-block;" class="text-center">Projects</div></div><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].InBusinessSince + '</span><span style="color:#808080; font-size:12px; width:100px; display:inline-block;" class="text-center">In Business Since</span></div><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].OfferCount + '</span><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;" class="text-center">Offers</span></div></div></li>');
                }
                else {
                    $("#data-list").append('<li><div style="height:50px;"><div class="pull-left" style="width:100%;"><span class="businessName"><a href="' + url + '">' + data[key].BusinessName + '&nbsp;</a></span><span class="tags">' + data[key].BusinessType + '</span></div><div class="pull-left" style="width:100%;"><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div>' + verified + '</div></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceOffered + '</div><div class="deals"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].InBusinessSince + '</span><span style="color:#808080; font-size:12px; width:100px; display:inline-block;" class="text-center">In Business Since</span></div><div style="float:left;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].OfferCount + '</span><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;" class="text-center">Offers</span></div></div></li>');
                }
                coordinates.push(new Array(data[key].Longitude, data[key].Lattitude, data[key].Address));
            }

            
            //}
           
            
            initialize(coordinates);
            $(function () {
                $("div.holder").jPages({
                    containerID: "data-list",
                    perPage: 2,
                    fallback: 500,
                    scrollBrowse: true,
                    keyBrowse: true
                });
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function initialize(coordinates) {
    var centerLatlng = new google.maps.LatLng(12.971599, 77.594563);
    var mapOptions = {
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: centerLatlng
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    var marker;
    for (index = 0; index < coordinates.length; ++index) {
        if (coordinates[index][0] != null) {
            var myLatlng = new google.maps.LatLng(parseFloat(coordinates[index][1]), parseFloat(coordinates[index][0]));
            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
            //google.maps.event.addListener(marker, 'click', (function (marker, i) {
            //    return function () {
            //        infowindow.setContent(coordinates[index][2]);
            //        infowindow.open(map, marker);
            //    }
            //})(marker, i));
        }
    }

    
}

function ShowFilterOptions() {
    if ($("#searchBy").val() == "Interior Designers" || $("#searchBy").val() == "Carpenters") {
        $("#filter-tab-cost").show();
        $("#filter-tab-service").show();
        GetAllServiceTypes();
    }
    else {
        $("#filter-tab-product").show();
        GetAllProducts();
    }
    
}

function PopulateFilteredData()
{
    var location = $("#location").val();
    var searchBy = $("#searchBy").val();
    var category = $("#category").val();
    var filterQuery = "?$filter=";
    if ($("#filter-cost-set").text() != "")
    {
        switch ($("#filter-cost-set").text()) {
            case "Less than 3Lakhs":
                filterQuery += "PriceRange le "+ 300000;
                break;
            case "3Lakhs to 5lakhs":
                filterQuery += "PriceRange ge "+ 300000 + "and PriceRange le "+ 500000;
                break;
            case "More than 5lakhs":
                filterQuery += "PriceRange ge "+ 500000;
            default:
        
        }
    }

    if ($("#filter-services-set").text() != "") {
        if ($("#filter-cost-set").text() != "") {
            filterQuery += " and contains(ServiceOffered, '" + $("#filter-services-set").text() + "')";
        }
        else {
            filterQuery += "contains(ServiceOffered, '" + $("#filter-services-set").text() + ",')";
        }
        
    }

    if ($("#filter-product-set").text() != "") {
        filterQuery += "contains(ServiceOffered, '" + $("#filter-product-set").text() + ",')";
    }


    if (filterQuery == "?$filter=") {
        filterQuery = "";
    }
    $.ajax({
        url: Refurl + "/api/ServiceProvider/GetServiceProviders/" + location + "/" + searchBy + "/" + category + filterQuery,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#data-list').empty();
            $(".search-resultCount").html(data.length + " results found");
            $("#ajax").hide();
            var coordinates = [];

            for (var key in data) {
                var url = Refurl + "/home/BusinessDetails?spid=" + data[key].BusinessId;
                var verified='';
                if (data[key].IsVerified == true) {
                    verified = '<div class="verification"><span class="glyphicon glyphicon-heart" style="color:#2c6c2c; font-size:18px;"></span><span class="ratting-text">Verified</span></div>';
                }
                
                if (data[key].BusinessType == "Interior Designers" || data[key].BusinessType == "Carpenters") {
                    $("#data-list").append('<li><div style="height:50px;"><div class="pull-left" style="width:100%;"><span class="businessName"><a href="'+url+'">' + data[key].BusinessName + '&nbsp;</a></span><span class="tags">' + data[key].BusinessType + '</span></div><div class="pull-left" style="width:100%;"><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div>' + verified + '</div></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceOffered + '</div><div class="deals"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"><div style="float:left;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].ProjectCount + '</span><div style="color:#808080; font-size:12px; display:inline-block;" class="text-center">Projects</div></div><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].InBusinessSince + '</span><span style="color:#808080; font-size:12px; width:100px; display:inline-block;" class="text-center">Experience</span></div><div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].OfferCount + '</span><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;" class="text-center">Offers</span></div></div></li>');
                }
                else {
                    $("#data-list").append('<li><div style="height:50px;"><div class="pull-left" style="width:100%;"><span class="businessName"><a href="'+url+'">' + data[key].BusinessName + '&nbsp;</a></span><span class="tags">' + data[key].BusinessType + '</span></div><div class="pull-left" style="width:100%;"><div class="ratting"><div class="ratting-number">' + data[key].Ratting + '</div><div class="ratting-text" style="">' + data[key].ReviewCount + ' Reviews</div>' + verified + '</div></div></div><div class="deals"><span style="font-weight:700;">Deals in </span>' + data[key].ServiceOffered + '</div><div class="deals"><span style="font-weight:700;">Located at </span>' + data[key].Address + '</div><div class="statistics"<div style="float:left; margin-left:15px;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">'+data[key].InBusinessSince+'</span><span style="color:#808080; font-size:12px; width:100px; display:inline-block;" class="text-center">In Business Since</span></div>><div style="float:left;"><span style="display:block; color: #514e4e; font-size: 18px; font-weight:600;" class="text-center">' + data[key].OfferCount + '</span><span style="color:#808080; font-size:12px; min-width:100px; display:inline-block;" class="text-center">Offers</span></div></div></li>');
                }
                coordinates.push(new Array(data[key].Longitude, data[key].Lattitude, data[key].Address));
            }

            initialize(coordinates);
            $(function () {
                $("div.holder").jPages({
                    containerID: "data-list",
                    perPage: 2,
                    fallback: 500,
                    scrollBrowse: true,
                    keyBrowse: true
                });
            });
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}

function GetAllServiceTypes() {
    $.ajax({
        url: Refurl+'/api/ServiceProvider/GetAllServiceTypes',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            for (var key in data) {
                $("#filter-services").append('<li>' + data[key].ServieTypeName + '</li>');
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}


function GetAllProducts() {
    $.ajax({
        url: Refurl + '/api/Product/GetAllProducts',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            for (var key in data) {
                $("#filter-product").append('<li>' + data[key].ProductName + '</li>');
            }
        },
        error: function (x, y, z) {
            alert(x + '\n' + y + '\n' + z);
        }
    });
}
