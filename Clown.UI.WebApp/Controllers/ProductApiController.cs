﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Core.BusinessEntities.ProductsEntities;
using Clown.Core.DataModel;
using Clown.Core.DataModel.ProductRepo;
namespace Clown.UI.WebApp.Controllers
{
    public class ProductApiController : ApiController
    {
        IProductRepository _repository;
        public ProductApiController()
        {
            _repository = new ProductRepository(new ClownEntities());
        }
        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }
        [HttpGet]
        [Route("api/Product/GetAllProducts")]
        public IEnumerable<ProductViewModel> GetAllProducts()
        {
           return _repository.GetAllProducts();
        }
    }
}
