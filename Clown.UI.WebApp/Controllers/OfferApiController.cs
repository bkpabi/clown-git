﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Core.DataModel;
using Clown.Core.DataModel.OfferRepo;
using Clown.Core.BusinessEntities.OfferEntities;
namespace Clown.UI.WebApp.Controllers
{
    public class OfferApiController : ApiController
    {
        private IOfferRepository _repository;
        public OfferApiController()
        {
            _repository = new OfferRepository(new ClownEntities());
        }

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        [Route("api/Offer/GetHotOffers")]
        public IEnumerable<OfferListViewModel> GetHotOffers()
        {
            var result = _repository.GetHotOffers();
            List<OfferListViewModel> offerCollection = new List<OfferListViewModel>();
            foreach (var item in result)
            {
                OfferListViewModel offerEntity = new OfferListViewModel();
                offerEntity.BusinessName = item.ServiceProvider.Name;
                offerEntity.OfferDescription = item.OfferDescription;
                offerEntity.OfferId = item.Id;
                offerEntity.OfferTittle = item.OfferTittle;
                offerEntity.ValidTill = item.EndDate;
                offerEntity.Views = 100;// TODO Change this once offer views are implemented.
                offerCollection.Add(offerEntity);
            }

            return offerCollection;
        }

        [HttpGet]
        [Route("api/Offer/GetOffersByServiceProvider/{id}")]
        public IEnumerable<OfferListViewModel> GetOffersByServiceProvider(long id)
        {
            var result = _repository.GetOffersByBusiness(id);
            List<OfferListViewModel> offerCollection = new List<OfferListViewModel>();
            foreach (var item in result)
            {
                OfferListViewModel offerEntity = new OfferListViewModel();
                offerEntity.BusinessName = item.ServiceProvider.Name;
                offerEntity.OfferDescription = item.OfferDescription;
                offerEntity.OfferId = item.Id;
                offerEntity.OfferTittle = item.OfferTittle;
                offerEntity.ValidTill = item.EndDate;
                offerEntity.Views = 100;// TODO Change this once offer views are implemented.
                offerCollection.Add(offerEntity);
            }

            return offerCollection;
        }
    }
}
