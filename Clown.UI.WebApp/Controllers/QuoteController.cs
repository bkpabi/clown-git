﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clown.UI.WebApp.Controllers
{
    public class QuoteController : ApplicationBaseController
    {
        // GET: Quote
        public ActionResult TrackQuoteRequest()
        {
            return View();
        }

        public FileResult Download(string file)
        {
            if (file == string.Empty)
            {
                throw new ArgumentNullException("file", "Please provide file path.");
            }

            byte[] fileBytes = System.IO.File.ReadAllBytes(file);
            var response = new FileContentResult(fileBytes, "application/octet-stream");
            response.FileDownloadName = "loremIpsum.pdf";
            return response;
        }
    }
}