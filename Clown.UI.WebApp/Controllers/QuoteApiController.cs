﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Clown.Core.DataModel;
using Clown.Core.DataModel.Quote;
using Clown.Core.BusinessEntities.Quote;
namespace Clown.UI.WebApp.Controllers
{
    public class QuoteApiController : ApiController
    {
        private IQuoteRepository _repository;

        public QuoteApiController()
        {
            _repository = new QuoteRepository(new ClownEntities());
        }

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        [Route("api/Quote/RequestQuote")]
        public string RequestQuote(InteriorRequirment interiorRequrimentEntity)
        {
            interiorRequrimentEntity.UserId = User.Identity.GetUserId();
            interiorRequrimentEntity.Location = string.Empty;
            interiorRequrimentEntity.IsActive = true;
            interiorRequrimentEntity.RequestDate = DateTime.Today;
            var result = _repository.RequestQuote(interiorRequrimentEntity);
            if (result)
            {
                return "Details are saved successfully";
            }
            else
            {
                return "We are not able save details";
            }
        }

        [Route("api/Quote/GetRequestsByUserId")]
        public IEnumerable<TrackQuoteRequestViewModel> GetRequestsByUserId()
        {
            return _repository.GetRequestsByUser(User.Identity.GetUserId());
        }

        [Route("api/Quote/GetQuotesByRequest")]
        public IEnumerable<SubmitedQuoteViewModel> GetQuotesByRequest(string RequestId)
        {
            if (RequestId == string.Empty)
            {
                throw new ArgumentNullException("RequestId", "Please pass the requestid");
            }

            return _repository.GetQuotesByRequest(long.Parse(RequestId));
        }

        [HttpDelete]
        [Route("api/Quote/DeleteRequest/{RequestId}")]
        public string DeleteRequest(string RequestId)
        {
            if (RequestId == string.Empty)
            {
                throw new ArgumentNullException("RequestId", "Please provide valid Request id.");
            }

            var result = _repository.DeleteRequest(long.Parse(RequestId));
            if (result)
            {
                return "Request deleted successfully";
            }
            else
            {
                return "Sorry, We are unable to delete the request.";
            }
        }
    }
}
