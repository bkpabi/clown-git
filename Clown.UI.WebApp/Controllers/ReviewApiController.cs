﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Clown.Core.DataModel;
using Clown.Core.BusinessEntities.ReviewEntities;
using Clown.Core.DataModel.ReviewRepo;
namespace Clown.UI.WebApp.Controllers
{
    public class ReviewApiController : ApiController
    {
        private IReviewRepository _repository;
        public ReviewApiController()
        {
            _repository = new ReviewRepository(new ClownEntities());
        }

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [Route("api/Review/GetAllReviewsByServiceProviderId/{ServiceProviderId}")]
        public IEnumerable<ReviewViewModel> GetAllReviewsByServiceProviderId(string ServiceProviderId)
        {
            List<ReviewViewModel> reviewCollection = new List<ReviewViewModel>();
            var dbReviewCollection = _repository.GetAllReviewsByServiceProvider(long.Parse(ServiceProviderId));
            foreach (var item in dbReviewCollection)
            {
                ReviewViewModel reviewEntity = new ReviewViewModel();
                reviewEntity.CreatedOn = item.CreatedOn.ToString("MMMM dd, yyyy");
                reviewEntity.Ratting = item.Ratting;
                reviewEntity.Reviews = item.Reviews;
                reviewEntity.UserName = item.AspNetUser.FirstName + " " + item.AspNetUser.LastName;
                reviewCollection.Add(reviewEntity);
            }
            return reviewCollection;
        }

        [HttpGet]
        [Route("api/Review/WriteAReview")]
        public string WriteAReview(string rating, string review, string ServiceProvider)
        {
            if (User.Identity.IsAuthenticated)
            {
                var x = User.Identity.GetUserId();
                // User is authenticatd so we will allow him to write the review
                if (_repository.AddAReview(new Review() { Ratting = decimal.Parse(rating), Reviews = review, ServiceProviderId = int.Parse(ServiceProvider), UserId = x.ToString(), CreatedBy = User.Identity.Name, CreatedOn = DateTime.Now }))
                {
                    return "Successful";
                }
                else
                {
                    return "Failure";
                }
            }
            else
            {
                return "No loggedin user found";
            }
        }
    }
}
