﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.UI.WebApp.Models;

namespace Clown.UI.WebApp.Controllers
{
    public class HomeController : ApplicationBaseController
    {
        public ActionResult Details(string spId)
        {
            ViewBag.ServiceProviderId = spId;
            return View();
        }

        public ActionResult BusinessDetails(string spId)
        {
            ViewBag.ServiceProviderId = spId;
            return View();
        }

        public ActionResult AddToWishList(string serviceProvider)
        {
            if(User.Identity.IsAuthenticated)
            {
                // Add a record
                return View();
            }
            else
            {
                return JavaScript("$('#myModal').modal('show')");
            }
        }

        public ActionResult Index()
        {
            //throw new Exception("Sorry");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public ActionResult Intex()
        //{
        //    return View();
        //}
    }
}