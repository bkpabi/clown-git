﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Core.DataModel.WishListRepo;
using Clown.Core.DataModel;
using Clown.Core.BusinessEntities.WishListEntities;
using Microsoft.AspNet.Identity;
namespace Clown.UI.WebApp.Controllers
{
    public class WishListApiController : ApiController
    {
        private IWishListRepository _repository;
        public WishListApiController()
        {
            _repository = new WishListRepository(new ClownEntities());
        }

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        [Route("api/WishList/AddToWishList/{ServiceProviderId}")]
        public string AddToWishList(string ServiceProviderId)
        {
            if (User.Identity.IsAuthenticated)
            {
                WishList entity = new WishList();
                entity.ServiceProviderId = int.Parse(ServiceProviderId);
                entity.UserId = User.Identity.GetUserId();
                //Check if the service provider is already added to wishlist
                int resultCount = _repository.GetWishListOfAUser(User.Identity.GetUserId()).Where(e => e.BusinessId == entity.ServiceProviderId).ToList().Count();
                if (resultCount > 0)
                {
                    return "Item already added to your wish list";
                }

                if (_repository.AddToWishList(entity))
                {
                    return "Success";
                }
                else
                {
                    return "Failed to add the service provider to your wishList";
                }
                //;
            }
            else
            {
                return "No loggedin user found";
            }
        }

        [HttpGet]
        [Route("api/WishList/GetWishListForAUser")]
        public List<WishListViewModel> GetWishListForAUser()
        {
            var wishListCollection = _repository.GetWishListOfAUser(User.Identity.GetUserId());
            return wishListCollection;
        }

        [HttpDelete]
        [Route("api/WishList/DeleteWish/{WishId}")]
        public string DeleteWish(string WishId)
        {
            if (WishId == string.Empty)
            {
                throw new ArgumentException("Wishid cannot be blank.","WishId");
            }

            var result = _repository.DeleteServiceProviderFromWishList(long.Parse(WishId));
            if (result)
            {
                return "Wish deleted successfully";
            }
            else
            {
                return "Sorry, We are not able to delete the wish";
            }
        }
    }
}
