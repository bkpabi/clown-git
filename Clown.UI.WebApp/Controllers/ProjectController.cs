﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clown.UI.WebApp.Controllers
{
    public class ProjectController : ApplicationBaseController
    {
        // GET: Project
        public ActionResult Index()
        {
            return View();
        }

        [Route("Project/{spid}")]
        public ActionResult Index(string spid)
        {
            ViewBag.SPid = spid;
            return View();
        }
    }
}