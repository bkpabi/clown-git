﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clown.UI.WebApp.Controllers
{
    public class ReviewController : ApplicationBaseController
    {
        // GET: Review
        public ActionResult Index()
        {
            return View();
        }

        [Route("Review/{spid}")]
        public ActionResult Index(string spid)
        {
            ViewBag.SPID = spid;
            return View();
        }
    }
}