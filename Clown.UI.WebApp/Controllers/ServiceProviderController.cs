﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.UI.WebApp.Models;
using Clown.Core.DataModel;
using Clown.Core.DataModel.ServiceProviderRepo;
using Clown.Core.BusinessEntities;
using Clown.Core.BusinessEntities.ServiceProviderEntities;
using Microsoft.AspNet.Identity;
using System.Web.OData;
using System.Web.Http.Cors;
namespace Clown.UI.WebApp.Controllers
{

    [EnableCors(origins: "http://clownadmin.azurewebsites.net, http://localhost:7495", headers: "*", methods: "*")]
    public class ServiceProviderController : ApiController
    {
        private IServiceProviderRepository _repository;
        public ServiceProviderController()
        {
            _repository = new ServiceProviderRepository(new ClownEntities());
        }
        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Fetch details of a service providers
        /// </summary>
        /// <param name="spId">Unique id of service provider</param>
        /// <returns>Details of service provider</returns>
        [Route("api/ServiceProvider/GetServiceProviderDetails")]
        public ServiceProviderViewModel GetServiceProviderDetails(string spId)
        {
            var serviceProviderEntity = _repository.GetServiceProviderDetailsById(long.Parse(spId));
            ServiceProviderViewModel ServiceProviderEntity = new ServiceProviderViewModel();
            ServiceProviderEntity.Id = serviceProviderEntity.Id;
            ServiceProviderEntity.Name = serviceProviderEntity.Name;
            ServiceProviderEntity.ContactNumber = serviceProviderEntity.ContactNumber != null ? serviceProviderEntity.ContactNumber : string.Empty;
            ServiceProviderEntity.Website = serviceProviderEntity.Website != null ? serviceProviderEntity.Website : string.Empty;
            if (serviceProviderEntity.Reviews.Sum(e => e.Ratting) > 0)
            {
                ServiceProviderEntity.Ratting = serviceProviderEntity.Reviews.Sum(e => e.Ratting) / serviceProviderEntity.Reviews.Count;
            }
            ServiceProviderEntity.ReviewCount = serviceProviderEntity.Reviews.Count;
            ServiceProviderEntity.Verified = serviceProviderEntity.Verified == true ? "Verified" : "";
            ServiceProviderEntity.Address = serviceProviderEntity.Address_Street + ", " + serviceProviderEntity.AreaMaster.AreaName + ", " + serviceProviderEntity.CityMaster.CityName + ", " + serviceProviderEntity.Address_State;
            ServiceProviderEntity.Lattitude = serviceProviderEntity.Lattitude;
            ServiceProviderEntity.Longitude = serviceProviderEntity.Longitude;
            ServiceProviderEntity.ServiceProviderTypeName = serviceProviderEntity.ServiceProviderType.Name;
            if (ServiceProviderEntity.ServiceProviderTypeName == "Interior Designers" || ServiceProviderEntity.ServiceProviderTypeName == "Carpenters")
            {
                foreach (var item in serviceProviderEntity.ServiceProvideds.ToList())
                {
                    ServiceProviderEntity.ServiceOffered.Add(new ServiceTypeViewModel { Id = item.Id, ServieTypeName = item.ServiceType.ServieTypeName, url = item.ServiceType.ImageUrl });
                }
            }
            else
            {
                foreach (var item in serviceProviderEntity.CatlogProducts.ToList())
                {
                    ServiceProviderEntity.ServiceOffered.Add(new ServiceTypeViewModel() { Id = item.ProductMaster.Id, ServieTypeName = item.ProductMaster.Name, url = item.ProductMaster.IconUrl });
                }
            }


            ServiceProviderEntity.WorkingHrs = new List<string>();
            foreach (var whrs in serviceProviderEntity.WorkingHrs)
            {
                DateTime StartDateTime = DateTime.Today.Add(whrs.StartTime.Value);
                DateTime EndDateTime = DateTime.Today.Add(whrs.EndTime.Value);
                ServiceProviderEntity.WorkingHrs.Add(whrs.WeekDay + " : " + StartDateTime.ToString("hh:mm tt") + " to " + EndDateTime.ToString("hh:mm tt"));
            }


            foreach (var item in serviceProviderEntity.Images)
            {
                ServiceProviderEntity.Images.Add(new ServiceProviderImageListViewModel { ImageUrl = item.ImageUrl, ThumbUrl = item.ThumbUrl });
            }

            foreach (var item in serviceProviderEntity.Projects)
            {
                ServiceProviderEntity.Projects.Add(new Clown.Core.BusinessEntities.ProjectsEntities.ProjectListViewModel() { ProjectName = item.ProjectTittle, Area = item.AreaMaster.AreaName, ProjectLocation = item.AreaMaster.AreaName + ", " + item.AreaMaster.CityMaster.CityName, ProjectType = item.HouseType });
            }

            foreach (var item in serviceProviderEntity.Reviews)
            {
                ServiceProviderEntity.Reviews.Add(new Clown.Core.BusinessEntities.ReviewEntities.ReviewViewModel() { Ratting = item.Ratting, Reviews = item.Reviews, CreatedOn = item.CreatedOn.ToString("MMMM dd, yyyy"), UserName = item.AspNetUser.UserName });
            }

            foreach (var item in serviceProviderEntity.Offers)
            {
                ServiceProviderEntity.Offers.Add(new Clown.Core.BusinessEntities.OfferEntities.OfferListViewModel() { BusinessName = item.ServiceProvider.Name, OfferDescription = item.OfferDescription, OfferId = item.Id, OfferTittle = item.OfferTittle, ValidTill = item.EndDate, Views= 100 });
            }

            return ServiceProviderEntity;
        }


        /// <summary>
        /// Fetches Service Provider list according to search criteria
        /// </summary>
        /// <param name="Location">location</param>
        /// <param name="SearchBy">Search param</param>
        /// <param name="SearchCategory">Seacrch Category</param>
        /// <returns>List of Service provider</returns>
        [HttpGet]
        [EnableQuery]
        [Route("api/ServiceProvider/GetServiceProviders/{Location}/{SearchBy}/{SearchCategory}")]
        public IQueryable<ServiceProviderListViewModel> GetServiceProviders(string Location, string SearchBy, string SearchCategory)
        {
            List<ServiceProviderListViewModel> serviceProviderCollection = new List<ServiceProviderListViewModel>();
            switch (SearchCategory)
            {
                case "1":
                    serviceProviderCollection = _repository.GetServiceProvidersByCityByBusinessType(SearchBy, Location).ToList();
                    break;
                case "2":
                    serviceProviderCollection = _repository.GetServiceProvidersByCityByService(SearchBy, Location).ToList();
                    break;
                case "3":
                    serviceProviderCollection = _repository.GetServiceProvidersByCityByProduct(SearchBy, Location).ToList();
                    break;
                case "4":
                    serviceProviderCollection = _repository.GetServiceProvidersByAreaByBusinessType(SearchBy, Location).ToList();
                    break;
                case "5":
                    serviceProviderCollection = _repository.GetServiceProvidersByAreaByService(SearchBy, Location).ToList();
                    break;
                case "6":
                    serviceProviderCollection = _repository.GetServiceProvidersByAreaByProduct(SearchBy, Location).ToList();
                    break;
                default:
                    break;
            }
            return serviceProviderCollection.AsQueryable();
        }

        /// <summary>
        /// Fetches the service provider detail by name
        /// </summary>
        /// <param name="Name">Service provider name</param>
        /// <param name="Location">Location</param>
        /// <param name="Locationtype">Location Type</param>
        /// <returns>Service Provider Details</returns>
        [Route("api/ServiceProvider/GetServiceProviderDetailsByName/{Name}/{Location}/{Locationtype}")]
        public ServiceProviderListViewModel GetServiceProviderDetailsByName(string Name, string Location, string Locationtype)
        {
            if (Locationtype == "City")
            {
                return _repository.GetServiceProvidersByNameByCity(Name, Location).FirstOrDefault();
            }
            else
            {
                return _repository.GetServiceProvidersByNameByArea(Name, Location).FirstOrDefault();
            }
        }
        //------------------------------------New Method----------------------------


        [Route("api/ServiceProvider/GetAllAreasByCityTypehead/{City}")]
        [HttpGet]
        public IQueryable<AutocompleteViewModel> GetAllAreasByCityTypehead(string City)
        {
            var areaCollection = _repository.GetAearByCity(City);
            List<Models.AutocompleteViewModel> acCollection = new List<Models.AutocompleteViewModel>();
            AutocompleteViewModel ac = new AutocompleteViewModel();
            ac.value = City;
            ac.data = "City";
            acCollection.Add(ac);
            foreach (var area in areaCollection)
            {
                AutocompleteViewModel areaAC = new AutocompleteViewModel();
                areaAC.value = area.AreaName + ", " + area.CityMaster.CityName;
                areaAC.data = "Area";
                acCollection.Add(areaAC);
            }
            return acCollection.OrderBy(e => e.value).AsQueryable();
        }

        [Route("api/ServiceProvider/GetAllAreaByCity/{City}")]
        public IEnumerable<AutocompleteViewModel> GetAllAreaByCity(string City)
        {
            var resultCity = _repository.GetAearByCity(City);
            List<AutocompleteViewModel> list = new List<AutocompleteViewModel>();
            foreach (var item in resultCity)
            {
                AutocompleteViewModel newItem = new AutocompleteViewModel();
                newItem.data = item.AreaName;
                newItem.value = item.Id.ToString();
                list.Add(newItem);
            }

            return list;
        }

        [Route("api/ServiceProvider/GetSearchString")]
        [HttpGet]
        public IQueryable<Models.AutocompleteViewModel> GetSearchString(string area)
        {
            List<Models.AutocompleteViewModel> acCollection = new List<Models.AutocompleteViewModel>();

            // Populate ServiceProvider for autocomplete list
            var dbServicProviderCollection = _repository.GetAllServiceProvider();
            foreach (var item in dbServicProviderCollection)
            {
                AutocompleteViewModel ac = new Models.AutocompleteViewModel();
                ac.value = item.Name;
                ac.data = "Service Provider";
                acCollection.Add(ac);
            }

            // Populate Service Provider Type
            var dbServicProviderTypeCollection = _repository.GetAllServiceProviderTypes();
            foreach (var spt in dbServicProviderTypeCollection)
            {
                Models.AutocompleteViewModel ad = new Models.AutocompleteViewModel();
                ad.value = spt.Name;
                ad.data = "Service Provider Type";
                acCollection.Add(ad);
            }

            // Populate product
            var dbProductCollection = _repository.GetAllProducts();
            foreach (var prod in dbProductCollection)
            {
                Models.AutocompleteViewModel ad = new Models.AutocompleteViewModel();
                ad.value = prod.Name;
                ad.data = "Product";
                acCollection.Add(ad);
            }

            var dbServices = _repository.GetAllServiceTypes();
            foreach (var srv in dbServices)
            {
                Models.AutocompleteViewModel ad = new Models.AutocompleteViewModel();
                ad.value = srv.ServieTypeName;
                ad.data = "Service";
                acCollection.Add(ad);
            }
            return acCollection.Distinct().AsQueryable();
        }

        //[Route("api/ServiceProvider/FindServiceProvider")]
        //[HttpGet]
        //public IQueryable<ServiceproviderViewModel> FindServiceProvider(string location, string locationCategory, string serach, string serachCategory)
        //{
        //    List<ServiceProvider> dbServicProvider = null;
        //    Dictionary<string, string> searchParm = new Dictionary<string, string>();
        //    searchParm.Add("location", location);
        //    searchParm.Add("locationCategory", locationCategory);
        //    searchParm.Add("search", serach);
        //    searchParm.Add("searchCategory", serachCategory);

        //    dbServicProvider = _repository.Search(searchParm).ToList();

        //    List<ServiceproviderViewModel> lServiceProviderCollection = new List<ServiceproviderViewModel>();
        //    foreach (var item in dbServicProvider)
        //    {
        //        ServiceproviderViewModel eachSP = new ServiceproviderViewModel();
        //        eachSP.Id = item.Id;
        //        eachSP.Name = item.Name;
        //        eachSP.Website = item.Website;
        //        eachSP.Address = item.Address_Street + ", " + item.AreaMaster.AreaName + ", " + item.CityMaster.CityName + ", " + item.Address_State;
        //        eachSP.InBusinessSince = item.InBusinessSince;
        //        eachSP.Verified = item.Verified == true ? "Verified" : "";
        //        eachSP.ReviewCount = item.Reviews.Count;
        //        eachSP.CompletedProjects = item.Projects.Count;
        //        eachSP.Longitude = item.Longitude;
        //        eachSP.Lattitude = item.Lattitude;
        //        eachSP.OfferCount = item.Offers.Count;
        //        eachSP.ServiceProviderTypeName = item.ServiceProviderType.Name;
        //        if (item.Reviews.Sum(e => e.Ratting) > 0)
        //        {
        //            eachSP.Ratting = item.Reviews.Sum(e => e.Ratting) / item.Reviews.Count;
        //        }
        //        eachSP.ServiceProvided = string.Empty;
        //        if (eachSP.ServiceProviderTypeName == "Interior Designers" || eachSP.ServiceProviderTypeName == "Carpenters")
        //        {
        //            foreach (var sp in item.ServiceProvideds)
        //            {
        //                eachSP.ServiceProvided += ", " + sp.ServiceType.ServieTypeName;
        //            }
        //        }
        //        else
        //        {
        //            foreach (var sp1 in item.Catlogs)
        //            {
        //                foreach (var prod in sp1.CatlogProducts)
        //                {
        //                    eachSP.ServiceProvided += ", " + prod.ProductMaster.Name;
        //                }
        //            }
        //        }


        //        if (eachSP.ServiceProvided.Length > 0)
        //        {
        //            eachSP.ServiceProvided = eachSP.ServiceProvided.Remove(0, 2);
        //            if (eachSP.ServiceProvided.Length > 100)
        //            {
        //                eachSP.ServiceProvided = eachSP.ServiceProvided.Substring(0, 100);
        //                eachSP.ServiceProvided = eachSP.ServiceProvided + "...";
        //            }
        //        }
        //        lServiceProviderCollection.Add(eachSP);
        //    }
        //    return lServiceProviderCollection.AsQueryable();
        //}

        [Route("api/ServiceProvider/GetAllServiceTypes")]
        [HttpGet]
        public IQueryable<ServiceTypeViewModel> GetAllServiceType()
        {
            List<ServiceTypeViewModel> serviceTypeCollection = new List<ServiceTypeViewModel>();
            var dbServieType = _repository.GetAllServiceTypes();
            foreach (var item in dbServieType)
            {
                ServiceTypeViewModel theServiceType = new ServiceTypeViewModel();
                theServiceType.Id = item.Id;
                theServiceType.ServieTypeName = item.ServieTypeName;
                theServiceType.url = item.ImageUrl;
                serviceTypeCollection.Add(theServiceType);
            }

            return serviceTypeCollection.AsQueryable();
        }


        [HttpGet]
        [Route("api/User/IsAuthenticated")]
        public bool IsAuthenticated()
        {
            if (User.Identity.IsAuthenticated)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }
}
