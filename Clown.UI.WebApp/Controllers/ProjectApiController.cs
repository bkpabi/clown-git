﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Core.DataModel;
using Clown.Core.DataModel.ProjectRepo;
using Clown.Core.BusinessEntities.ProjectsEntities;
using System.Web.OData;
namespace Clown.UI.WebApp.Controllers
{
    public class ProjectApiController : ApiController
    {
        private IProjectRepository _repository;

        public ProjectApiController()
        {
            _repository = new ProjectRepository(new ClownEntities());
        }

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [Route("api/Project/GetProjectsByServiceProvider/{ServiceProviderId}")]
        [EnableQuery]
        public IQueryable<ProjectListViewModel> GetProjectsByServiceProvider(string ServiceProviderId)
        {
            try
            {
                var result = _repository.GetProjectsByServiceProviderId(long.Parse(ServiceProviderId)).ToList();
                List<ProjectListViewModel> projectsCollection = new List<ProjectListViewModel>();
                foreach (var item in result)
                {
                    ProjectListViewModel projectEntity = new ProjectListViewModel();
                    projectEntity.ProjectId = item.Id;
                    projectEntity.Gallery = GetProjectImagesByProjectId(item.Id.ToString()).ToList();
                    if (item.NoOfDaysTaken.HasValue)
                    {
                        projectEntity.NoOfDaysTaken = item.NoOfDaysTaken.Value;
                    }
                    else
                    {
                        projectEntity.NoOfDaysTaken = 0;
                    }
                    if (item.Cost.HasValue)
                    {
                        projectEntity.ProjectCost = item.Cost.Value;
                    }
                    else
                    {
                        projectEntity.ProjectCost = 0;
                    }
                    projectEntity.ProjectLocation = item.AreaMaster.AreaName + ", " + item.AreaMaster.CityMaster.CityName;
                    projectEntity.Area = item.AreaMaster.AreaName;
                    projectEntity.City = item.AreaMaster.CityMaster.CityName;
                    projectEntity.ProjectName = item.ProjectTittle;
                    projectEntity.ProjectType = item.HouseType;
                    projectEntity.Status = item.Status;
                    projectsCollection.Add(projectEntity);
                }

                return projectsCollection.AsQueryable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        [HttpGet]
        [Route("api/Project/GetProjectImagesByProjectId/{ProjectId}")]
        public IEnumerable<ProjectImageViewModel> GetProjectImagesByProjectId(string ProjectId)
        {
            var dbProjectImagesCollection = _repository.GetProjectImagesByProject(long.Parse(ProjectId));
            List<ProjectImageViewModel> projectImagesCollection = new List<ProjectImageViewModel>();
            foreach (var item in dbProjectImagesCollection)
            {
                ProjectImageViewModel projectImageEntity = new ProjectImageViewModel();
                projectImageEntity.ProjectId = item.ProjectId;
                projectImageEntity.ProjectImageId = item.Id;
                projectImageEntity.Link = item.ImageLink;
                projectImagesCollection.Add(projectImageEntity);
            }

            return projectImagesCollection;
        }
    }
}
