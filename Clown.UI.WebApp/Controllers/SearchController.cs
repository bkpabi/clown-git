﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Clown.UI.WebApp.Controllers
{
    public class SearchController : ApplicationBaseController
    {
        // GET: Search
        public ActionResult Index()
        {
            return View();
        }

        [Route("Search/{Location}/{SearchBy}")]
        public ActionResult Index(string Location, string SearchBy,string Category, string PreFilter)
        {
            ViewBag.Location = Location;
            ViewBag.SearchBy = SearchBy;
            ViewBag.Category = Category;
            ViewBag.PreFilter = PreFilter;
            return View();
        }
    }
}