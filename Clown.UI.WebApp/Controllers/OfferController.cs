﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Clown.UI.WebApp.Controllers
{
    
    public class OfferController : Controller
    {
        // GET: Offer
        public ActionResult Index(long id)
        {
            ViewBag.SPid = id;
            return View();
        }
    }
}