﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Clown.UI.WebApp.Startup))]
namespace Clown.UI.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
