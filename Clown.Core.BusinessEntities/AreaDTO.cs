﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class AreaDTO
    {
        public long Id { get; set; }
        public string AreaName { get; set; }
        public long CityId { get; set; }
        public string CityName { get; set; }
    }
}
