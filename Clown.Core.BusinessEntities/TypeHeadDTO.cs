﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class TypeHeadDTO
    {
        public string Value { get; set; }
        public string Key { get; set; }
        public long ItemId { get; set; }
    }
}
