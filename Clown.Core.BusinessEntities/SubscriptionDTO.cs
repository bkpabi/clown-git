﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class SubscriptionDTO
    {
        public long Id { get; set; }
        public long BusinessId { get; set; }
        public long BundledProductId { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
    }
}
