﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ProductDetailDTO
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long LineItemId { get; set; }
        public bool Availability { get; set; }
        public string Limit { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}
