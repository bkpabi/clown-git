﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ImageDTO
    {
        public long Id { get; set; }
        public long BusinessId { get; set; }
        public string ImageUrl { get; set; }
        public string ThumbUrl { get; set; }
    }
}
