﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class QuoteRequestDTO
    {
        public QuoteRequestDTO()
        {
            Quotes = new List<QuoteDTO>();
        }
        public long Id { get; set; }
        public string UserId { get; set; }
        public string RoomType { get; set; }
        public decimal CarpetArea { get; set; }
        public string Location { get; set; }
        public string KitchenShape { get; set; }
        public Nullable<decimal> KitchenWidth { get; set; }
        public Nullable<decimal> KitchenHeight { get; set; }
        public string KitchenRequriments { get; set; }
        public bool LivingRoomTVUnit { get; set; }
        public Nullable<decimal> LivingRoomTVUnitWidth { get; set; }
        public Nullable<decimal> LivingRoomTVUnitHeight { get; set; }
        public bool LivingRoomShoeRack { get; set; }
        public Nullable<decimal> LivingRoomShoeRackWidth { get; set; }
        public Nullable<decimal> LivingRoomShoeRackHeight { get; set; }
        public bool LivingRoomFalseCelling { get; set; }
        public Nullable<decimal> LivingRoomFalseCellingArea { get; set; }
        public string LivingRoomFalseCellingDesing { get; set; }
        public bool LivingRoomPujaUnit { get; set; }
        public string LivingRoomPujaUnitSize { get; set; }
        public bool LivingRoomCrockeryUnit { get; set; }
        public Nullable<decimal> LivingRoomCrockeryUnitWidth { get; set; }
        public Nullable<decimal> LivingRoomCrockeryUnitHeight { get; set; }
        public string LivingRoomRequriments { get; set; }
        public bool BedRoomWardrobes { get; set; }
        public Nullable<decimal> BedRoomWardrobesWidth { get; set; }
        public Nullable<decimal> BedRoomWardrobesHeight { get; set; }
        public Nullable<int> BedRoomWardrobesQuantity { get; set; }
        public bool BedRoomLoft { get; set; }
        public Nullable<decimal> BedRoomLoftWidth { get; set; }
        public Nullable<decimal> BedRoomLoftHeight { get; set; }
        public Nullable<int> BedRoomLoftQuantity { get; set; }
        public bool BedRoomFalseCelling { get; set; }
        public Nullable<decimal> BedRoomFalseCellingArea { get; set; }
        public string BedRoomFalseCellingDesign { get; set; }
        public Nullable<int> BedRoomFalseCellingQuantity { get; set; }
        public bool BedRoomStudyTable { get; set; }
        public Nullable<int> BedRoomStudyTableQuantity { get; set; }
        public bool BedRoomDressingTable { get; set; }
        public Nullable<int> BedRoomDressingTableQuantity { get; set; }
        public bool BedRoomTVUnit { get; set; }
        public Nullable<int> BedRoomTVUnitQuantity { get; set; }
        public string BedRoomRequriments { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime RequestDate { get; set; }

        public List<QuoteDTO> Quotes { get; set; }
    }
}
