﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class OfferDTO
    {
        public long Id { get; set; }
        public long BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string OfferTittle { get; set; }
        public string OfferDescription { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }


    }
}
