﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities.WishListEntities
{
    public class WishListViewModel
    {
        public long WishId { get; set; }
        public long BusinessId { get; set; }

        public string BusinessName { get; set; }

        public string BusinessType { get; set; }

        public decimal Ratting { get; set; }

        public int ReviewCount { get; set; }

        public bool IsVerified { get; set; }

        public string ServiceOffered { get; set; }

        public string Address { get; set; }

        public int ProjectCount { get; set; }

        public int InBusinessSince { get; set; }

        public int OfferCount { get; set; }
    }

    //public class WishListViewModel
    //{
    //    public string UserId { get; set; }

    //    public int ServiceProviderId { get; set; }
    //}

    //public class GetWishListViewModel
    //{
    //    public long Id { get; set; }

    //    public string ServiceProviderName { get; set; }

    //    public string ServiceProviderType { get; set; }

    //    public string Verified { get; set; }
    //}
}
