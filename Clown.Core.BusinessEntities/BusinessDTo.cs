﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class BusinessDTO
    {
        public BusinessDTO()
        {
            Projects = new List<ProjectDTO>();
            Catlogs = new List<ProductCatalogDTO>();
            ServiceOffered = new List<ServiceOfferdDTO>();
            Images = new List<ImageDTO>();
            Reviews = new List<ReviewDTO>();
            WorkingHrs = new List<WorkingHrsDTO>();
            Offers = new List<OfferDTO>();
        }
        public List<ProjectDTO> Projects { get; set; }
        public List<ProductCatalogDTO> Catlogs { get; set; }
        public List<ServiceOfferdDTO> ServiceOffered { get; set; }
        public List<ImageDTO> Images { get; set; }
        public List<ReviewDTO> Reviews { get; set; }
        public List<WorkingHrsDTO> WorkingHrs { get; set; }
        public List<OfferDTO> Offers { get; set; }

        public long Id { get; set; }
        public long BusinessTypeId { get; set; }
        public string BusinessType { get; set; }
        public string Name { get; set; }
        public long Address_Area { get; set; }
        public string Area { get; set; }
        public string Address_Street { get; set; }
        public long Address_City { get; set; }
        public string City { get; set; }
        public string Address_State { get; set; }
        public string Address_ZipCode { get; set; }
        public Nullable<int> InBusinessSince { get; set; }
        public Nullable<decimal> PriceRange { get; set; }
        public string ContactNumber { get; set; }
        public string Website { get; set; }
        public string Keywords { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public Nullable<bool> Verified { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
