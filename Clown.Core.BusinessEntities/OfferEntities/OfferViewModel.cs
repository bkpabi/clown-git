﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Clown.Business.Entities.OfferEntities
{
    public class OfferListViewModel
    {
        public long OfferId { get; set; }

        public string BusinessName { get; set; }

        public string OfferTittle { get; set; }

        public string OfferDescription { get; set; }

        public DateTime ValidTill { get; set; }

        public int Views { get; set; }
    }

    public class OfferSummaryViewModel
    {
        public long Offerid { get; set; }

        public string BusinessName { get; set; }

        public long BusinessId { get; set; }

        public string OfferTittle { get; set; }

        public string OfferDescription { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

    }

    public class AddOfferViewModel
    {
        public long BusinessId { get; set; }

        public string BusinessName { get; set; }

        [Required(ErrorMessage="Offer tittle is required")]
        [Display(Name="Offer Tittle")]
        public string OfferTittle { get; set; }

        [Required(ErrorMessage="Offer Description is required")]
        [Display(Name="Offer Description")]
        [MaxLength(150)]
        public string OfferDescription { get; set; }

        [Required(ErrorMessage="Start date is required")]
        [Display(Name="Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [RegularExpression(@"^([0]\d|[1][0-2])\/([0-2]\d|[3][0-1])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?$", ErrorMessage = "Invalid start date")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage="End date is required")]
        [Display(Name="End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [RegularExpression(@"^([0]\d|[1][0-2])\/([0-2]\d|[3][0-1])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?$", ErrorMessage = "Invalid end date")]
        public DateTime EndDate { get; set; }

        public bool IsActive { get; set; }
    }

    public class UpdateOfferViewModel
    {
        public UpdateOfferViewModel()
        {
            AvailableActiveStatus = new List<SelectListItem>(){new SelectListItem(){Text = "Yes", Value = "1"}, new SelectListItem(){ Text = "No", Value = "0"}};
        }
        public long OfferId { get; set; }

        public string BusinessName { get; set; }

        public long BusinessId { get; set; }

        [Required(ErrorMessage = "Offer tittle is required")]
        [Display(Name = "Offer Tittle")]
        public string OfferTittle { get; set; }

        [Required(ErrorMessage = "Offer Description is required")]
        [Display(Name = "Offer Description")]
        [MaxLength(150)]
        public string OfferDescription { get; set; }

        [Required(ErrorMessage = "Start date is required")]
        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)] 
        //[RegularExpression(@"^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$", ErrorMessage = "Invalid start date")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "End date is required")]
        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)] 
        //[RegularExpression(@"^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$", ErrorMessage = "Invalid end date")]
        public DateTime EndDate { get; set; }

        public bool IsActive { get; set; }

        public List<SelectListItem> AvailableActiveStatus { get; set; }
    }
}
