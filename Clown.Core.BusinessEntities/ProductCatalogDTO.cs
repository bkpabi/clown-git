﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ProductCatalogDTO
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public long BusinessId { get; set; }
    }
}
