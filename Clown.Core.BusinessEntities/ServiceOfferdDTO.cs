﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ServiceOfferdDTO
    {
        public long Id { get; set; }
        public long ServiceProviderId { get; set; }
        public long ServiceTypeId { get; set; }
        public string ServiceTypeName { get; set; }

        public string ServiceIcon { get; set; }
        public Nullable<bool> Highlight { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
