﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities.Quote
{
    public class TrackQuoteRequestViewModel
    {
        public DateTime RequestDate { get; set; }

        public long RequestId { get; set; }

        public int TotalViews { get; set; }

        public int QuotesSubmited { get; set; }
    }

    public class SubmitedQuoteViewModel
    {
        public long QuoteId { get; set; }

        public DateTime QuoteDate { get; set; }

        public DateTime ValidTill { get; set; }

        public decimal QuoteAmount { get; set; }

        public string Quote { get; set; }
    }
}
