﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
namespace Clown.Business.Entities.ProjectsEntities
{
    public class ProjectListViewModel
    {
        public ProjectListViewModel()
        {
            Gallery = new List<ProjectImageViewModel>();
        }
        public long ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string ProjectLocation { get; set; }

        public string Area { get; set; }

        public string City { get; set; }

        public decimal ProjectCost { get; set; }

        public int NoOfDaysTaken { get; set; }

        public string ProjectType { get; set; }

        public string Status { get; set; }

        public List<ProjectImageViewModel> Gallery { get; set; }
    }

    public class ProjectImageViewModel
    {
        public long ProjectImageId { get; set; }

        public long ProjectId { get; set; }

        public string Link { get; set; }
    }

    public class AddProjectViewModel
    {
        public AddProjectViewModel()
        {
            AvailableAreas = new List<SelectListItem>();
            AvailableHouseType = new List<SelectListItem>(){new SelectListItem(){Text = "1 BHK", Value = "1 BHK"}, new SelectListItem(){Text = "2 BHK", Value = "2 BHK"}, new SelectListItem(){Text  ="3 BHK", Value ="3 BHK"}, new SelectListItem(){Text="Villa", Value = "Villa"}, new SelectListItem(){Text = "Commercial", Value ="commercial"} };
            AvailableStatus = new List<SelectListItem>() { new SelectListItem() { Text = "In progress", Value = "In Progress" }, new SelectListItem() { Text = "Completed", Value = "Completed" } };
        }
        public string ServiceproviderName { get; set; }

        public long ServiceProviderId { get; set; }

        [Display(Name="Project Name")]
        public string ProjectName { get; set; }

        public long Area { get; set; }

        public List<SelectListItem> AvailableAreas { get; set; }

        [Display(Name="Work days")]
        public int NoOfDaysTaken { get; set; }

        [Display(Name = "Project cost(Approximate)")]
        public decimal ProjectCost { get; set; }

        public string Status { get; set; }

        public List<SelectListItem> AvailableStatus { get; set; }

        [Display(Name="House Type")]
        public string HouseType { get; set; }

        public List<SelectListItem> AvailableHouseType { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }
    }

    public class AddProjectImageViewModel
    {
        public long ProjectId { get; set; }
        public string ImagePath { get; set; }
    }

    public class ProjectSummary
    {
        public long ProjectId { get; set; }

        public string ProjectName { get; set; }

        public string Location { get; set; }

        public decimal ProjectCost { get; set; }
    }


    public class ModifyProjectViewModel
    {
        public ModifyProjectViewModel()
        {
            ProjectImages = new List<ProjectImageViewModel>();
            AvailableAreas = new List<SelectListItem>();
            AvailableHouseType = new List<SelectListItem>() { new SelectListItem() { Text = "1 BHK", Value = "1 BHK" }, new SelectListItem() { Text = "2 BHK", Value = "2 BHK" }, new SelectListItem() { Text = "3 BHK", Value = "3 BHK" }, new SelectListItem() { Text = "Villa", Value = "Villa" }, new SelectListItem() { Text = "Commercial", Value = "commercial" } };
            AvailableStatus = new List<SelectListItem>() { new SelectListItem() { Text = "In progress", Value = "In Progress" }, new SelectListItem() { Text = "Completed", Value = "Completed" } };
        }
        public long ProjectId { get; set; }

        public string ServiceproviderName { get; set; }
        public long ServiceProviderId { get; set; }

        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }

        public long Area { get; set; }

        public List<SelectListItem> AvailableAreas { get; set; }

        [Display(Name = "Work days")]
        public int NoOfDaysTaken { get; set; }

        [Display(Name = "Project cost(Approximate)")]
        public decimal ProjectCost { get; set; }

        public string Status { get; set; }

        public List<SelectListItem> AvailableStatus { get; set; }

        [Display(Name = "House Type")]
        public string HouseType { get; set; }

        public List<SelectListItem> AvailableHouseType { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public List<ProjectImageViewModel> ProjectImages { get; set; }
    }

}
