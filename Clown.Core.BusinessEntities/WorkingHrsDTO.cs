﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class WorkingHrsDTO
    {
        public long Id { get; set; }
        public long BusinessId { get; set; }
        public string WeekDay { get; set; }
        public Nullable<System.TimeSpan> StartTime { get; set; }
        public Nullable<System.TimeSpan> EndTime { get; set; }
        public bool IsOpen { get; set; }
    }
}
