﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class QuoteDTO
    {
        public long Id { get; set; }
        public long RequestId { get; set; }
        public System.DateTime ValidTill { get; set; }
        public decimal QuoteAmount { get; set; }
        public string QuoteLink { get; set; }
        public System.DateTime QuoteDate { get; set; }
    }
}
