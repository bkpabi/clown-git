﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class CityDTO
    {
        public long Id { get; set; }
        public string CityName { get; set; }
    }
}
