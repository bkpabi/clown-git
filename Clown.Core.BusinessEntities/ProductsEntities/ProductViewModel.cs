﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities.ProductsEntities
{
    public class ProductViewModel
    {
        public long ProductId { get; set; }

        public string ProductName { get; set; }
    }
}
