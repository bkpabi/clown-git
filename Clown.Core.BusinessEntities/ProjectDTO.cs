﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ProjectDTO
    {
        public ProjectDTO()
        {
            ProjectImages = new List<ProjectImageDTO>();
        }

        public long Id { get; set; }
        public long BusinessId { get; set; }
        public string BusinessName { get; set; }
        public Nullable<long> Area { get; set; }
        public string AreaName { get; set; }
        public Nullable<int> NoOfDaysTaken { get; set; }
        public string ProjectTittle { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public string Status { get; set; }
        public string HouseType { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }

        public List<ProjectImageDTO> ProjectImages { get; set; }
    }
}
