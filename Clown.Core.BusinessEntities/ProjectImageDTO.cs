﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ProjectImageDTO
    {
        public int Id { get; set; }
        public long ProjectId { get; set; }
        public string ImageLink { get; set; }
    }
}
