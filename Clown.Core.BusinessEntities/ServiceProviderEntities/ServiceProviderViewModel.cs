﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace Clown.Business.Entities.ServiceProviderEntities
{
    /// <summary>
    /// Used for stroing details of a particular service provider
    /// </summary>
    public class ServiceProviderViewModel
    {
        public ServiceProviderViewModel()
        {
            ServiceOffered = new List<ServiceTypeViewModel>();
            Reviews = new List<ReviewEntities.ReviewViewModel>();
            Images = new List<ServiceProviderImageListViewModel>();
            Projects = new List<ProjectsEntities.ProjectListViewModel>();
            Offers = new List<OfferEntities.OfferListViewModel>();
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public Nullable<decimal> TotalExperience { get; set; }
        public int CompletedProjects { get; set; }
        public decimal Ratting { get; set; }
        public int ReviewCount { get; set; }
        public int OfferCount { get; set; }
        //public string ServiceProvided { get; set; }
        public string Verified { get; set; }
        public string Longitude { get; set; }
        public string Lattitude { get; set; }
        public int ServiceProviderTypeId { get; set; }
        public string ServiceProviderTypeName { get; set; }
        public string Address_Area { get; set; }
        public string Address_Street { get; set; }
        public string Address_City { get; set; }
        public string Address_State { get; set; }

        public List<OfferEntities.OfferListViewModel> Offers { get; set; }
        public List<string> WorkingHrs { get; set; }
        public List<ServiceProviderImageListViewModel> Images { get; set; }
        public List<ProjectsEntities.ProjectListViewModel> Projects { get; set; }
        public List<ReviewEntities.ReviewViewModel> Reviews { get; set; }
        public List<ServiceTypeViewModel> ServiceOffered { get; set; }
        public string ContactNumber { get; set; }
        public string PriceRange { get; set; }
        public string Tags { get; set; }
    }

    /// <summary>
    /// Used for adding new Service provider
    /// </summary>
    public class AddServiceProviderViewModel
    {
        public AddServiceProviderViewModel()
        {
            AvailableAreas = new List<SelectListItem>();
            AvailableCities = new List<SelectListItem>();
            AvailableBusinessTypes = new List<SelectListItem>();
        }

        [Required(ErrorMessage = "Business Type is required")]
        [Display(Name = "Business Type")]
        public long BusinessType { get; set; }
        public IList<SelectListItem> AvailableBusinessTypes { get; set; }

        [Required(ErrorMessage = "Business Name is required")]
        [Display(Name = "Business name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Area is required")]
        public long Area { get; set; }

        public IList<SelectListItem> AvailableAreas { get; set; }

        [Required(ErrorMessage = "Street is required")]
        public string Street { get; set; }

        [Required(ErrorMessage = "City is required")]
        public long City { get; set; }
        public IList<SelectListItem> AvailableCities { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        [Display(Name = "In Business Since")]
        [RegularExpression(@"^(?:(?:19|20)[0-9]{2})$", ErrorMessage = "Not a valid year")]
        public int InBusinessSince { get; set; }

        [Display(Name = "Price Range")]
        public decimal PriceRange { get; set; }

        [RegularExpression(@"^\+?\d[\d -]{8,12}\d$", ErrorMessage = "Contact number is not in proper format.")]
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }

        [RegularExpression(@"^(www)\.[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$", ErrorMessage = "Website is not in proper format")]
        public string Website { get; set; }

        [Display(Name = "Map Co-ordinates")]
        [RegularExpression(@"^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$", ErrorMessage = "Co-ordinates are not in proper format")]
        public string Coordinates { get; set; }

        public string Longitude { get; set; }

        public string Lattitude { get; set; }

        public bool Verified { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Monday start time is invalid")]
        public DateTime MonStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Monday end time is invalid")]
        public DateTime MonEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Tuesday start time is invalid")]
        public DateTime TueStartTime { get; set; }
        
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Tuesday end time is invalid")]
        public DateTime TueEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Wednesday start time is invalid")]
        public DateTime WedStartTime { get; set; }
        
        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Wednesday end time is invalid")]
        public DateTime WedEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Thrusday start time is invalid")]
        public DateTime ThrusStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Thrusday end time is invalid")]
        public DateTime ThrusEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Friday start time is invalid")]
        public DateTime FriStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Friday end time is invalid")]
        public DateTime FriEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Saturday start time is invalid")]
        public DateTime SatStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Saturday end time is invalid")]
        public DateTime SatEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Sunday start time is invalid")]
        public DateTime SunStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Sunday end time is invalid")]
        public DateTime SunEndTime { get; set; }
    }

    /// <summary>
    /// Used for modify details of existing service provider
    /// </summary>
    public class ModifyServiceProviderViewModel
    {
        public ModifyServiceProviderViewModel()
        {
            AvailableAreas = new List<SelectListItem>();
            AvailableCities = new List<SelectListItem>();
            AvailableBusinessTypes = new List<SelectListItem>();
            ProjectSummary = new List<ProjectsEntities.ProjectSummary>();
            OfferSummary = new List<OfferEntities.OfferSummaryViewModel>();
            AvailableServices = new List<SelectListItem>();
        }

        public long BusinessId { get; set; }

        [Required(ErrorMessage = "Business Type is required")]
        [Display(Name = "Business Type")]
        public long BusinessType { get; set; }
        public IList<SelectListItem> AvailableBusinessTypes { get; set; }

        [Required(ErrorMessage = "Business Name is required")]
        [Display(Name = "Business name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Area is required")]
        public long Area { get; set; }

        public IList<SelectListItem> AvailableAreas { get; set; }

        [Required(ErrorMessage = "Street is required")]
        public string Street { get; set; }

        [Required(ErrorMessage = "City is required")]
        public long City { get; set; }
        public IList<SelectListItem> AvailableCities { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        [Display(Name = "In Business Since")]
        [RegularExpression(@"^(?:(?:19|20)[0-9]{2})$", ErrorMessage = "Not a valid year")]
        public int InBusinessSince { get; set; }

        [Display(Name = "Price Range")]
        public decimal PriceRange { get; set; }

        [RegularExpression(@"^\+?\d[\d -]{8,12}\d$", ErrorMessage = "Contact number is not in proper format.")]
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }

        [RegularExpression(@"^(www)\.[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$", ErrorMessage = "Website is not in proper format")]
        public string Website { get; set; }

        [Display(Name = "Map Co-ordinates")]
        [RegularExpression(@"^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$", ErrorMessage = "Co-ordinates are not in proper format")]
        public string Coordinates { get; set; }

        public string Longitude { get; set; }

        public string Lattitude { get; set; }

        public bool Verified { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public List<OfferEntities.OfferSummaryViewModel> OfferSummary { get; set; }

        public List<ProjectsEntities.ProjectSummary> ProjectSummary { get; set; }

        [Required(ErrorMessage="Please select atleat one service")]
        [Display(Name = "Service Offered")]
        public List<string> SelectedServices { get; set; }

        [Required(ErrorMessage="Please select atleast one product")]
        [Display(Name = "Products Sold")]
        public List<string> SelectedProducts { get; set; }

        public List<SelectListItem> AvailableServices { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Monday start time is invalid")]
        public DateTime MonStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Monday end time is invalid")]
        public DateTime MonEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Tuesday start time is invalid")]
        public DateTime TueStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Tuesday end time is invalid")]
        public DateTime TueEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Wednesday start time is invalid")]
        public DateTime WedStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Wednesday end time is invalid")]
        public DateTime WedEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Thrusday start time is invalid")]
        public DateTime ThrusStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Thrusday end time is invalid")]
        public DateTime ThrusEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Friday start time is invalid")]
        public DateTime FriStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Friday end time is invalid")]
        public DateTime FriEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:H:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Saturday start time is invalid")]
        public DateTime SatStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Saturday end time is invalid")]
        public DateTime SatEndTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Sunday start time is invalid")]
        public DateTime SunStartTime { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        //[RegularExpression(@"^([0-9]|[0-1][0-9]|[2][0-3]):([0-5][0-9])(\s{0,1})(AM|PM|am|pm|aM|Am|pM|Pm{2,2})$", ErrorMessage = "Sunday end time is invalid")]
        public DateTime SunEndTime { get; set; }



    }

    /// <summary>
    /// Used for display list of service provider in a grid
    /// </summary>
    public class ServiceProviderSummaryViewModel
    {
        [Display(Name = "Business Id")]
        public long BusinessId { get; set; }

        [Display(Name = "Business Type")]
        public string BusinessType { get; set; }

        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }

        public string Address { get; set; }
    }

    /// <summary>
    /// Used for listing service provider
    /// </summary>
    public class ServiceProviderListViewModel
    {
        public long BusinessId { get; set; }

        public string BusinessName { get; set; }

        public string BusinessType { get; set; }

        public decimal Ratting { get; set; }

        public int ReviewCount { get; set; }

        public bool IsVerified { get; set; }

        public string ServiceOffered { get; set; }

        public string Address { get; set; }

        public int ProjectCount { get; set; }

        public int? InBusinessSince { get; set; }

        public decimal? PriceRange { get; set; }

        public int OfferCount { get; set; }

        public string Longitude { get; set; }

        public string Lattitude { get; set; }
    }

    /// <summary>
    /// Used for adding new image for service provider
    /// </summary>
    public class AddServiceProviderImageViewModel
    {

    }

    /// <summary>
    /// Used for store modified details of image for service provider
    /// </summary>
    public class ModifyServiceProviderImageViewModel
    {

    }
    /// <summary>
    /// Used for 
    /// </summary>
    public class ServiceProviderImageListViewModel
    {
        public long Id { get; set; }
        public string ImageUrl { get; set; }
        public string ThumbUrl { get; set; }
    }
    #region--City
    /// <summary>
    /// Used for store details of new city
    /// </summary>
    public class AddCityViewModel
    {

    }

    /// <summary>
    /// Used for store details of modified city
    /// </summary>
    public class ModifyCity
    {

    }

    /// <summary>
    /// Used for store details of a particular city
    /// </summary>
    public class CityViewModel
    {

    }

    /// <summary>
    /// Used for store details of summary of city
    /// </summary>
    public class CitySummaryViewModel
    {

    }
    #endregion

    #region -- Area --
    /// <summary>
    /// Used for store details of new area
    /// </summary>
    public class AddAreaViewModel
    {

    }

    /// <summary>
    /// Used for storing details of modified Area
    /// </summary>
    public class ModifyAreaViewModel
    {

    }

    /// <summary>
    /// Used for storing details of a particular area
    /// </summary>
    public class AreaViewModel
    {

    }

    /// <summary>
    /// Used for storing details of area summary
    /// </summary>
    public class AreaSummaryViewModel
    {

    }
    #endregion

    #region -- Service provider Type
    /// <summary>
    /// Used for storing details of particular service provider type 
    /// </summary>
    public class ServiceTypeViewModel
    {
        public long Id { get; set; }
        public string ServieTypeName { get; set; }
        public string url { get; set; }
    }
    #endregion
}
