﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clown.Business.Entities
{
    public class ReviewDTO
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public long BusinessId { get; set; }
        public decimal Ratting { get; set; }
        public string Reviews { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    }
}
