﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Business.Entities;
using Clown.Persistance.DataModel;
using AutoMapper;
using System.Web.Http.Cors;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Data;
namespace Clown.Business.WebApi.Controllers
{
    [EnableCors(origins: "http://clownadmin.azurewebsites.net, http://localhost:7495", headers: "*", methods: "*")]
    public class BaseController : ApiController
    {
        private IUnitOfWork unitOfWork;
        private LogWriterFactory factory;
        public BaseController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            factory = new LogWriterFactory();
        }

        [Route("api/BusinessType")]
        public IEnumerable<BusinessTypeDTO> GetAllBusinessTypes()
        {
            var dbBusinessTypeCollection = this.unitOfWork.BusinessTypeRepository.Get().ToList();
            var mBusinessTypeCollection = Mapper.Map<IList<ServiceProviderType>, IList<BusinessTypeDTO>>(dbBusinessTypeCollection);
            return mBusinessTypeCollection;
        }

        [Route("api/City")]
        public IEnumerable<CityDTO> GetAllCities()
        {
            var dbCityCollection = this.unitOfWork.CityRepository.Get().ToList();
            var mCityCollection = Mapper.Map<IList<CityMaster>, IList<CityDTO>>(dbCityCollection);
            return mCityCollection;
        }

        [Route("api/Area")]
        public IEnumerable<AreaDTO> GetAllAreas()
        {
            var dbAreaCollection = this.unitOfWork.AreaRepository.Get(includeProperties: "CityMaster").ToList();
            var mAreaCollection = Mapper.Map<IList<AreaMaster>, IList<AreaDTO>>(dbAreaCollection);
            return mAreaCollection;
        }
        [Route("api/Area/ByCity/{Id}")]
        public IEnumerable<AreaDTO> GetAreasByCity(long Id)
        {
            var dbAreaCollection = this.unitOfWork.AreaRepository.Get(e => e.CityId == Id, includeProperties: "CityMaster").ToList();
            var mAreaCollection = Mapper.Map<IList<AreaMaster>, IList<AreaDTO>>(dbAreaCollection);
            return mAreaCollection;
        }

        [Route("api/Service")]
        public IEnumerable<ServiceDTO> GetAllService()
        {
            var dbServiceCollection = this.unitOfWork.ServiceRepository.Get().ToList();
            var mServiceCollection = Mapper.Map<IList<ServiceType>, IList<ServiceDTO>>(dbServiceCollection);
            return mServiceCollection;
        }
        [Route("api/Product")]
        public IEnumerable<ProductMasterDTO> GetAllProducts()
        {
            var dbProductCollection = this.unitOfWork.ProductMasterRepository.Get().ToList();
            var mProductCollection = Mapper.Map<IList<ProductMaster>, IList<ProductMasterDTO>>(dbProductCollection);
            return mProductCollection;
        }
        
        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
