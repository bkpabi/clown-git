﻿using Clown.Business.Entities;
using Clown.Persistance.DataModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
//using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Clown.Business.WebApi.Controllers
{
    public class BusinessController : ApiController
    {
        private IUnitOfWork unitOfWork;
        private LogWriterFactory factory;
        //constructor
        public BusinessController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            factory = new LogWriterFactory();
        }

        [HttpGet]
        [Route("api/Business/GetById/{Id}")]
        public BusinessDTO GetBusinessDetailById(long Id)
        {
            var dbBusiness = this.unitOfWork.BusinessRepository.Get(e => e.Id == Id, includeProperties: "Images,ServiceProvideds,CatlogProducts,AreaMaster,CityMaster,ServiceProviderType,WorkingHrs,Projects,Projects.AreaMaster,Offers").FirstOrDefault();
            var mBusiness = AutoMapper.Mapper.Map<ServiceProvider, BusinessDTO>(dbBusiness);
            return mBusiness;
        }

        [HttpGet]
        [Route("api/Business/GetAllBusiness")]
        public IEnumerable<BusinessDTO> GetAllBusiness()
        {
            try
            {
                var businessList = this.unitOfWork.BusinessRepository.Get(includeProperties: "ServiceProvideds,AreaMaster,CityMaster,ServiceProviderType").ToList();
                var mBusinessList = AutoMapper.Mapper.Map<IList<ServiceProvider>, IList<BusinessDTO>>(businessList);
                return mBusinessList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

        [HttpPost]
        [Route("api/Business/Add")]
        public bool AddNewBusiness(BusinessDTO businessDTO)
        {
            if (businessDTO == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "BusinessDTO can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbBusinessEntity = AutoMapper.Mapper.Map<BusinessDTO, ServiceProvider>(businessDTO);
                    this.unitOfWork.BusinessRepository.Insert(dbBusinessEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }

                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [HttpPost]
        [Route("api/Business/Update")]
        public bool UpdateBusinessDetails(BusinessDTO businessDTO)
        {
            if (businessDTO == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "BusinessDTO can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var businessEntiity = this.unitOfWork.BusinessRepository.Get(e => e.Id == businessDTO.Id, includeProperties: "ServiceProvideds,CatlogProducts").FirstOrDefault();
                    //var businessEntiity = this.unitOfWork.BusinessRepository.GetByID(businessDTO.Id);
                    //businessEntiity = AutoMapper.Mapper.Map<BusinessDTO, ServiceProvider>(businessDTO, businessEntiity);
                    businessEntiity.Address_Area = businessDTO.Address_Area;
                    businessEntiity.Address_City = businessDTO.Address_City;
                    businessEntiity.Address_State = businessDTO.Address_State;
                    businessEntiity.Address_Street = businessDTO.Address_Street;
                    businessEntiity.Address_ZipCode = businessDTO.Address_ZipCode;
                    businessEntiity.ContactNumber = businessDTO.ContactNumber;
                    businessEntiity.InBusinessSince = businessDTO.InBusinessSince;
                    businessEntiity.Lattitude = businessDTO.Lattitude;
                    businessEntiity.Longitude = businessDTO.Longitude;
                    businessEntiity.ModifiedBy = businessDTO.ModifiedBy;
                    businessEntiity.ModifiedOn = businessDTO.ModifiedOn;
                    businessEntiity.Name = businessDTO.Name;
                    businessEntiity.PriceRange = businessDTO.PriceRange;
                    businessEntiity.ServiceProviderTypeId = businessDTO.ServiceProviderTypeId;
                    businessEntiity.Verified = businessDTO.Verified;
                    businessEntiity.Website = businessDTO.Website;


                    if (businessDTO.ServiceProviderTypeId == 1 || businessDTO.ServiceProviderTypeId == 2)
                    {

                        var dbServiceProvidedList = businessEntiity.ServiceProvideds.ToList();
                        foreach (var item in dbServiceProvidedList)
                        {
                            var x = businessDTO.ServiceOffered.SingleOrDefault(e => e.ServiceTypeId == item.ServiceTypeId);
                            if (x == null)
                            {
                                this.unitOfWork.ServiceProvidedRepository.Delete(item.Id);
                            }
                        }

                        var newServiceProvidedList = businessDTO.ServiceOffered.ToList();
                        foreach (var item in newServiceProvidedList)
                        {
                            var x = this.unitOfWork.ServiceProvidedRepository.Get(e => e.ServiceProviderId == businessDTO.Id).FirstOrDefault(e => e.ServiceTypeId == item.ServiceTypeId);
                            //var x = businessEntiity.ServiceProvideds.FirstOrDefault(e => e.ServiceTypeId == item.ServiceTypeId);
                            if (x == null)
                            {
                                var newItem = AutoMapper.Mapper.Map<ServiceOfferdDTO, ServiceProvided>(item);
                                this.unitOfWork.ServiceProvidedRepository.Insert(newItem);
                                //dbServiceProvider.ServiceProvideds.Add(item);
                            }
                        }
                    }
                    else
                    {
                        var dbCatlogList = businessEntiity.CatlogProducts.ToList();
                        foreach (var item in dbCatlogList)
                        {
                            var x = businessDTO.Catlogs.SingleOrDefault(e => e.ProductId == item.ProductId);
                            if (x == null)
                            {
                                this.unitOfWork.CatlogRepository.Delete(item.Id);
                            }
                        }

                        var newCatlogList = businessDTO.Catlogs.ToList();
                        foreach (var item in newCatlogList)
                        {
                            var x = this.unitOfWork.CatlogRepository.Get(e => e.ServiceProviderId == businessDTO.Id).FirstOrDefault(e => e.ProductId == item.ProductId);
                            //var x = businessEntiity.ServiceProvideds.FirstOrDefault(e => e.ServiceTypeId == item.ServiceTypeId);
                            if (x == null)
                            {
                                var newItem = AutoMapper.Mapper.Map<ProductCatalogDTO, CatlogProduct>(item);
                                this.unitOfWork.CatlogRepository.Insert(newItem);
                                //dbServiceProvider.ServiceProvideds.Add(item);
                            }
                        }
                    }

                    var workingHrs = businessDTO.WorkingHrs.ToList();
                    foreach (var item in workingHrs)
                    {
                        var whr = this.unitOfWork.WorkingHrsRepository.Get(e => e.ServiceProviderId == businessDTO.Id && e.WeekDay == item.WeekDay).SingleOrDefault();
                        //var whr = clownContext.WorkingHrs.FirstOrDefault(e => e.WeekDay == item.WeekDay && e.ServiceProviderId == item.ServiceProviderId);
                        if (whr != null)
                        {
                            whr.StartTime = item.StartTime;
                            whr.EndTime = item.EndTime;
                            whr.IsOpen = item.IsOpen;
                            this.unitOfWork.WorkingHrsRepository.Update(whr);
                        }
                    }

                    foreach (var item in businessDTO.Images)
                    {
                        var newImage = AutoMapper.Mapper.Map<ImageDTO, Image>(item);
                        newImage.ServiceProviderId = businessDTO.Id;
                        this.unitOfWork.ImageRepository.Insert(newImage);
                    }

                    //businessEntiity.ServiceProvideds = businessDTO.ServiceOffered;
                    this.unitOfWork.BusinessRepository.Update(businessEntiity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [HttpPost]
        [Route("api/Business/Delete/{Id}")]
        public bool DeleteBusinessDetails(long Id)
        {
            try
            {
                var dbBusinessEntity = this.unitOfWork.BusinessRepository.Get(e => e.Id == Id, includeProperties: "Images,WorkingHrs,Offers,Projects,Projects.ProjectImages,ServiceProvideds,CatlogProducts,Reviews,WishLists").SingleOrDefault();
                dbBusinessEntity.WorkingHrs.ToList().ForEach(p => this.unitOfWork.WorkingHrsRepository.Delete(p));
                dbBusinessEntity.Offers.ToList().ForEach(p => this.unitOfWork.OfferRepository.Delete(p));
                dbBusinessEntity.ServiceProvideds.ToList().ForEach(p => this.unitOfWork.ServiceProvidedRepository.Delete(p));
                dbBusinessEntity.CatlogProducts.ToList().ForEach(p => this.unitOfWork.CatlogRepository.Delete(p));
                dbBusinessEntity.Reviews.ToList().ForEach(p => this.unitOfWork.ReviewRepository.Delete(p));
                dbBusinessEntity.Images.ToList().ForEach(p => this.unitOfWork.ImageRepository.Delete(p));
                dbBusinessEntity.WishLists.ToList().ForEach(p => this.unitOfWork.WishListRepository.Delete(p));

                foreach (var item in dbBusinessEntity.Projects)
                {
                    foreach (var eachProjectImage in item.ProjectImages)
                    {
                        string image = eachProjectImage.ImageLink.Split('/').Last();
                        // Delete the image from CDN
                        // Retrieve storage account from connection string.
                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

                        // Create the blob client.
                        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                        // Retrieve reference to a previously created container.
                        CloudBlobContainer container = blobClient.GetContainerReference("images");

                        // Retrieve reference to a blob named "myblob.txt".
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

                        // Delete the blob.
                        blockBlob.Delete();
                    }
                    item.ProjectImages.ToList().ForEach(p => this.unitOfWork.ProjectImageRepository.Delete(p));
                }
                //Remove projects
                dbBusinessEntity.Projects.ToList().ForEach(p => this.unitOfWork.ProjectRepository.Delete(p));

                this.unitOfWork.BusinessRepository.Delete(Id);
                this.unitOfWork.Save();
                return true;
            }
            catch (DataException dbEx)
            {
                // Log Error 
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbEx.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(dbEx.Message),
                    ReasonPhrase = dbEx.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = ex.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route("api/Business/Image/GetById/{Id}")]
        public ImageDTO GetImageById(int id)
        {
            var image = this.unitOfWork.ImageRepository.Get(e => e.Id == id, includeProperties: "ServiceProvider").SingleOrDefault();
            var mappedImage = AutoMapper.Mapper.Map<Image, ImageDTO>(image);
            return mappedImage;
        }

        [HttpPost]
        [Route("api/Business/Image/Delete/{Id}")]
        public bool DeleteImage(int id)
        {
            this.unitOfWork.ImageRepository.Delete(id);
            this.unitOfWork.Save();
            return true;
        }

        [HttpGet]
        [Route("api/Business/GetSubscription")]
        public SubscriptionDTO GetSubscription(long BusinessId)
        {
            var dbSubscription = this.unitOfWork.SubsciptionRepository.Get(e => e.StartDate <= DateTime.Today && e.EndDate >= DateTime.Today && e.BusinessId == BusinessId).FirstOrDefault();
            var mSubsciption = AutoMapper.Mapper.Map<Subscription, SubscriptionDTO>(dbSubscription);
            return mSubsciption;
        }

        public bool PurchaseSubscription(SubscriptionDTO subscription)
        {
            var mSubscription = AutoMapper.Mapper.Map<SubscriptionDTO, Subscription>(subscription);
            this.unitOfWork.SubsciptionRepository.Insert(mSubscription);
            return true;
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
