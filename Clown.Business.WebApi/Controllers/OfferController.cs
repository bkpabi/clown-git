﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Business.Entities;
using Clown.Persistance.DataModel;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Clown.Business.WebApi.Controllers
{
    public class OfferController : ApiController
    {
        private IUnitOfWork unitOfWork;
        private LogWriterFactory factory;
        //constructor
        public OfferController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            factory = new LogWriterFactory();
        }

        [HttpGet]
        [Route("api/Offer/GetById/{Id}")]
        public OfferDTO GetOfferDetailsById(long Id)
        {
            var dbOffer = this.unitOfWork.OfferRepository.Get(e => e.Id == Id, includeProperties: "ServiceProvider").SingleOrDefault();
            if (dbOffer == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "No offer found for the given id"
                };
                throw new HttpResponseException(resp);
            }
            var mOffer = AutoMapper.Mapper.Map<Offer, OfferDTO>(dbOffer);
            return mOffer;
        }

        [HttpGet]
        [Route("api/Offer/GetOffersByBusiness/{BusinessId}")]
        public IEnumerable<OfferDTO> GetOffersByBusiness(long BusinessId)
        {
            try
            {
                var offerList = this.unitOfWork.OfferRepository.Get(e => e.ServiceProviderId == BusinessId, includeProperties: "ServiceProvider").ToList();
                var mOfferList = AutoMapper.Mapper.Map<IList<Offer>, IList<OfferDTO>>(offerList);
                return mOfferList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(dbx.Message),
                    ReasonPhrase = dbx.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = ex.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
        }

        [Route("api/Offer/Add")]
        public bool AddNewOffer(OfferDTO OfferDTO)
        {
            if (OfferDTO == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "OfferDTO can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbOfferEntity = AutoMapper.Mapper.Map<OfferDTO, Offer>(OfferDTO);
                    this.unitOfWork.OfferRepository.Insert(dbOfferEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [Route("api/Offer/Update")]
        public bool UpdateOfferDetails(OfferDTO offerDTO)
        {
            if (offerDTO == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "OfferDTO can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var offerEntity = this.unitOfWork.OfferRepository.GetByID(offerDTO.Id);
                    offerEntity.IsActive = offerDTO.IsActive;
                    offerEntity.OfferDescription = offerDTO.OfferDescription;
                    offerEntity.OfferTittle = offerDTO.OfferTittle;
                    offerEntity.StartDate = offerDTO.StartDate;
                    offerEntity.EndDate = offerDTO.EndDate;
                    //businessEntiity.ServiceProvideds = businessDTO.ServiceOffered;
                    this.unitOfWork.OfferRepository.Update(offerEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [HttpDelete]
        [Route("api/Offer/Delete/{Id}")]
        public bool DeleteOffer(long Id)
        {
            try
            {
                this.unitOfWork.OfferRepository.Delete(Id);

                this.unitOfWork.Save();
                return true;
            }
            catch (DataException dbEx)
            {
                // Log Error 
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbEx.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(dbEx.Message),
                    ReasonPhrase = dbEx.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = ex.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
