﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Business.Entities;
using Clown.Persistance.DataModel;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
namespace Clown.Business.WebApi.Controllers
{
    public class ProjectController : ApiController
    {
        private IUnitOfWork unitOfWork;
        private LogWriterFactory factory;
        //constructor
        public ProjectController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            factory = new LogWriterFactory();
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        [Route("api/Project/GetById/{Id}")]
        public ProjectDTO GetProjectDetailsById(long Id)
        {
            var dbProject = this.unitOfWork.ProjectRepository.Get(e => e.Id == Id, includeProperties: "ProjectImages,ServiceProvider").SingleOrDefault();
            if (dbProject == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "No projects found for the given id"
                };
                throw new HttpResponseException(resp);
            }
            var mProject = AutoMapper.Mapper.Map<Project, ProjectDTO>(dbProject);
            return mProject;
        }

        [HttpGet]
        [Route("api/Project/GetAllProjectsByBusinessId/{BusinessId}")]
        public IEnumerable<ProjectDTO> GetAllProjectsByBusinessId(long BusinessId)
        {
            try
            {
                var projectList = this.unitOfWork.ProjectRepository.Get(e => e.ServiceProviderId == BusinessId, includeProperties: "ProjectImages").ToList();
                var mProjectList = AutoMapper.Mapper.Map<IList<Project>, IList<ProjectDTO>>(projectList);
                return mProjectList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

        [Route("api/Project/Add")]
        public bool AddNewProject(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "ProjectDTO can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbProjectEntity = AutoMapper.Mapper.Map<ProjectDTO, Project>(projectDTO);
                    this.unitOfWork.ProjectRepository.Insert(dbProjectEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }
        [Route("api/Project/Update")]
        public bool UpdateProject(ProjectDTO projectDTO)
        {
            if (projectDTO == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "ProjectDTO can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    //var dbProjectEntity = AutoMapper.Mapper.Map<ProjectDTO, Project>(projectDTO);
                    var dbProjectEntity = this.unitOfWork.ProjectRepository.GetByID(projectDTO.Id);
                    dbProjectEntity.Area = projectDTO.Area;
                    dbProjectEntity.Cost = projectDTO.Cost;
                    dbProjectEntity.HouseType = projectDTO.HouseType;
                    dbProjectEntity.ModifiedBy = projectDTO.ModifiedBy;
                    dbProjectEntity.ModifiedOn = projectDTO.ModifiedOn;
                    dbProjectEntity.NoOfDaysTaken = projectDTO.NoOfDaysTaken;
                    dbProjectEntity.ProjectTittle = projectDTO.ProjectTittle;
                    //dbProjectEntity.ServiceProviderId = projectDTO.BusinessId;
                    dbProjectEntity.Status = projectDTO.Status;
                    this.unitOfWork.ProjectRepository.Update(dbProjectEntity);

                    foreach (var item in projectDTO.ProjectImages)
                    {
                        ProjectImage newProjectImage = new ProjectImage();
                        newProjectImage.ImageLink = item.ImageLink;
                        newProjectImage.ProjectId = dbProjectEntity.Id;
                        this.unitOfWork.ProjectImageRepository.Insert(newProjectImage);
                    }

                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [HttpPost]
        [Route("api/Project/Delete/{Id}")]
        public bool DeleteProject(long Id)
        {
            try
            {
                var dbProjectImageCollection = this.unitOfWork.ProjectRepository.Get(e=> e.Id == Id,includeProperties:"ProjectImages").SingleOrDefault().ProjectImages.ToList();
                foreach (var item in dbProjectImageCollection)
                {
                    this.unitOfWork.ProjectImageRepository.Delete(item.Id);
                }

                this.unitOfWork.ProjectRepository.Delete(Id);

                this.unitOfWork.Save();
                return true;
            }
            catch (DataException dbEx)
            {
                // Log Error 
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbEx.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(dbEx.Message),
                    ReasonPhrase = dbEx.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = ex.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
        }

        

        [HttpGet]
        [Route("api/Project/Image/GetById/{Id}")]
        public ProjectImageDTO GetProjectImageById(long Id)
        {
            var projectImage = this.unitOfWork.ProjectImageRepository.GetByID(Id);
            var projectImageDTO = AutoMapper.Mapper.Map<ProjectImage, ProjectImageDTO>(projectImage);
            return projectImageDTO;
        }
        [Route("api/Project/Image/GetByProject/{projectId}")]
        public IEnumerable<ProjectImageDTO> GetAllImagesByProject(long projectId)
        {
            var projectImages = this.unitOfWork.ProjectImageRepository.Get(e=> e.ProjectId == projectId).ToList();
            var projectImageCollection = AutoMapper.Mapper.Map<IList<ProjectImage>, IList<ProjectImageDTO>>(projectImages);
            return projectImageCollection;
        }

        [HttpPost]
        [Route("api/Project/Image/Delete/{Id}")]
        public bool DeleteProjectImage(int Id)
        {
            try
            {
                var dbProjectEntity = this.unitOfWork.ProjectRepository.GetByID(Id);
                this.unitOfWork.ProjectImageRepository.Delete(Id);

                this.unitOfWork.Save();
                return true;
            }
            catch (DataException dbEx)
            {
                // Log Error 
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbEx.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(dbEx.Message),
                    ReasonPhrase = dbEx.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = ex.InnerException.ToString()
                };
                throw new HttpResponseException(resp);
            }
        }
    }
}
