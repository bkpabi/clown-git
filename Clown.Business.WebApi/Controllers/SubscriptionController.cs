﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Clown.Business.Entities;
using Clown.Persistance.DataModel;
using AutoMapper;
using System.Web.Http.Cors;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Data;
namespace Clown.Business.WebApi.Controllers
{
    [EnableCors(origins: "http://clownadmin.azurewebsites.net, http://localhost:7495", headers: "*", methods: "*")]
    public class SubscriptionController : ApiController
    {
        private IUnitOfWork unitOfWork;
        private LogWriterFactory factory;

        public SubscriptionController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            factory = new LogWriterFactory();
        }

        [Route("api/Subscription/LineItem/Add")]
        public bool AddNewLineItem(LineItemDTO LineItem)
        {
            if (LineItem == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "Line Item can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbLineItemEntity = AutoMapper.Mapper.Map<LineItemDTO, LineItem>(LineItem);
                    this.unitOfWork.LineItemRepository.Insert(dbLineItemEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }

                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [Route("api/Subscription/Product/Add")]
        public bool AddNewProduct(ProductDTO Product)
        {
            if (Product == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "Product can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbProductEntity = AutoMapper.Mapper.Map<ProductDTO, Product>(Product);
                    this.unitOfWork.ProductRepository.Insert(dbProductEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }

                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }
        [Route("api/Subscription/ProductLineItem/Add")]
        public bool AddProductDetail(ProductDetailDTO ProductDetail)
        {
            if (ProductDetail == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "Product details can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbProductDetailEntity = AutoMapper.Mapper.Map<ProductDetailDTO, ProductDetail>(ProductDetail);
                    this.unitOfWork.ProductDetailRepository.Insert(dbProductDetailEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }

                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }
        [Route("api/Subscription/BundledProduct/Add")]
        public bool AddBundledProduct(BundledProductDTO BundledProduct)
        {
            if (BundledProduct == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "Bundled Product can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbBundledProductEntity = AutoMapper.Mapper.Map<BundledProductDTO, BundledProduct>(BundledProduct);
                    this.unitOfWork.BundledProductRepository.Insert(dbBundledProductEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }

                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }
        [Route("api/Subscription/Add")]
        public bool AddSubscription(SubscriptionDTO Subscription)
        {
            if (Subscription == null)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(""),
                    ReasonPhrase = "Subscription can't be null"
                };
                throw new HttpResponseException(resp);
            }
            else
            {
                try
                {
                    var dbSubscriptionEntity = AutoMapper.Mapper.Map<SubscriptionDTO, Subscription>(Subscription);
                    this.unitOfWork.SubsciptionRepository.Insert(dbSubscriptionEntity);
                    this.unitOfWork.Save();
                    return true;
                }
                catch (DataException dbEx)
                {
                    // Log Error 
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(dbEx.Message);
                    }

                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(dbEx.Message),
                        ReasonPhrase = dbEx.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
                catch (Exception ex)
                {
                    // Log the error
                    using (var logwriter = factory.Create())
                    {
                        logwriter.Write(ex.Message);
                    }
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(ex.Message),
                        ReasonPhrase = ex.InnerException.ToString()
                    };
                    throw new HttpResponseException(resp);
                }
            }
        }

        [Route("api/Subscription/LineItem/GetAllLineItems")]
        public IEnumerable<LineItemDTO> GetAllLineItems()
        {
            try
            {
                var lineItemList = this.unitOfWork.LineItemRepository.Get().ToList();
                var mLineItemList = AutoMapper.Mapper.Map<IList<LineItem>, IList<LineItemDTO>>(lineItemList);
                return mLineItemList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

        [Route("api/Subscription/GetAllProduct")]
        public IEnumerable<ProductDTO> GetAllProducts()
        {
            try
            {
                var productList = this.unitOfWork.ProductRepository.Get().ToList();
                var mProductList = AutoMapper.Mapper.Map<IList<Product>, IList<ProductDTO>>(productList);
                return mProductList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

        public IEnumerable<ProductDetailDTO> GetAllProductDetails(long ProductId)
        {
            try
            {
                var productDetailList = this.unitOfWork.ProductDetailRepository.Get(e=> e.ProductId == ProductId).ToList();
                var mProductDetailList = AutoMapper.Mapper.Map<IList<ProductDetail>, IList<ProductDetailDTO>>(productDetailList);
                return mProductDetailList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

        public IEnumerable<BundledProductDTO> GetAllBundledProduct()
        {
            try
            {
                var bundledProductList = this.unitOfWork.BundledProductRepository.Get().ToList();
                var mBundledProductList = AutoMapper.Mapper.Map<IList<BundledProduct>, IList<BundledProductDTO>>(bundledProductList);
                return mBundledProductList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

        public IEnumerable<SubscriptionDTO> GetAllSubscriptions()
        {
            try
            {
                var subscriptionList = this.unitOfWork.SubsciptionRepository.Get().ToList();
                var mSubscriptionList = AutoMapper.Mapper.Map<IList<Subscription>, IList<SubscriptionDTO>>(subscriptionList);
                return mSubscriptionList;
            }
            catch (DataException dbx)
            {
                // Log the Error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(dbx.Message);
                }
                throw dbx;
            }
            catch (Exception ex)
            {
                // Log the error
                using (var logwriter = factory.Create())
                {
                    logwriter.Write(ex.Message);
                }
                throw ex;
            }
        }

   }
}
