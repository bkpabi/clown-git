﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Clown.Persistance.DataModel;
using Clown.Business.Entities;
using AutoMapper;
namespace Clown.Business.WebApi
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<ServiceProvider, BusinessDTO>()
                .ForMember(dest => dest.ServiceOffered, opts => opts.MapFrom(src => src.ServiceProvideds))
                .ForMember(dest => dest.Reviews, opts => opts.Ignore())
                .ForMember(dest => dest.Catlogs, opts => opts.MapFrom(src => src.CatlogProducts))
                .ForMember(dest => dest.ServiceProviderType, opts => opts.MapFrom(src => src.ServiceProviderType.Name)); 
            Mapper.CreateMap<ServiceProvided, ServiceOfferdDTO>();
            Mapper.CreateMap<WorkingHr, WorkingHrsDTO>();
            Mapper.CreateMap<Image, ImageDTO>();
            Mapper.CreateMap<ServiceProvided, ServiceOfferdDTO>();
            Mapper.CreateMap<Review, ReviewDTO>();
            Mapper.CreateMap<Project, ProjectDTO>()
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.ServiceProviderId))
                .ForMember(dest => dest.BusinessName, opts => opts.MapFrom(src => src.ServiceProvider.Name))
                .ForMember(dest=> dest.AreaName, opts=> opts.MapFrom(src => src.AreaMaster.AreaName));
            Mapper.CreateMap<ProjectDTO, Project>()
                .ForMember(dest => dest.ServiceProviderId, opts => opts.MapFrom(src => src.BusinessId))
                .ForMember(dest => dest.AreaMaster, opts => opts.Ignore())
                .ForMember(dest => dest.ServiceProvider, opts => opts.Ignore());
            Mapper.CreateMap<ProjectImageDTO, ProjectImage>()
                .ForMember(dest => dest.Project, opts => opts.Ignore());
            Mapper.CreateMap<ProjectImage, ProjectImageDTO>();
            Mapper.CreateMap<ServiceProviderType, BusinessTypeDTO>();
            Mapper.CreateMap<CatlogProduct, ProductCatalogDTO>();
            Mapper.CreateMap<WorkingHrsDTO, WorkingHr>();
            Mapper.CreateMap<ImageDTO, Image>()
                .ForMember(dest => dest.ServiceProvider, opts=> opts.Ignore());
            Mapper.CreateMap<BusinessDTO, ServiceProvider>()
                .ForMember(dest => dest.AreaMaster, opts => opts.Ignore())
                .ForMember(dest => dest.CatlogProducts, opts => opts.Ignore())
                .ForMember(dest => dest.CityMaster, opts => opts.Ignore())
                .ForMember(dest => dest.Keywords, opts => opts.Ignore())
                .ForMember(dest => dest.Projects, opts => opts.Ignore())
                .ForMember(dest => dest.Reviews, opts => opts.Ignore())
                .ForMember(dest => dest.ServiceProvideds, opts => opts.Ignore())
                .ForMember(dest => dest.ServiceProviderType, opts => opts.Ignore())
                .ForMember(dest => dest.WishLists, opts => opts.Ignore());
            Mapper.CreateMap<ServiceOfferdDTO, ServiceProvided>();
            Mapper.CreateMap<ProductCatalogDTO, CatlogProduct>()
                .ForMember(dest => dest.ServiceProvider, opts => opts.Ignore())
                .ForMember(dest => dest.ProductMaster, opts => opts.Ignore());
            Mapper.CreateMap<BusinessTypeDTO, ServiceProviderType>()
                .ForMember(dest => dest.ServiceProviders, opts => opts.Ignore())
                .ForMember(dest => dest.ServiceProviderType1, opts => opts.Ignore())
                .ForMember(dest => dest.ServiceProviderType2, opts => opts.Ignore());
            Mapper.CreateMap<CityMaster, CityDTO>();
            Mapper.CreateMap<AreaMaster, AreaDTO>().ForMember(dest => dest.CityName, opts => opts.MapFrom(src => src.CityMaster.CityName));
            Mapper.CreateMap<ServiceType, ServiceDTO>();
            Mapper.CreateMap<ProductMaster, ProductMasterDTO>();
            Mapper.CreateMap<Offer, OfferDTO>().ForMember(dest=> dest.ServiceProviderName, opts => opts.MapFrom(src=> src.ServiceProvider.Name));
            Mapper.CreateMap<OfferDTO, Offer>();
            Mapper.CreateMap<ProductDTO, Product>();
            Mapper.CreateMap<LineItemDTO, LineItem>();
            Mapper.CreateMap<ProductDetailDTO, ProductDetail>();
            Mapper.CreateMap<BundledProductDTO, BundledProduct>();
            Mapper.CreateMap<SubscriptionDTO, Subscription>();

            Mapper.CreateMap<Product,ProductDTO>();
            Mapper.CreateMap<LineItem, LineItemDTO>();
            Mapper.CreateMap<ProductDetail, ProductDetailDTO>();
            Mapper.CreateMap<BundledProduct, BundledProductDTO>();
            Mapper.CreateMap<Subscription, SubscriptionDTO>();
        }
    }
}