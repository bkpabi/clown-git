﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Clown.Business.Entities;
using Clown.UI.Admin.Models;
namespace Clown.UI.Admin
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<BusinessDTO, BusinessSummaryViewModel>()
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.BusinessName, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.BusinessType, opts => opts.MapFrom(src => src.ServiceProviderType))
                .ForMember(dest => dest.Address, opts => opts.MapFrom(src => string.Format("{0},{1},{2},{3}", src.Address_Street, src.Area, src.City, src.Address_State)));
            Mapper.CreateMap<AddBusinessViewModel, BusinessDTO>()
                .ForMember(dest => dest.ServiceProviderTypeId, opts => opts.MapFrom(src => src.BusinessType))
                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => src.Coordinates != null ? src.Coordinates.Split(',')[0].Trim() : null))
                .ForMember(dest => dest.Lattitude, opts => opts.MapFrom(src => src.Coordinates != null ? src.Coordinates.Split(',')[1].Trim() : null))
                .ForMember(dest => dest.Address_Area, opts => opts.MapFrom(src => src.Area))
                .ForMember(dest => dest.Address_City, opts => opts.MapFrom(src => src.City))
                .ForMember(dest => dest.Address_State, opts => opts.MapFrom(src => src.State))
                .ForMember(dest => dest.Address_Street, opts => opts.MapFrom(src => src.Street))
                .ForMember(dest => dest.Address_ZipCode, opts => opts.MapFrom(src => src.Zip))
                .ForMember(dest => dest.Catlogs, opts => opts.Ignore())
                .ForMember(dest => dest.ServiceOffered, opts => opts.Ignore())
                .ForMember(dest => dest.Reviews, opts => opts.Ignore())
                .ForMember(dest => dest.Projects, opts => opts.Ignore())
                .ForMember(dest => dest.Images, opts => opts.Ignore())
                .ForMember(dest => dest.WorkingHrs, opts => opts.Ignore());
            Mapper.CreateMap<BusinessDTO, ModifyBusinessViewModel>()
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.BusinessType, opts => opts.MapFrom(src => src.ServiceProviderTypeId))
                .ForMember(dest => dest.BusinessTypeName, opts => opts.MapFrom(src => src.ServiceProviderType))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.MonStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Monday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Monday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.MonEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Monday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Monday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.TueStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Tuesday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Tuesday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.TueEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Tuesday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Tuesday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.WedStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Wednesday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Wednesday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.WedEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Wednesday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Wednesday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.ThrusStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Thrusday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Thrusday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.ThrusEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Thrusday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Thrusday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.FriStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Friday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Friday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.FriEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Friday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Friday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.SatStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Saturday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Saturday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.SatEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Saturday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Saturday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.SunStartTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Sunday").StartTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Sunday").StartTime.Value) : DateTime.Today))
                .ForMember(dest => dest.SunEndTime, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Sunday").EndTime.HasValue ? DateTime.Today.Add(src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Sunday").EndTime.Value) : DateTime.Today))
                .ForMember(dest => dest.SunIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Sunday").IsOpen))
                .ForMember(dest => dest.MonIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Monday").IsOpen))
                .ForMember(dest => dest.TueIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Tuesday").IsOpen))
                .ForMember(dest => dest.WedIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Wednesday").IsOpen))
                .ForMember(dest => dest.ThrusIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Thrusday").IsOpen))
                .ForMember(dest => dest.FriIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Friday").IsOpen))
                .ForMember(dest => dest.SatIsOpen, opts => opts.MapFrom(src => src.WorkingHrs.SingleOrDefault(e => e.WeekDay == "Saturday").IsOpen))
                .ForMember(dest => dest.Area, opts => opts.MapFrom(src => src.Address_Area))
                .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.Address_City))
                .ForMember(dest => dest.Street, opts => opts.MapFrom(src => src.Address_Street))
                .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.Address_State))
                .ForMember(dest => dest.Coordinates, opts => opts.MapFrom(src => src.Lattitude != null ? string.Format("{0}" + ", " + "{1}", src.Longitude, src.Lattitude) : string.Empty))
                .ForMember(dest => dest.Zip, opts => opts.MapFrom(src => src.Address_ZipCode))
                .ForMember(dest => dest.OfferSummary, opts => opts.MapFrom(src => src.Offers))
                .ForMember(dest => dest.ImageSummary, opts => opts.MapFrom(src => src.Images))
                .ForMember(dest => dest.ProjectSummary, opts => opts.MapFrom(src => src.Projects));
                
            Mapper.CreateMap<ProjectDTO, ProjectSummary>()
                .ForMember(dest => dest.ProjectName, opts => opts.MapFrom(src => src.ProjectTittle))
                .ForMember(dest => dest.ProjectId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.ProjectCost, opts => opts.MapFrom(src => src.Cost))
                .ForMember(dest => dest.ProjectName, opts => opts.MapFrom(src => src.ProjectTittle))
                .ForMember(dest => dest.Location, opts => opts.MapFrom(src => src.AreaName));
            Mapper.CreateMap<OfferDTO, OfferSummaryViewModel>()
                .ForMember(dest => dest.Offerid, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.ServiceProviderId));
            Mapper.CreateMap<ImageDTO, ImageViewModel>()
                .ForMember(dest => dest.ImageLink, opts => opts.MapFrom(src => src.ImageUrl))
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.ServiceProviderId))
                .ForMember(dest => dest.BusinessName, opts => opts.Ignore());

            Mapper.CreateMap<ModifyBusinessViewModel, BusinessDTO>()
                .ForMember(dest => dest.Longitude, opts => opts.MapFrom(src => src.Coordinates != null ? src.Coordinates.Split(',')[0].Trim() : string.Empty))
                .ForMember(dest => dest.Lattitude, opts => opts.MapFrom(src => src.Coordinates != null ? src.Coordinates.Split(',')[1].Trim() : string.Empty))
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.BusinessId))
                .ForMember(dest => dest.Address_Area, opts => opts.MapFrom(src => src.Area))
                .ForMember(dest => dest.Address_City, opts => opts.MapFrom(src => src.City))
                .ForMember(dest => dest.Address_State, opts => opts.MapFrom(src => src.State))
                .ForMember(dest => dest.Address_Street, opts => opts.MapFrom(src => src.Street))
                .ForMember(dest => dest.Address_ZipCode, opts => opts.MapFrom(src => src.Zip))
                .ForMember(dest => dest.ServiceProviderTypeId, opts => opts.MapFrom(src => src.BusinessType));

            Mapper.CreateMap<AddOfferViewModel, OfferDTO>()
                .ForMember(dest => dest.ServiceProviderId, opts => opts.MapFrom(src => src.BusinessId));

            Mapper.CreateMap<UpdateOfferViewModel, OfferDTO>()
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.OfferId));

            Mapper.CreateMap<OfferDTO, UpdateOfferViewModel>()
                .ForMember(dest => dest.OfferId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.ServiceProviderId))
                .ForMember(dest => dest.BusinessName, opts => opts.MapFrom(src => src.ServiceProviderName));
            Mapper.CreateMap<OfferDTO, OfferSummaryViewModel>()
                .ForMember(dest => dest.Offerid, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.ServiceProviderId))
                .ForMember(dest => dest.BusinessName, opts => opts.MapFrom(src => src.ServiceProviderName));
            Mapper.CreateMap<AddProjectViewModel, ProjectDTO>()
                .ForMember(dest => dest.ProjectTittle, opts => opts.MapFrom(src => src.ProjectName))
                .ForMember(dest => dest.Cost, opts => opts.MapFrom(src => src.ProjectCost))
                .ForMember(dest => dest.BusinessId, opts => opts.MapFrom(src => src.ServiceProviderId))
                .ForMember(dest => dest.BusinessName, opts => opts.MapFrom(src => src.ServiceproviderName));
            Mapper.CreateMap<ProjectDTO, ModifyProjectViewModel>()
                .ForMember(dest => dest.ProjectId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.ProjectName, opts => opts.MapFrom(src => src.ProjectTittle))
                .ForMember(dest => dest.ProjectCost, opts => opts.MapFrom(src => src.Cost.Value))
                .ForMember(dest => dest.NoOfDaysTaken, opts => opts.MapFrom(src => src.NoOfDaysTaken.Value))
                .ForMember(dest => dest.ServiceProviderId, opts => opts.MapFrom(src => src.BusinessId))
                .ForMember(dest => dest.ServiceproviderName, opts => opts.MapFrom(src => src.BusinessName));
            Mapper.CreateMap<ProjectImageDTO, ProjectImageViewModel>()
                .ForMember(dest => dest.ProjectId, opts => opts.MapFrom(src => src.ProjectId))
                .ForMember(dest => dest.ProjectImageId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.Link, opts => opts.MapFrom(src => src.ImageLink));
            Mapper.CreateMap<ModifyProjectViewModel, ProjectDTO>()
                .ForMember(dest => dest.Cost, opts => opts.MapFrom(src => src.ProjectCost))
                .ForMember(dest => dest.Id, opts => opts.MapFrom(src => src.ProjectId))
                .ForMember(dest => dest.ProjectTittle, opts => opts.MapFrom(src => src.ProjectName));
            Mapper.CreateMap<AddProductViewModel, ProductDTO>();
            Mapper.CreateMap<AddLineItemViewModel, LineItemDTO>();
            Mapper.CreateMap<AddProductDetailViewModel, ProductDetailDTO>();
            Mapper.CreateMap<AddBundledProductViewModel, BundledProductDTO>();
        }
    }
}