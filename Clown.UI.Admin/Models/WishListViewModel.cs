﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clown.UI.Admin.Models
{
    public class WishListViewModel
    {
        public long WishId { get; set; }
        public long BusinessId { get; set; }

        public string BusinessName { get; set; }

        public string BusinessType { get; set; }

        public decimal Ratting { get; set; }

        public int ReviewCount { get; set; }

        public bool IsVerified { get; set; }

        public string ServiceOffered { get; set; }

        public string Address { get; set; }

        public int ProjectCount { get; set; }

        public int InBusinessSince { get; set; }

        public int OfferCount { get; set; }
    }
}