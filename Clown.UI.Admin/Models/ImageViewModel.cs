﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clown.UI.Admin.Models
{
    public class ImageViewModel
    {
        public int Id { get; set; }
        public string ImageLink { get; set; }

        public string BusinessName { get; set; }

        public int BusinessId { get; set; }
    }
}