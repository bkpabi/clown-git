﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using RestSharp;
using Clown.Business.Entities;
using AutoMapper;
namespace Clown.UI.Admin.Models
{

    public class ProductViewModel
    {
        public long Id { get; set; }
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
    }

    public class AddProductViewModel
    {
        [Required]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
    }
    public class UpdatePlanViewModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        [Display(Name = "Plan Name")]
        public string ProductName { get; set; }
    }

    public class DeletePlanViewModel
    {
        public long Id { get; set; }
    }

    public class LineItemViewModel
    {
        public long Id { get; set; }
        public string LineItemName { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class AddLineItemViewModel
    {
        [Required]
        [Display(Name="Line Item")]
        public string LineItemName { get; set; }
    }
    public class UpdateLineItemViewModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        [Display(Name="Line Item")]
        public string LineItemName { get; set; }
    }

    public class DeleteLineItemViewModel
    {
        [Required]
        public long Id { get; set; }
    }

    public class ProductDetailViewModel
    {
        public long Id { get; set; }
        public long PlanId { get; set; }
        public string PlanName { get; set; }
        public long LineItemId { get; set; }
        public string LineItemName { get; set; }
        public bool Availability { get; set; }
        public string Limit { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class AddProductDetailViewModel
    {
        public AddProductDetailViewModel()
        {
            //Initialise Rest Client
            var _client = new RestClient(new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]));

            //Populate all available Line items
            AvailableLineItems = new List<SelectListItem>();
            var lineItemRequest = new RestSharp.RestRequest("api/Subscription/LineItem/GetAllLineItems", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var lineItemresponse = _client.Execute<List<LineItemDTO>>(lineItemRequest);
            if (lineItemresponse != null)
            {
                foreach (var item in lineItemresponse.Data)
                {
                    AvailableLineItems.Add(new SelectListItem() { Text = item.LineItemName, Value = item.Id.ToString() });
                }
            }
        }
        [Required]
        public long ProductId { get; set; }
        
        [Required]
        public long LineItemId { get; set; }
        public List<SelectListItem> AvailableLineItems { get; set; }
        [Required]
        public bool Availability { get; set; }
        [Required]
        public string Limit { get; set; }
    }

    public class UpdateProductDetailViewModel
    {
        public UpdateProductDetailViewModel()
        {
            //Initialise Rest Client
            var _client = new RestClient(new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]));

            //Populate all available plans
            AvailableProduct = new List<SelectListItem>();
            var request = new RestSharp.RestRequest("api/Subscription/GetAllProduct", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<List<ProductDTO>>(request);
            if (response != null)
            {
                foreach (var item in response.Data)
                {
                    AvailableProduct.Add(new SelectListItem() { Text = item.ProductName, Value = item.Id.ToString() });
                }
            }

            //Populate all available Line items
            AvailableLineItems = new List<SelectListItem>();
            var lineItemRequest = new RestSharp.RestRequest("api/Subscription/GetAllLineItems", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var lineItemresponse = _client.Execute<List<LineItemDTO>>(lineItemRequest);
            if (lineItemresponse != null)
            {
                foreach (var item in lineItemresponse.Data)
                {
                    AvailableLineItems.Add(new SelectListItem() { Text = item.LineItemName, Value = item.Id.ToString() });
                }
            }
        }
        [Required]
        public long Id { get; set; }
        [Required]
        public long PlanId { get; set; }
        public List<SelectListItem> AvailableProduct { get; set; }
        [Required]
        public long LineItemId { get; set; }
        public List<SelectListItem> AvailableLineItems { get; set; }
        [Required]
        public bool Availability { get; set; }
        [Required]
        public string Limit { get; set; }
    }

    public class DeleteProductDetailViewModel
    {
        [Required]
        public long Id { get; set; }
    }

    public class BundledProductViewModel
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public int Duration { get; set; }
        public decimal Price { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }

    public class AddBundledProductViewModel
    {
        public AddBundledProductViewModel()
        {
            //Initialise Rest Client
            var _client = new RestClient(new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]));

            //Populate all available plans
            AvailableProducts = new List<SelectListItem>();
            var request = new RestSharp.RestRequest("api/Subscription/GetAllProduct", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<List<ProductDTO>>(request);
            if (response != null)
            {
                foreach (var item in response.Data)
                {
                    AvailableProducts.Add(new SelectListItem() { Text = item.ProductName, Value = item.Id.ToString() });
                }
            }
        }
        [Required]
        [Display(Name="Product")]
        public long ProductId { get; set; }
        public List<SelectListItem> AvailableProducts { get; set; }
        [Required]
        public int Duration { get; set; }
        [Required]
        public decimal Price { get; set; }
    }

    public class UpdateBundledProductViewModel
    {
        public UpdateBundledProductViewModel()
        {
            //Initialise Rest Client
            var _client = new RestClient(new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]));

            //Populate all available plans
            AvailableProducts = new List<SelectListItem>();
            var request = new RestSharp.RestRequest("api/Subscription/GetAllProduct", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<List<ProductDTO>>(request);
            if (response != null)
            {
                foreach (var item in response.Data)
                {
                    AvailableProducts.Add(new SelectListItem() { Text = item.ProductName, Value = item.Id.ToString() });
                }
            }
        }
        public long Id { get; set; }
        public long ProductId { get; set; }
        public List<SelectListItem> AvailableProducts { get; set; }
        public int Duration { get; set; }

        public decimal Price { get; set; }
    }

    public class DeleteBundledProductViewModel
    {
        [Required]
        public long Id { get; set; }
    }

    public class SubscriptionViewModel
    {
        
        public long BusinessId { get; set; }

        public List<SelectListItem> AvailableBusiness { get; set; }
        public long BundledProductId { get; set; }
    }

    public class AddSubscriptionViewModel 
    {
        public AddSubscriptionViewModel()
        {
            /* -- 
            //Initialise Rest Client
            var _client = new RestClient(new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]));

            //Populate all available plans
            AvailableBusiness = new List<SelectListItem>();
            var request = new RestSharp.RestRequest("api/Business/GetAllBusiness", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<List<BusinessDTO>>(request);
            if (response != null)
            {
                foreach (var item in response.Data)
                {
                    AvailableBusiness.Add(new SelectListItem() { Text = item.Name, Value = item.Id.ToString() });
                }
            }
             * */
        }

        public long BusinessId { get; set; }

        //public List<SelectListItem> AvailableBusiness { get; set; }
        public long BundledProductId { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
    }

    public class UpdateSubscriptionViewModel
    {
        public UpdateSubscriptionViewModel()
        {
            /*
            //Initialise Rest Client
            var _client = new RestClient(new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]));

            //Populate all available plans
            AvailableBusiness = new List<SelectListItem>();
            var request = new RestSharp.RestRequest("api/Business/GetAllBusiness", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<List<BusinessDTO>>(request);
            if (response != null)
            {
                foreach (var item in response.Data)
                {
                    AvailableBusiness.Add(new SelectListItem() { Text = item.Name, Value = item.Id.ToString() });
                }
            }
             * */
        }

        public long Id { get; set; }
        public long BusinessId { get; set; }

        //public List<SelectListItem> AvailableBusiness { get; set; }
        public long BundledProductId { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
    }
    public class DeleteSubscriptionViewModel
    {
        [Required]
        public long Id { get; set; }
    }

}