﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Clown.UI.Admin.Startup))]
namespace Clown.UI.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
