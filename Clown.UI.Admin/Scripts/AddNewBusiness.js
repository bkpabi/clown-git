﻿//var refURl = "http://localhost:6688";
//var refURl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    $("#city").change(function () {
        if ($('option:selected', $(this)).val() != "") {

            $.ajax({
                //url: refURl + '/api/ServiceProvider/GetAllAreaByCity/' + $('option:selected', $(this)).text(),
                url: refURl + '/api/Area/ByCity/' + $('option:selected', $(this)).val(),
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                //crossDomain: true,
                success: function (data) {
                    for (var key in data) {
                        $('#area').append($("<option></option>").val(data[key].Id).text(data[key].AreaName));
                    }
                },
                error: function (x, y, z) {
                    alert(x + y + z);
                }

            });
        }
        else {
            $("#area").empty();
        }
    });

    jQuery('#sunstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12:false
    });
    jQuery('#sunendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });
    
    jQuery('#monstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12: false
    });
    jQuery('#monendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });

    jQuery('#tuestarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12: false
    });
    jQuery('#tueendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });

    jQuery('#wedstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12: false
    });
    jQuery('#wedendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });

    jQuery('#thrustarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12: false
    });
    jQuery('#thruendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });

    jQuery('#fristarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12: false
    });
    jQuery('#friendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });

    jQuery('#satstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '09:30 AM',
        hours12: false
    });
    jQuery('#satendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        defaultTime: '11:00 PM',
        hours12: false
    });

    //Checkbox 
    //$("#SunIsOpen").on('change', function () {
    //    alert("Hi");
    //});

    $("#tbltimings").on("change", ":checkbox", function () {
        var isChecked = this.checked;
        if (isChecked) {
            $(this).parents().parents().children("td:eq(2)").find("input").prop("disabled", false);
            $(this).parents().parents().children("td:eq(2)").find("input").next("span").css("display", "");
            $(this).parents().parents().children("td:eq(3)").find("input").prop("disabled", false);
            $(this).parents().parents().children("td:eq(3)").find("input").next("span").css("display", "");
        }
        else
        {
            $(this).parents().parents().children("td:eq(2)").find("input").val("");
            $(this).parents().parents().children("td:eq(3)").find("input").val("");
            $(this).parents().parents().children("td:eq(2)").find("input").prop("disabled", true);
            $(this).parents().parents().children("td:eq(2)").find("input").next("span").css("display", "none");
            $(this).parents().parents().children("td:eq(3)").find("input").prop("disabled", true);
            $(this).parents().parents().children("td:eq(3)").find("input").next("span").css("display", "none");
        }
        //$(this).parents("tr:first").css('background-color', 'yellow');
        //alert($(this).parents().parents().children("td:eq(2)").find("input").val());
    });

    $("#file").change(function (e) {
        $("#imagePlaceHolder").empty();
        readURL(this);
        var fileList = e.target.files;
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        for (var i = 0; i < input.files.length; i++) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[i]);
            reader.onload = function (e) {
                $("#imagePlaceHolder").append('<div class="col-sm-2" style="margin-bottom:10px;"><img src="' + e.target.result + '" height="150" width="150"></div>')
                //$('#blah').attr('src', e.target.result);
            }
        }

    }
}