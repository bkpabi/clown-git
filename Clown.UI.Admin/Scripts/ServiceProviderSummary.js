﻿$(document).ready(function () {
    $('#tblServiceProvider').DataTable({
        //"ordering": false,
        "bLengthChange": false,
        'iDisplayLength': 15,
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [4,5] }
        ]
    });

    $("#tblServiceProvider tbody tr td ").on("click", "button.businessDelete", function () {
        $("#DltBusinessId").val($(this).parent().parent().parent().children("td").eq(0).html());
    });

    $("#btnDltBusiness").click(function () {
        $.ajax({
            url: "/ServiceProvider/Delete",
            type: 'POST',
            dataType: 'json',
            data: { id: $("#DltBusinessId").val() },
            success: function (data) {
                if (data == "Business details deleted successfully") {
                    $("#opResultMsg").removeClass("alert-danger ");
                    $("#opResultMsg").addClass("alert-success");
                }
                else {
                    $("#opResultMsg").removeClass("alert-success");
                    $("#opResultMsg").addClass("alert-danger");
                }
                $("#opResultMsg").html('<button class="close" data-dismiss="alert">×</button>'+data);
                $("#opResultMsg").show();
                $("#tblServiceProvider tbody tr#" + $("#DltBusinessId").val()).remove();
                $('#deleteBusiness').modal('hide');
                //$("#" + $("#DltProjImageId").val()).parent().parent().remove();
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });
});