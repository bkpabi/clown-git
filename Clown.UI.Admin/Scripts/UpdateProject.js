﻿$(document).ready(function () {

    $("#ProjectImages tbody tr td ").on("click", "button.prjImageDelete", function () {
        $("#DltProjImageId").val($(this).parent().parent().parent().children("td").eq(0).html());
    });

    $("#DeleteProjectImage").click(function () {
        $.ajax({
            url: "/Project/DeleteProjectImage",
            type: 'POST',
            dataType: 'json',
            data: { id: $("#DltProjImageId").val() },
            success: function (data) {
                $("#ProjectImages tbody tr#" + $("#DltProjImageId").val()).remove();
                $('#delete').modal('hide');
                //$("#" + $("#DltProjImageId").val()).parent().parent().remove();
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

    $("#file").change(function (e) {
        readURL(this);
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        for (var i = 0; i < input.files.length; i++) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[i]);
            reader.onload = function (e) {
                $("#imagePlaceHolder").append('<div class="col-sm-2" style="margin-bottom:10px;"><img src="' + e.target.result + '" height="150" width="150"></div>')
                //$('#blah').attr('src', e.target.result);
            }
        }

    }
}
