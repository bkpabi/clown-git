﻿//var refURl = "http://localhost:6688";
//var refURl = "http://clowndemo.azurewebsites.net";
$(document).ready(function () {
    //if ($("#businesstype").val() == 1 || $("#businesstype").val() == 2) {
    //    $("#product").hide();
    //}
    //else {
    //    $("#service").hide();
    //}

    //$("#businesstype").change(function () {
    //    if ($('option:selected', $(this)).val() == 1 || $('option:selected', $(this)).val() == 2) {
    //        $("#product").hide();
    //        $("#service").show();
    //    }
    //    else {
    //        $("#product").show();
    //        $("#service").hide();
    //    }
    //});
    $("#city").change(function () {
        if ($('option:selected', $(this)).text() != "-- Select City --") {

            $.ajax({
                //url: refURl + '/api/ServiceProvider/GetAllAreaByCity/' + $('option:selected', $(this)).text(),
                url: refURl + '/api/Area/ByCity/' + $('option:selected', $(this)).val(),
                type: 'GET',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                //crossDomain: true,
                success: function (data) {
                    for (var key in data) {
                        $('#area').append($("<option></option>").val(data[key].Id).text(data[key].AreaName));
                    }
                },
                error: function (x, y, z) {
                    alert(x + y + z);
                }

            });
        }
        else {
            $("#area").empty();
        }
    });
    $('#tblProjects').DataTable({
        //"ordering": false,
        "bLengthChange": false,
        'iDisplayLength': 5,
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [4, 5] }
        ]
    });

    $('#tblOffers').DataTable({
        //"ordering": false,
        "bLengthChange": false,
        'iDisplayLength': 5,
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [5, 6] }
        ]
    });
    //Once the user clicks the delete offer button fetch the id of the offer and strore it in hidden field
    $("#tblOffers tbody tr td ").on("click", "button.offerDelete", function () {
        $("#DltOfferId").val($(this).parent().parent().parent().children("td").eq(0).html());
    });
    // Once the user press the yes button for offer delete send the request
    $("#btnDltOffer").click(function () {
        $.ajax({
            url: "/Offer/Delete",
            type: 'POST',
            dataType: 'json',
            data: { id: $("#DltOfferId").val() },
            success: function (data) {
                $("#tblOffers tbody tr#" + $("#DltOfferId").val()).remove();
                $('#deleteOffer').modal('hide');
                //$("#" + $("#DltProjImageId").val()).parent().parent().remove();
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

    //Once the user clicks the delete offer button fetch the id of the offer and strore it in hidden field
    $("#tblProjects tbody tr td ").on("click", "button.projectDelete", function () {
        $("#DltProjectId").val($(this).parent().parent().parent().children("td").eq(0).html());
    });
    // Once the user press the yes button for offer delete send the request
    $("#btnDltProject").click(function () {
        $.ajax({
            url: "/Project/Delete",
            type: 'POST',
            dataType: 'json',
            data: { id: $("#DltProjectId").val() },
            success: function (data) {
                $("#tblProjects tbody tr#" + $("#DltProjectId").val()).remove();
                $('#deleteProject').modal('hide');
                //$("#" + $("#DltProjImageId").val()).parent().parent().remove();
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });


    $("#tblImages tbody tr td ").on("click", "button.imageDelete", function () {
        //alert($(this).parent().parent().parent().children("td").eq(0).html());
        $("#deleteImageMeassage").html("");
        $("#DltImageId").val($(this).parent().parent().parent().children("td").eq(0).html());
    });

    // Once the user press the yes button for offer delete send the request
    $("#DeleteImagebtn").click(function () {
        $.ajax({
            url: "/ServiceProvider/DeleteImage?id=" + $("#DltImageId").val(),
            type: 'POST',
            dataType: 'json',
            //data: { id: $("#DltImageId").val() },
            success: function (data) {
                if (data == "Image deleted successfully") {
                    $("#tblImages tbody tr#" + $("#DltImageId").val()).remove();
                    $("#deleteImageMeassage").html("");
                    $('#deleteImage').modal('hide');
                }
                else {
                    $("#deleteImageMeassage").html(data);
                }
               
                //$("#" + $("#DltProjImageId").val()).parent().parent().remove();
            },
            error: function (x, y, z) {
                alert(x + '\n' + y + '\n' + z);
            }
        });
    });

    jQuery('#sunstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30 am',
        hours12: false
    });
    jQuery('#sunendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '11:00 pm',
        hours12: false
    });

    jQuery('#monstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });
    jQuery('#monendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });

    jQuery('#tuestarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });
    jQuery('#tueendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });

    jQuery('#wedstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });
    jQuery('#wedendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });

    jQuery('#thrustarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });
    jQuery('#thruendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });

    jQuery('#fristarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });
    jQuery('#friendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });

    jQuery('#satstarttime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });
    jQuery('#satendtime').datetimepicker({
        datepicker: false,
        format: 'g:i A',
        formatTime: 'g:i A',
        step: 30,
        //defaultTime: '09:30',
        hours12: false
    });

    $("#tbltimings").on("change", ":checkbox", function () {
        var isChecked = this.checked;
        if (isChecked) {
            $(this).parents().parents().children("td:eq(2)").find("input").prop("disabled", false);
            $(this).parents().parents().children("td:eq(2)").find("input").next("span").css("display", "");
            $(this).parents().parents().children("td:eq(3)").find("input").prop("disabled", false);
            $(this).parents().parents().children("td:eq(3)").find("input").next("span").css("display", "");
        }
        else {
            $(this).parents().parents().children("td:eq(2)").find("input").val("");
            $(this).parents().parents().children("td:eq(3)").find("input").val("");
            $(this).parents().parents().children("td:eq(2)").find("input").prop("disabled", true);
            $(this).parents().parents().children("td:eq(2)").find("input").next("span").css("display", "none");
            $(this).parents().parents().children("td:eq(3)").find("input").prop("disabled", true);
            $(this).parents().parents().children("td:eq(3)").find("input").next("span").css("display", "none");
        }
        //$(this).parents("tr:first").css('background-color', 'yellow');
        //alert($(this).parents().parents().children("td:eq(2)").find("input").val());
    });

    $("#file").change(function (e) {
        readURL(this);
    });

});

function readURL(input) {
    if (input.files && input.files[0]) {
        for (var i = 0; i < input.files.length; i++) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[i]);
            reader.onload = function (e) {
                $("#imagePlaceHolder").append('<div class="col-sm-2" style="margin-bottom:10px;"><img src="' + e.target.result + '" height="150" width="150"></div>')
                //$('#blah').attr('src', e.target.result);
            }
        }

    }
}