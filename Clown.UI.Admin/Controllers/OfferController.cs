﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.Business.Entities;
using Clown.UI.Admin.Models;
using System.Data;
using System.Net.Http;
using Newtonsoft.Json;
using RestSharp;
namespace Clown.UI.Admin.Controllers
{
    [Authorize]
    public class OfferController : Controller
    {
        private RestClient _client;
        public OfferController()
        {
            this._client = new RestClient();
            _client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]);
        }

        // GET: Offer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add(long spid)
        {
            return View(GetNewBaseModel(spid));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddOfferViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                var newModel = GetNewBaseModel(model.BusinessId, model);
                if (ModelState.IsValid)
                {
                    
                    if (model.StartDate > model.EndDate)
                    {
                        ModelState.AddModelError(string.Empty, "Offer start date cannot be greater than offer end date");
                        ViewData["ErrorMessage"] = "Offer start date cannot be greater than offer end date";
                        //return View(GetNewBaseModel(model.BusinessId));
                    }
                    else
                    {
                        try
                        {
                            var newOffer = AutoMapper.Mapper.Map<AddOfferViewModel, OfferDTO>(model);
                            newOffer.IsActive = true;
                            var request = new RestSharp.RestRequest("api/Offer/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                            var json = JsonConvert.SerializeObject(newOffer);
                            request.AddParameter("text/json", json, ParameterType.RequestBody);
                            var response = _client.Execute(request);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                            {
                                ViewData["SuccessMessage"] = "New offer details are added successfully.";
                                newModel = GetNewBaseModel(model.BusinessId);
                                ModelState.Clear();
                            }
                            else
                            {
                                ViewData["ErrorMessage"] = "Error while saving the details";
                            }
                            //return View(GetNewBaseModel(model.BusinessId));

                        }
                        catch (DataException dbEx)
                        {
                            // Log the error here
                            ModelState.AddModelError(string.Empty, "Unable to save details. Please try again, if the problem persist contact customer care");
                        }
                        catch (Exception ex)
                        {
                            // Log Error here
                            ModelState.AddModelError(string.Empty, "Unable to delete offer. Please try after some time, If problem persist contact customer care");
                        }
                    }
                }
               
                
                return View(newModel);
            }

        }

        public ActionResult Update(long id)
        {
            return View(GetUpdateBaseModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(UpdateOfferViewModel model)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (model.StartDate > model.EndDate)
                    {
                        ModelState.AddModelError(string.Empty, "Offer start date cannot be greater than offer end date");
                        ViewData["ErrorMessage"] = "Offer start date cannot be greater than offer end date";
                    }
                    else
                    {
                        try
                        {
                            var offerDTO = AutoMapper.Mapper.Map<UpdateOfferViewModel, OfferDTO>(model);
                            var request = new RestSharp.RestRequest("api/Offer/Update", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                            var json = JsonConvert.SerializeObject(offerDTO);
                            request.AddParameter("text/json", json, ParameterType.RequestBody);
                            var response = _client.Execute(request);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                            {
                                ViewData["SuccessMessage"] = "Success! Offer details are updated successfully.";
                            }
                            else
                            {
                                ViewData["ErrorMessage"] = "Unable to update details. Please try after some time, If problem persist contact customer care.";
                            }
                        }
                        catch (DataException)
                        {
                            // Log Error here
                            ModelState.AddModelError(string.Empty, "Unable to update details. Please try after some time, If problem persist contact customer care.");
                            ViewData["ErrorMessage"] = "Unable to update details. Please try after some time, If problem persist contact customer care.";
                        }
                        catch (Exception ex)
                        {
                            // Log Error here
                            ModelState.AddModelError(string.Empty, "Unable to delete offer. Please try after some time, If problem persist contact customer care");
                            ViewData["ErrorMessage"] = "Unable to update details. Please try after some time, If problem persist contact customer care.";
                        }
                    }
                    
                }
                return View(GetUpdateBaseModel(model.OfferId));
            }
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            try
            {
                var request = new RestSharp.RestRequest("api/Offer/Delete/" + id.ToString(), RestSharp.Method.DELETE) { RequestFormat = RestSharp.DataFormat.Json };
                var response = _client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                {
                    return Json("Offer Deleted");
                }
                else
                {
                    return Json("Offer deletion failed");
                }
                
            }
            catch (DataException DBEx)
            {
                // Log Error here
                return Json("Unable to delete offer. Please try after some time, If problem persist contact customer care");
            }
            catch (Exception ex)
            {
                return Json("Unable to delete offer. Please try after some time, If problem persist contact customer care");
            }

        }

        private AddOfferViewModel GetNewBaseModel(long BusinessId,  AddOfferViewModel model = null)
        {
            if (model == null)
            {
                model = new AddOfferViewModel();
            }
            //AddOfferViewModel model = new AddOfferViewModel();
            var requestForBusinessDetails = new RestSharp.RestRequest("api/Business/GetById/" + BusinessId.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForBusinessDetails = _client.Execute<BusinessDTO>(requestForBusinessDetails);
            if (responseForBusinessDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model.BusinessId = BusinessId;
                model.BusinessName = responseForBusinessDetails.Data.Name;
            }

            return model;
        }

        private UpdateOfferViewModel GetUpdateBaseModel(long OfferId)
        {
            UpdateOfferViewModel model = new UpdateOfferViewModel();
            var requestForOfferDetails = new RestSharp.RestRequest("api/Offer/GetById/" + OfferId.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForOfferDetails = _client.Execute<OfferDTO>(requestForOfferDetails);
            if (responseForOfferDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                AutoMapper.Mapper.Map<OfferDTO, UpdateOfferViewModel>(responseForOfferDetails.Data, model);
            }

            return model;
        }
    }
}