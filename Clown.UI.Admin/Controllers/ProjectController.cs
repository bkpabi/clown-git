﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.Business.Entities;
using Clown.UI.Admin.Models;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using Newtonsoft.Json;
using RestSharp;
namespace Clown.UI.Admin.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private RestClient _client;

        public ProjectController()
        {
            this._client = new RestClient();
            _client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]);
        }
        // GET: Project
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Add(long spid)
        {
            return View(CreateBaseModel(spid, new AddProjectViewModel()));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(AddProjectViewModel model, HttpPostedFileBase[] file)
        {
            if (model == null || file == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                var newProject = AutoMapper.Mapper.Map<AddProjectViewModel, ProjectDTO>(model);
                newProject.CreatedBy = User.Identity.Name;
                newProject.CreatedOn = DateTime.Now;
                newProject.ProjectImages = AddNewProjectImage(file);

                var request = new RestSharp.RestRequest("api/Project/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                var json = JsonConvert.SerializeObject(newProject);
                request.AddParameter("text/json", json, ParameterType.RequestBody);
                var response = _client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                {
                    ViewData["SuccessMessage"] = "New project details are added successfully.";
                    model = CreateBaseModel(model.ServiceProviderId,new AddProjectViewModel());
                    ModelState.Clear();
                }
                else
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                    model = CreateBaseModel(model.ServiceProviderId, model);
                }
                return View(model);
            }
        }

        private List<ProjectImageDTO> AddNewProjectImage(HttpPostedFileBase[] file)
        {
            // New project is created now upload the images
            try
            {
                List<ProjectImageDTO> imageList = new List<ProjectImageDTO>();
                if (file[0] != null)
                {

                    foreach (var item in file)
                    {
                        if (item.ContentLength > 0)
                        {
                            var filePath = System.IO.Path.GetFileName(item.FileName);
                            // Create a cloud storage account and initialize it with the storage connection string
                            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                            // As we will be accessing the slob storage on the azure we need to create a blob client
                            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                            // Get the reference to the container
                            CloudBlobContainer container = blobClient.GetContainerReference("images");
                            // create the container if it do not exist and set the public access 
                            //container.CreateIfNotExists();
                            //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                            // Retrieve reference to a blob named "myblob".
                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(item.FileName);

                            // Create or overwrite the "myblob" blob with contents from a local file.
                            //using (var fileStream = System.IO.File.OpenRead(item.FileName))
                            //{
                            blockBlob.UploadFromStream(item.InputStream);
                            //}

                            imageList.Add(new ProjectImageDTO() { ImageLink = System.Configuration.ConfigurationManager.AppSettings["CDNBaseUrl"] + item.FileName });
                            //ProjectImage newProjectImage = new ProjectImage();
                            //newProjectImage.ProjectId = ProjectId;
                            //newProjectImage.ImageLink = System.Configuration.ConfigurationManager.AppSettings["CDNBaseUrl"] + item.FileName;
                            //_repositoryPrj.AddProjectImage(newProjectImage);
                        }
                    }


                }
                return imageList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult Update(long id)
        {
            return View(CreateUpdateBaseModel(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ModifyProjectViewModel model, HttpPostedFileBase[] file)
        {
            if (model != null)
            {
                if (ModelState.IsValid)
                {
                    var updatedProject = AutoMapper.Mapper.Map<ModifyProjectViewModel, ProjectDTO>(model);
                    updatedProject.ModifiedBy = User.Identity.Name;
                    updatedProject.ModifiedOn = DateTime.Now;
                    updatedProject.ProjectImages = AddNewProjectImage(file);


                    var request = new RestSharp.RestRequest("api/Project/Update", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(updatedProject);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New project details are updated successfully.";
                        model = CreateUpdateBaseModel(model.ProjectId);
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
            }
            return View(model);
        }

        public JsonResult Delete(long id)
        {
            var requestForProjectImages = new RestSharp.RestRequest("api/Project/Image/GetByProject/" + id.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForProjectImages = _client.Execute<List<ProjectImageDTO>>(requestForProjectImages);
            if (responseForProjectImages.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var request = new RestSharp.RestRequest("api/Project/Delete/"+ id.ToString(), RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                //var json = JsonConvert.SerializeObject(id.ToString());
                //request.AddParameter("text/json", json, ParameterType.RequestBody);
                var response = _client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                {
                    foreach (var item in responseForProjectImages.Data)
                    {
                        string image = item.ImageLink.Split('/').Last();
                        // Delete the image from CDN
                        // Retrieve storage account from connection string.
                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

                        // Create the blob client.
                        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                        // Retrieve reference to a previously created container.
                        CloudBlobContainer container = blobClient.GetContainerReference("images");

                        // Retrieve reference to a blob named "myblob.txt".
                        CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

                        // Delete the blob.
                        blockBlob.Delete();
                    }

                    return Json("Project Deleted Successfully");
                }
                else
                {
                    return Json("Project deletion failed.");
                }
            }
            else
            {
                return Json("Project deletion failed.");
            }
            /*
            //var prjImages = _repositoryPrj.GetProjectImagesByProject(id);
            foreach (var item in prjImages)
            {
                string image = item.ImageLink.Split('/').Last();
                _repositoryPrj.DeleteProjectImage(item.Id);
                // Delete the image from CDN
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

                // Create the blob client.
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference("images");

                // Retrieve reference to a blob named "myblob.txt".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

                // Delete the blob.
                blockBlob.Delete();
            }
            //_repositoryPrj.DeleteProject(id);
             * */
            
        }

        public ActionResult UploadImage(AddProjectImageViewModel model)
        {
            /*
            if (model == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                // Create a cloud storage account and initialize it with the storage connection string
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                // As we will be accessing the slob storage on the azure we need to create a blob client
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Get the reference to the container
                CloudBlobContainer container = blobClient.GetContainerReference(_repositoryPrj.GetProjectDetailsByProjectId(model.ProjectId).ProjectTittle);
                // create the container if it do not exist and set the public access 
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob = container.GetBlockBlobReference("myblob");

                // Create or overwrite the "myblob" blob with contents from a local file.
                using (var fileStream = System.IO.File.OpenRead(model.ImagePath))
                {
                    blockBlob.UploadFromStream(fileStream);
                }
            

                return View();
            }
             * */
            return View();
        }

        [HttpPost]
        //[Route("Project/DeleteProjectImage/{id}")]
        public JsonResult DeleteProjectImage(long id)
        {

            var requestForBusinessDetails = new RestSharp.RestRequest("api/Project/Image/GetById/" + id.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForBusinessDetails = _client.Execute<ProjectImageDTO>(requestForBusinessDetails);
            if (responseForBusinessDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var request = new RestSharp.RestRequest("api/Project/Image/Delete/" + id.ToString(), RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                //var json = JsonConvert.SerializeObject(updatedProject);
                //request.AddParameter("text/json", json, ParameterType.RequestBody);
                var response = _client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                {
                    string image = responseForBusinessDetails.Data.ImageLink.Split('/').Last();
                    // Delete the image from CDN
                    // Retrieve storage account from connection string.
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("images");

                    // Retrieve reference to a blob named "myblob.txt".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

                    // Delete the blob.
                    blockBlob.Delete();
                    return Json("Project Image deleted successfully");
                }
                else
                {
                    return Json("Project Image could not be deleted");
                }
            }
            else
            {
                return Json("Project Image could not be deleted");
            }
            /*
            string image = _repositoryPrj.GetProjectImageByid(id).ImageLink.Split('/').Last();
            _repositoryPrj.DeleteProjectImage(id);
            // Delete the image from CDN
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference("images");

            // Retrieve reference to a blob named "myblob.txt".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

            // Delete the blob.
            blockBlob.Delete();
             * */

        }

        private AddProjectViewModel CreateBaseModel(long BusinessId, AddProjectViewModel model)
        {
            //AddProjectViewModel model = new AddProjectViewModel();
            var requestForBusinessDetails = new RestSharp.RestRequest("api/Business/GetById/" + BusinessId.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForBusinessDetails = _client.Execute<BusinessDTO>(requestForBusinessDetails);
            if (responseForBusinessDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                model.ServiceProviderId = BusinessId;
                model.ServiceproviderName = responseForBusinessDetails.Data.Name;
            }

            var requestForArea = new RestSharp.RestRequest("api/Area/ByCity/" + responseForBusinessDetails.Data.Address_City, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForArea = _client.Execute<List<AreaDTO>>(requestForArea);
            if (responseForArea != null)
            {
                List<SelectListItem> areaList = new List<SelectListItem>();
                foreach (var item in responseForArea.Data)
                {
                    SelectListItem newItem = new SelectListItem();
                    newItem.Text = item.AreaName;
                    newItem.Value = item.Id.ToString();
                    areaList.Add(newItem);
                }
                model.AvailableAreas = areaList;
            }

            return model;

        }

        private ModifyProjectViewModel CreateUpdateBaseModel(long ProjectId)
        {
            ModifyProjectViewModel model = new ModifyProjectViewModel();
            var requestForOfferDetails = new RestSharp.RestRequest("api/Project/GetById/" + ProjectId.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForOfferDetails = _client.Execute<ProjectDTO>(requestForOfferDetails);
            if (responseForOfferDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                AutoMapper.Mapper.Map<ProjectDTO, ModifyProjectViewModel>(responseForOfferDetails.Data, model);
                var requestForBusinessDetails = new RestSharp.RestRequest("api/Business/GetById/" + responseForOfferDetails.Data.BusinessId.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                var responseForBusinessDetails = _client.Execute<BusinessDTO>(requestForBusinessDetails);
                if (responseForBusinessDetails.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var requestForArea = new RestSharp.RestRequest("api/Area/ByCity/" + responseForBusinessDetails.Data.Address_City, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                    var responseForArea = _client.Execute<List<AreaDTO>>(requestForArea);
                    if (responseForArea != null)
                    {
                        List<SelectListItem> areaList = new List<SelectListItem>();
                        foreach (var item in responseForArea.Data)
                        {
                            SelectListItem newItem = new SelectListItem();
                            newItem.Text = item.AreaName;
                            newItem.Value = item.Id.ToString();
                            areaList.Add(newItem);
                        }
                        model.AvailableAreas = areaList;
                    }
                }
            }

            return model;
        }
    }
}