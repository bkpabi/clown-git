﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.UI.Admin.Models;
using Clown.Business.Entities;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Clown.UI.Admin.Controllers
{
    [Authorize]
    public class ServiceProviderController : Controller
    {
        private RestClient _client;
        public ServiceProviderController()
        {
            this._client = new RestClient();
            _client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]);
        }

        [HttpGet]
        public ActionResult Index()
        {
            //Get the Service Providers
            List<BusinessSummaryViewModel> model = new List<BusinessSummaryViewModel>();
            var request = new RestSharp.RestRequest("api/Business/GetAllBusiness", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var response = _client.Execute<List<BusinessDTO>>(request);
            if (response != null)
            {
                var mModel = AutoMapper.Mapper.Map<IList<BusinessDTO>, IList<BusinessSummaryViewModel>>(response.Data);
                return View(mModel);
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult AddNewBusiness()
        {
            AddBusinessViewModel model = FillAddBusinessViewModel(new AddBusinessViewModel());
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddNewBusiness(AddBusinessViewModel model, HttpPostedFileBase[] file)
        {
            model = FillAddBusinessViewModel(model);
            if (!ModelState.IsValid)
            {
                ViewData["ErrorMessage"] = "Error while saving the new business details";
                return View(model);
            }
            else
            {
                try
                {
                    model.CreatedBy = User.Identity.Name;
                    model.CreatedOn = DateTime.Now;
                    model.State = "";
                    BusinessDTO mBusinessDTO = AutoMapper.Mapper.Map<AddBusinessViewModel, BusinessDTO>(model);
                    WorkingHrsDTO mon;
                    WorkingHrsDTO tue;
                    WorkingHrsDTO wed;
                    WorkingHrsDTO thru;
                    WorkingHrsDTO fri;
                    WorkingHrsDTO sat;
                    WorkingHrsDTO sun;
                    if (model.MonIsOpen)
                    {
                        mon = new WorkingHrsDTO() { EndTime = model.MonEndTime.TimeOfDay, StartTime = model.MonStartTime.TimeOfDay, WeekDay = "Monday", IsOpen = model.MonIsOpen };
                    }
                    else
                    {
                        mon = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Monday", IsOpen = model.MonIsOpen };
                    }

                    if (model.TueIsOpen)
                    {
                        tue = new WorkingHrsDTO() { EndTime = model.TueEndTime.TimeOfDay, StartTime = model.TueStartTime.TimeOfDay, WeekDay = "Tuesday", IsOpen = model.TueIsOpen };
                    }
                    else
                    {
                        tue = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Tuesday", IsOpen = model.TueIsOpen };
                    }
                    if (model.WedIsOpen)
                    {
                        wed = new WorkingHrsDTO() { EndTime = model.WedEndTime.TimeOfDay, StartTime = model.WedStartTime.TimeOfDay, WeekDay = "Wednesday", IsOpen = model.WedIsOpen };
                    }
                    else
                    {
                        wed = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Wednesday", IsOpen = model.WedIsOpen };
                    }
                    if (model.ThrusIsOpen)
                    {
                        thru = new WorkingHrsDTO() { EndTime = model.ThrusEndTime.TimeOfDay, StartTime = model.ThrusStartTime.TimeOfDay, WeekDay = "Thrusday", IsOpen = model.ThrusIsOpen };
                    }
                    else
                    {
                        thru = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Thrusday", IsOpen = model.ThrusIsOpen };
                    }
                    if (model.FriIsOpen)
                    {
                        fri = new WorkingHrsDTO() { EndTime = model.FriEndTime.TimeOfDay, StartTime = model.FriStartTime.TimeOfDay, WeekDay = "Friday", IsOpen = model.FriIsOpen };
                    }
                    else
                    {
                        fri = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Friday", IsOpen = model.FriIsOpen };
                    }
                    if (model.SatIsOpen)
                    {
                        sat = new WorkingHrsDTO() { EndTime = model.SatEndTime.TimeOfDay, StartTime = model.SatStartTime.TimeOfDay, WeekDay = "Saturday", IsOpen = model.SatIsOpen };
                    }
                    else
                    {
                        sat = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Saturday", IsOpen = model.SatIsOpen };
                    }
                    if (model.SunIsOpen)
                    {
                        sun = new WorkingHrsDTO() { EndTime = model.SunEndTime.TimeOfDay, StartTime = model.SunStartTime.TimeOfDay, WeekDay = "Sunday", IsOpen = model.SunIsOpen };
                    }
                    else
                    {
                        sun = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Sunday", IsOpen = model.SunIsOpen };
                    }

                    mBusinessDTO.WorkingHrs.Add(mon);
                    mBusinessDTO.WorkingHrs.Add(tue);
                    mBusinessDTO.WorkingHrs.Add(wed);
                    mBusinessDTO.WorkingHrs.Add(thru);
                    mBusinessDTO.WorkingHrs.Add(fri);
                    mBusinessDTO.WorkingHrs.Add(sat);
                    mBusinessDTO.WorkingHrs.Add(sun);
                    mBusinessDTO.Verified = false;
                    mBusinessDTO.Images = AddBusinessImage(file);
                    // Call the web api
                    var request = new RestSharp.RestRequest("api/Business/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(mBusinessDTO);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New business details added successfully.";
                        model = FillAddBusinessViewModel(new AddBusinessViewModel());
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
                catch (Exception)
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                }
                return View(model);
            }

        }

        private AddBusinessViewModel FillAddBusinessViewModel(AddBusinessViewModel model)
        {
            var requestForBusinessType = new RestSharp.RestRequest("api/businessType", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForBusinessType = _client.Execute<List<BusinessTypeDTO>>(requestForBusinessType);
            if (responseForBusinessType != null)
            {
                List<SelectListItem> businesstypeList = new List<SelectListItem>();
                foreach (var item in responseForBusinessType.Data)
                {
                    SelectListItem newItem = new SelectListItem();
                    newItem.Text = item.Name;
                    newItem.Value = item.Id.ToString();
                    businesstypeList.Add(newItem);
                }
                model.AvailableBusinessTypes = businesstypeList;
            }

            var requestForCities = new RestSharp.RestRequest("api/city", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForCities = _client.Execute<List<CityDTO>>(requestForCities);
            if (responseForCities != null)
            {
                List<SelectListItem> cityList = new List<SelectListItem>();
                foreach (var item in responseForCities.Data)
                {
                    SelectListItem newItem = new SelectListItem();
                    newItem.Text = item.CityName;
                    newItem.Value = item.Id.ToString();
                    cityList.Add(newItem);
                }
                model.AvailableCities = cityList;
            }
            return model;
        }
        
        public ActionResult Update(long Id)
        {
            try
            {
                var model = FillUpdateBusinessModel(Id);
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ModifyBusinessViewModel model, HttpPostedFileBase[] file)
        {
            if (model == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            else
            {
                model.ModifiedBy = User.Identity.Name;
                model.ModifiedOn = DateTime.Now;
                var mappedModel = AutoMapper.Mapper.Map<ModifyBusinessViewModel, BusinessDTO>(model);
                WorkingHrsDTO mon;
                WorkingHrsDTO tue;
                WorkingHrsDTO wed;
                WorkingHrsDTO thru;
                WorkingHrsDTO fri;
                WorkingHrsDTO sat;
                WorkingHrsDTO sun;
                if (model.MonIsOpen)
                {
                    mon = new WorkingHrsDTO() { EndTime = model.MonEndTime.TimeOfDay, StartTime = model.MonStartTime.TimeOfDay, WeekDay = "Monday", IsOpen = model.MonIsOpen };
                }
                else
                {
                    mon = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Monday", IsOpen = model.MonIsOpen };
                }

                if (model.TueIsOpen)
                {
                    tue = new WorkingHrsDTO() { EndTime = model.TueEndTime.TimeOfDay, StartTime = model.TueStartTime.TimeOfDay, WeekDay = "Tuesday", IsOpen = model.TueIsOpen };
                }
                else
                {
                    tue = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Tuesday", IsOpen = model.TueIsOpen };
                }
                if (model.WedIsOpen)
                {
                    wed = new WorkingHrsDTO() { EndTime = model.WedEndTime.TimeOfDay, StartTime = model.WedStartTime.TimeOfDay, WeekDay = "Wednesday", IsOpen = model.WedIsOpen };
                }
                else
                {
                    wed = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Wednesday", IsOpen = model.WedIsOpen };
                }
                if (model.ThrusIsOpen)
                {
                    thru = new WorkingHrsDTO() { EndTime = model.ThrusEndTime.TimeOfDay, StartTime = model.ThrusStartTime.TimeOfDay, WeekDay = "Thrusday", IsOpen = model.ThrusIsOpen };
                }
                else
                {
                    thru = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Thrusday", IsOpen = model.ThrusIsOpen };
                }
                if (model.FriIsOpen)
                {
                    fri = new WorkingHrsDTO() { EndTime = model.FriEndTime.TimeOfDay, StartTime = model.FriStartTime.TimeOfDay, WeekDay = "Friday", IsOpen = model.FriIsOpen };
                }
                else
                {
                    fri = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Friday", IsOpen = model.FriIsOpen };
                }
                if (model.SatIsOpen)
                {
                    sat = new WorkingHrsDTO() { EndTime = model.SatEndTime.TimeOfDay, StartTime = model.SatStartTime.TimeOfDay, WeekDay = "Saturday", IsOpen = model.SatIsOpen };
                }
                else
                {
                    sat = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Saturday", IsOpen = model.SatIsOpen };
                }
                if (model.SunIsOpen)
                {
                    sun = new WorkingHrsDTO() { EndTime = model.SunEndTime.TimeOfDay, StartTime = model.SunStartTime.TimeOfDay, WeekDay = "Sunday", IsOpen = model.SunIsOpen };
                }
                else
                {
                    sun = new WorkingHrsDTO() { EndTime = null, StartTime = null, WeekDay = "Sunday", IsOpen = model.SunIsOpen };
                }

                mappedModel.WorkingHrs.Add(mon);
                mappedModel.WorkingHrs.Add(tue);
                mappedModel.WorkingHrs.Add(wed);
                mappedModel.WorkingHrs.Add(thru);
                mappedModel.WorkingHrs.Add(fri);
                mappedModel.WorkingHrs.Add(sat);
                mappedModel.WorkingHrs.Add(sun);
                mappedModel.Address_State = string.Empty;
                mappedModel.Images = AddBusinessImage(file);
                if (model.BusinessType == 1 || model.BusinessType == 2)
                {
                    foreach (var item in model.SelectedServices)
                    {
                        ServiceOfferdDTO newServiceOffered = new ServiceOfferdDTO();
                        newServiceOffered.Highlight = true;
                        newServiceOffered.ServiceProviderId = model.BusinessId;
                        newServiceOffered.ServiceTypeId = long.Parse(item);
                        newServiceOffered.CreatedBy = User.Identity.Name;
                        newServiceOffered.CreatedOn = DateTime.Now;
                        mappedModel.ServiceOffered.Add(newServiceOffered);
                    }
                }
                else
                {
                    foreach (var item in model.SelectedProducts)
                    {
                        ProductCatalogDTO newProductCatlog = new ProductCatalogDTO();
                        newProductCatlog.ProductId = long.Parse(item);
                        newProductCatlog.ServiceProviderId = model.BusinessId;
                        mappedModel.Catlogs.Add(newProductCatlog);
                    }
                }


                var request = new RestSharp.RestRequest("api/Business/Update", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                var json = JsonConvert.SerializeObject(mappedModel);
                request.AddParameter("text/json", json, ParameterType.RequestBody);
                var response = _client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                {
                    ViewData["SuccessMessage"] = "Success! Business Details are updated successfully.";
                }
                else
                {
                    ViewData["ErrorMessage"] = "Error while updating the details";
                }
                var newModel = FillUpdateBusinessModel(model.BusinessId);
                return View(newModel);
                //return View(model);

            }
        }

        private ModifyBusinessViewModel FillUpdateBusinessModel(long Id)
        {
            var requestForBusinessDetails = new RestSharp.RestRequest("api/Business/GetById/" + Id.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForBusinessDetails = _client.Execute<BusinessDTO>(requestForBusinessDetails);
            if (responseForBusinessDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                BusinessDTO business = responseForBusinessDetails.Data;
                ModifyBusinessViewModel model = AutoMapper.Mapper.Map<BusinessDTO, ModifyBusinessViewModel>(business);
                //model = FillUpdateBusinessModel(model, business);
                //Get all services 


                var requestForBusinessType = new RestSharp.RestRequest("api/businessType", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                var responseForBusinessType = _client.Execute<List<BusinessTypeDTO>>(requestForBusinessType);
                if (responseForBusinessType != null)
                {
                    List<SelectListItem> businesstypeList = new List<SelectListItem>();
                    foreach (var item in responseForBusinessType.Data)
                    {
                        SelectListItem newItem = new SelectListItem();
                        newItem.Text = item.Name;
                        newItem.Value = item.Id.ToString();
                        businesstypeList.Add(newItem);
                    }
                    model.AvailableBusinessTypes = businesstypeList;
                }

                var requestForCities = new RestSharp.RestRequest("api/city", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                var responseForCities = _client.Execute<List<CityDTO>>(requestForCities);
                if (responseForCities != null)
                {
                    List<SelectListItem> cityList = new List<SelectListItem>();
                    foreach (var item in responseForCities.Data)
                    {
                        SelectListItem newItem = new SelectListItem();
                        newItem.Text = item.CityName;
                        newItem.Value = item.Id.ToString();
                        cityList.Add(newItem);
                    }
                    model.AvailableCities = cityList;
                }

                var requestForAreas = new RestSharp.RestRequest("api/Area/ByCity/" + model.City, RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                var responseForArea = _client.Execute<List<AreaDTO>>(requestForAreas);
                if (responseForArea != null)
                {
                    List<SelectListItem> AreaList = new List<SelectListItem>();
                    foreach (var item in responseForArea.Data)
                    {
                        SelectListItem newItem = new SelectListItem();
                        newItem.Text = item.AreaName;
                        newItem.Value = item.Id.ToString();
                        AreaList.Add(newItem);
                    }
                    model.AvailableAreas = AreaList;
                }

                if (model.BusinessType == 1 || model.BusinessType == 2)
                {

                    var requestForService = new RestSharp.RestRequest("api/Service", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                    var responseForService = _client.Execute<List<ServiceDTO>>(requestForService);
                    if (responseForService.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        foreach (var item in responseForService.Data)
                        {
                            SelectListItem newItem = new SelectListItem();
                            newItem.Text = item.ServieTypeName;
                            newItem.Value = item.Id.ToString();
                            newItem.Selected = business.ServiceOffered.SingleOrDefault(e => e.ServiceTypeId == item.Id) == null ? false : true;
                            model.AvailableServices.Add(newItem);
                        }

                    }
                }
                else
                {
                    var requestForProduct = new RestSharp.RestRequest("api/Product", RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
                    var responseForProduct = _client.Execute<List<ProductMasterDTO>>(requestForProduct);
                    if (responseForProduct.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        foreach (var item in responseForProduct.Data)
                        {
                            SelectListItem newItem = new SelectListItem();
                            newItem.Text = item.Name;
                            newItem.Value = item.Id.ToString();
                            newItem.Selected = business.Catlogs.SingleOrDefault(e => e.ProductId == item.Id) == null ? false : true;
                            model.AvailableProducts.Add(newItem);
                        }

                    }
                }
                return model;
            }
            else
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult Delete(long id)
        {
            //var requestForBusinessDetails = new RestSharp.RestRequest("api/Business/Delete" + id.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var requestForBusinessDetails = new RestSharp.RestRequest("api/Business/Delete/" + id.ToString(), RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForBusinessDetails = _client.Execute(requestForBusinessDetails);
            if (responseForBusinessDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Business details deleted successfully");
            }
            else
            {
                return Json("Business details could not be deleted");
            }
        }

        private List<ImageDTO> AddBusinessImage(HttpPostedFileBase[] file)
        {
            // New project is created now upload the images
            try
            {
                List<ImageDTO> imageList = new List<ImageDTO>();
                if (file[0] != null)
                {

                    foreach (var item in file)
                    {
                        if (item.ContentLength > 0)
                        {
                            var filePath = System.IO.Path.GetFileName(item.FileName);
                            // Create a cloud storage account and initialize it with the storage connection string
                            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                            // As we will be accessing the slob storage on the azure we need to create a blob client
                            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                            // Get the reference to the container
                            CloudBlobContainer container = blobClient.GetContainerReference("images");
                            // create the container if it do not exist and set the public access 
                            //container.CreateIfNotExists();
                            //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                            // Retrieve reference to a blob named "myblob".
                            CloudBlockBlob blockBlob = container.GetBlockBlobReference(item.FileName);

                            // Create or overwrite the "myblob" blob with contents from a local file.
                            //using (var fileStream = System.IO.File.OpenRead(item.FileName))
                            //{
                            blockBlob.UploadFromStream(item.InputStream);
                            //}

                            imageList.Add(new ImageDTO() { ImageUrl = System.Configuration.ConfigurationManager.AppSettings["CDNBaseUrl"] + item.FileName, ThumbUrl = System.Configuration.ConfigurationManager.AppSettings["CDNBaseUrl"] + item.FileName });
                            //ProjectImage newProjectImage = new ProjectImage();
                            //newProjectImage.ProjectId = ProjectId;
                            //newProjectImage.ImageLink = System.Configuration.ConfigurationManager.AppSettings["CDNBaseUrl"] + item.FileName;
                            //_repositoryPrj.AddProjectImage(newProjectImage);
                        }
                    }


                }
                return imageList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        //[Route("Serviceprovider/DeleteImage/{id}")]
        public JsonResult DeleteImage(long id)
        {
            var requestForImageDetails = new RestSharp.RestRequest("api/Business/Image/GetById/" + id.ToString(), RestSharp.Method.GET) { RequestFormat = RestSharp.DataFormat.Json };
            var responseForImageDetails = _client.Execute<ImageDTO>(requestForImageDetails);
            if (responseForImageDetails.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var request = new RestSharp.RestRequest("api/Business/Image/Delete/" + id.ToString(), RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                //var json = JsonConvert.SerializeObject(updatedProject);
                //request.AddParameter("text/json", json, ParameterType.RequestBody);
                var response = _client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                {
                    string image = responseForImageDetails.Data.ImageUrl.Split('/').Last();
                    // Delete the image from CDN
                    // Retrieve storage account from connection string.
                    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

                    // Create the blob client.
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    // Retrieve reference to a previously created container.
                    CloudBlobContainer container = blobClient.GetContainerReference("images");

                    // Retrieve reference to a blob named "myblob.txt".
                    CloudBlockBlob blockBlob = container.GetBlockBlobReference(image);

                    // Delete the blob.
                    blockBlob.Delete();
                    return Json("Image deleted successfully");
                }
                else
                {
                    return Json("Image could not be deleted");
                }
            }
            else
            {
                return Json("Project Image could not be deleted");
            }
        }
    }
}