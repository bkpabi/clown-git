﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Clown.UI.Admin.Models;
using System.Net;
using AutoMapper;
using Clown.Business.Entities;
using Newtonsoft.Json;
using RestSharp;
namespace Clown.UI.Admin.Controllers
{
    public class SubscriptionController : Controller
    {
        private RestClient _client;

        public SubscriptionController()
        {
            this._client = new RestClient();
            _client.BaseUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ClownApiBaseUrl"]);
        }

        [HttpGet]
        public ActionResult AddLineItem()
        {
            return View(new AddLineItemViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddLineItem(AddLineItemViewModel modelLineItem)
        {
            if (modelLineItem == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if (ModelState.IsValid)
            {
                try
                {
                    //Convert Viewmodel to DTO
                    var lineItemEntity = Mapper.Map<AddLineItemViewModel, LineItemDTO>(modelLineItem);
                    lineItemEntity.CreatedBy = User.Identity.Name;
                    lineItemEntity.CreatedOn = DateTime.Today;

                    var request = new RestSharp.RestRequest("api/Subscription/LineItem/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(lineItemEntity);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New line item details are added successfully.";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
                catch (Exception)
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                }

                return View(modelLineItem);

            }
            else
            {
                ModelState.AddModelError("", "Data provided by you is invalid");
                return View(modelLineItem);
            }
        }

        [HttpGet]
        public ActionResult AddProduct()
        {
            return View(new AddProductViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProduct(AddProductViewModel modelProduct)
        {
            if (modelProduct == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if (ModelState.IsValid)
            {
                try
                {
                    //Convert Viewmodel to DTO
                    var productEntity = Mapper.Map<AddProductViewModel, ProductDTO>(modelProduct);
                    productEntity.CreatedBy = User.Identity.Name;
                    productEntity.CreatedOn = DateTime.Today;

                    var request = new RestSharp.RestRequest("api/Subscription/Product/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(productEntity);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New product details are added successfully.";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
                catch (Exception)
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                }

                return View(modelProduct);

            }
            else
            {
                ModelState.AddModelError("", "Data provided by you is invalid");
                return View(modelProduct);
            }
        }

        [HttpGet]
        public ActionResult AddProductLineItem(long Id)
        {
            var model = new AddProductDetailViewModel();
            model.ProductId = Id;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddProductLineItem(AddProductDetailViewModel modelProductLineItem)
        {
            if (modelProductLineItem == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if (ModelState.IsValid)
            {
                try
                {
                    var productLimeItemEntity = Mapper.Map<AddProductDetailViewModel, ProductDetailDTO>(modelProductLineItem);
                    productLimeItemEntity.CreatedBy = User.Identity.Name;
                    productLimeItemEntity.CreatedOn = DateTime.Today;
                    var request = new RestSharp.RestRequest("api/Subscription/ProductLineItem/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(productLimeItemEntity);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New product line item details are added successfully.";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
                catch (Exception)
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                }

                return View(modelProductLineItem);
            }
            else
            {
                ModelState.AddModelError("", "Data provided by you is invalid");
                return View(modelProductLineItem);
            }
        }

        [HttpGet]
        public ActionResult AddBundledProduct()
        {
            AddBundledProductViewModel model = new AddBundledProductViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBundledProduct(AddBundledProductViewModel modelBundledProduct)
        {
            if (modelBundledProduct == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if (ModelState.IsValid)
            {
                try
                {
                    var bundledProductEntity = Mapper.Map<AddBundledProductViewModel, BundledProductDTO>(modelBundledProduct);
                    bundledProductEntity.CreatedBy = User.Identity.Name;
                    bundledProductEntity.CreatedOn = DateTime.Today;
                    var request = new RestSharp.RestRequest("api/Subscription/BundledProduct/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(bundledProductEntity);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New bundled details are added successfully.";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
                catch (Exception)
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                }

                return View(modelBundledProduct);
            }
            else
            {
                ModelState.AddModelError("", "Data provided by you is invalid");
                return View(modelBundledProduct);
            }
        }

        [HttpGet]
        public ActionResult AddSubscription()
        {
            AddSubscriptionViewModel model = new AddSubscriptionViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddSubscription(AddSubscriptionViewModel modelSubscription)
        {
            if (modelSubscription == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else if (ModelState.IsValid)
            {
                try
                {
                    var subscriptionEntity = Mapper.Map<AddSubscriptionViewModel, SubscriptionDTO>(modelSubscription);
                    
                    var request = new RestSharp.RestRequest("api/Subscription/Add", RestSharp.Method.POST) { RequestFormat = RestSharp.DataFormat.Json };
                    var json = JsonConvert.SerializeObject(subscriptionEntity);
                    request.AddParameter("text/json", json, ParameterType.RequestBody);
                    var response = _client.Execute(request);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content == "true")
                    {
                        ViewData["SuccessMessage"] = "New subscription details are added successfully.";
                        ModelState.Clear();
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "Error while saving the details";
                    }
                }
                catch (Exception)
                {
                    ViewData["ErrorMessage"] = "Error while saving the details";
                }

                return View(modelSubscription);
            }
            else
            {
                ModelState.AddModelError("", "Data provided by you is invalid");
                return View(modelSubscription);
            }
        }
    }
}